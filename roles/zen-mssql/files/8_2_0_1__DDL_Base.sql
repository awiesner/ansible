/*
 Navicat SQL Server Data Transfer

 Source Server         : vagrant DB
 Source Server Type    : SQL Server
 Source Server Version : 14003048
 Source Host           : 10.0.0.13:1433
 Source Catalog        : env2000-002-DEV-AOCU
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 14003048
 File Encoding         : 65001

 Date: 12/03/2019 14:56:30
*/


-- ----------------------------
-- Table structure for Account
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Account]') AND type IN ('U'))
	DROP TABLE [dbo].[Account]
GO

CREATE TABLE [dbo].[Account] (
  [PhysicalId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Balance] numeric(19,2)  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FIIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [finInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Account] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AccountType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountType]') AND type IN ('U'))
	DROP TABLE [dbo].[AccountType]
GO

CREATE TABLE [dbo].[AccountType] (
  [AcctTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AcctClass] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[AccountType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AccountTypeTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountTypeTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[AccountTypeTXT]
GO

CREATE TABLE [dbo].[AccountTypeTXT] (
  [AcctTypeTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AcctTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AccountTypeTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AcctAccessLevel
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AcctAccessLevel]') AND type IN ('U'))
	DROP TABLE [dbo].[AcctAccessLevel]
GO

CREATE TABLE [dbo].[AcctAccessLevel] (
  [AccessLevelId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Code] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustType] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BillPayEligible] tinyint  NOT NULL,
  [BusAcct] tinyint  NOT NULL,
  [BusSSNCheck] tinyint  NOT NULL,
  [EnrollStmt] tinyint  NOT NULL,
  [ForceSSNCheck] tinyint  NOT NULL,
  [ModPrimaryRel] tinyint  NOT NULL,
  [ModSecondaryRel] tinyint  NOT NULL,
  [TransferFrom] tinyint  NOT NULL,
  [TransferTo] tinyint  NOT NULL,
  [Viewable] tinyint  NOT NULL,
  [LevelType] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AcctAccessLevel] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AcctCrossRef
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AcctCrossRef]') AND type IN ('U'))
	DROP TABLE [dbo].[AcctCrossRef]
GO

CREATE TABLE [dbo].[AcctCrossRef] (
  [AccountNumber] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AccountType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CIFNumber] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [UpdatedDate] datetime  NULL
)
GO

ALTER TABLE [dbo].[AcctCrossRef] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertCategory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertCategory]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertCategory]
GO

CREATE TABLE [dbo].[AlertCategory] (
  [AlertCatId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AlertCatName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[AlertCategory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertDeviceRegister
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertDeviceRegister]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertDeviceRegister]
GO

CREATE TABLE [dbo].[AlertDeviceRegister] (
  [AlertDeviceRegisterId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ContactType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsRegistered] tinyint  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UserType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AlertDeviceRegister] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertLog]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertLog]
GO

CREATE TABLE [dbo].[AlertLog] (
  [AlertLogId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Address] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Content] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeliveryMethod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RetryCount] numeric(19)  NULL,
  [LogState] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TransmitDt] datetime  NULL,
  [UserId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AlertMessageId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FromEmail] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FromName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusDesc] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AlertLog] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertLogMetaData
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertLogMetaData]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertLogMetaData]
GO

CREATE TABLE [dbo].[AlertLogMetaData] (
  [AlertMetaDataId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [IsActive] tinyint  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExpiresAt] numeric(19)  NULL,
  [ExpiresIn] numeric(19)  NULL,
  [Memo] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MetaDataDesc] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MetaDataKey] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MetaDataText] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MetaDataValue] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AlertLogMetaData] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertMessage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertMessage]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertMessage]
GO

CREATE TABLE [dbo].[AlertMessage] (
  [AlertMessageId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DefaultSubject] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeliveryMethod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Enabled] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Internal] tinyint  NOT NULL,
  [ProtocolType] int  NOT NULL,
  [AlertMsgName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RecordStatus] int  NOT NULL,
  [AlertMsgType] int  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [AlertCatId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExternalRefKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AlertMessage] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertMessageField
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertMessageField]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertMessageField]
GO

CREATE TABLE [dbo].[AlertMessageField] (
  [AlertMsgFieldId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FieldName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SourceKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TargetKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AlertMessageId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AlertMessageField] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertTemplate
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertTemplate]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertTemplate]
GO

CREATE TABLE [dbo].[AlertTemplate] (
  [AlertTemplateId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Template] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AlertMessageId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AlertTemplate] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AlertTrigger
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AlertTrigger]') AND type IN ('U'))
	DROP TABLE [dbo].[AlertTrigger]
GO

CREATE TABLE [dbo].[AlertTrigger] (
  [AlertTriggerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [TriggerKey] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TriggerOption] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ValueType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TriggerValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AlertMessageId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AlertTrigger] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AltLcrParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AltLcrParty]') AND type IN ('U'))
	DROP TABLE [dbo].[AltLcrParty]
GO

CREATE TABLE [dbo].[AltLcrParty] (
  [AltPartyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BirthDate] datetime  NULL,
  [BusinessName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ConductorType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Dba] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailAddress] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExpDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FirstName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdIssuedBy] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsConductor] tinyint  NULL,
  [IsForeignTin] tinyint  NULL,
  [IssuedCountry] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssuedDate] datetime  NULL,
  [IssuedState] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleInitial] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NaicsCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Occupation] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OtherId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneNbrExt] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TinCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AltLcrParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AltParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AltParty]') AND type IN ('U'))
	DROP TABLE [dbo].[AltParty]
GO

CREATE TABLE [dbo].[AltParty] (
  [AltPartylId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [EmailAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneNum] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AltParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AltRole
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AltRole]') AND type IN ('U'))
	DROP TABLE [dbo].[AltRole]
GO

CREATE TABLE [dbo].[AltRole] (
  [AltRoleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDeleted] tinyint  NULL,
  [RoleName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsEnabledForFloater] bit  NULL
)
GO

ALTER TABLE [dbo].[AltRole] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AltRoleTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AltRoleTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[AltRoleTXT]
GO

CREATE TABLE [dbo].[AltRoleTXT] (
  [AltRoleTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AltRoleId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AltRoleTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AltUserAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AltUserAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[AltUserAttribute]
GO

CREATE TABLE [dbo].[AltUserAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltUserId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AltUserAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AltUserAuth
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AltUserAuth]') AND type IN ('U'))
	DROP TABLE [dbo].[AltUserAuth]
GO

CREATE TABLE [dbo].[AltUserAuth] (
  [AltUserId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DefaultRole] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserFullName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EscalateComplaint] tinyint  NULL,
  [expiryDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsAdministrator] tinyint  NULL,
  [IsReadOnly] tinyint  NULL,
  [UserName] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Password] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResolveComplaint] tinyint  NULL,
  [UserStatus] int  NULL,
  [UserType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FloaterEndDate] datetime  NULL,
  [FloaterStartDate] datetime  NULL,
  [IsFloater] tinyint  NULL,
  [IsSupervisor] tinyint  NULL,
  [PrimaryPoint] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SecondaryPoint] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FirstName] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleInitial] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AltUserAuth] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AltUserRole
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AltUserRole]') AND type IN ('U'))
	DROP TABLE [dbo].[AltUserRole]
GO

CREATE TABLE [dbo].[AltUserRole] (
  [AltUserId] numeric(19)  NOT NULL,
  [AltRoleId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[AltUserRole] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppBranding
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppBranding]') AND type IN ('U'))
	DROP TABLE [dbo].[AppBranding]
GO

CREATE TABLE [dbo].[AppBranding] (
  [BrandingId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BrandingName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AppBranding] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppBrandingTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppBrandingTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[AppBrandingTXT]
GO

CREATE TABLE [dbo].[AppBrandingTXT] (
  [BrandingTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BrandingId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AppBrandingTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Application
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Application]') AND type IN ('U'))
	DROP TABLE [dbo].[Application]
GO

CREATE TABLE [dbo].[Application] (
  [ApplId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DepricateDt] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsConfigApp] tinyint  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VersionName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Application] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ApplicationTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ApplicationTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ApplicationTXT]
GO

CREATE TABLE [dbo].[ApplicationTXT] (
  [ApplTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ApplicationTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppModule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppModule]') AND type IN ('U'))
	DROP TABLE [dbo].[AppModule]
GO

CREATE TABLE [dbo].[AppModule] (
  [ModuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Disallow] tinyint  NOT NULL,
  [IsDefault] tinyint  NULL,
  [IsMaintainence] tinyint  NULL,
  [moduleClass] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModuleIndex] numeric(19)  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplId] numeric(19)  NOT NULL,
  [Image] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModuleName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AppModule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppModuleTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppModuleTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[AppModuleTXT]
GO

CREATE TABLE [dbo].[AppModuleTXT] (
  [ModuleTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModuleId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AppModuleTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppNote
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppNote]') AND type IN ('U'))
	DROP TABLE [dbo].[AppNote]
GO

CREATE TABLE [dbo].[AppNote] (
  [AppNoteId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NoteSubject] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NoteText] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NoteType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [BookApplicationId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AppNote] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppProcessAction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppProcessAction]') AND type IN ('U'))
	DROP TABLE [dbo].[AppProcessAction]
GO

CREATE TABLE [dbo].[AppProcessAction] (
  [AppProcessActionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Action] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ActionOrder] int  NULL,
  [ActionTXT] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecordState] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Role] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AppProcessAction] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppProperty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppProperty]') AND type IN ('U'))
	DROP TABLE [dbo].[AppProperty]
GO

CREATE TABLE [dbo].[AppProperty] (
  [PropertyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CodeCategory] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DataFormat] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DataType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DefaultValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ApplId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AppProperty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppPropertyTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppPropertyTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[AppPropertyTXT]
GO

CREATE TABLE [dbo].[AppPropertyTXT] (
  [AppPropTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AppPropertyTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppRestriction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppRestriction]') AND type IN ('U'))
	DROP TABLE [dbo].[AppRestriction]
GO

CREATE TABLE [dbo].[AppRestriction] (
  [AppRestrictionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionReasonCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SupervisorNote] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserNote] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[AppRestriction] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppThresholdMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppThresholdMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[AppThresholdMatrix]
GO

CREATE TABLE [dbo].[AppThresholdMatrix] (
  [AppThresholdId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ArchiveThreshold] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PurgeThreshold] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecordState] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AppThresholdMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AppValidationRule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AppValidationRule]') AND type IN ('U'))
	DROP TABLE [dbo].[AppValidationRule]
GO

CREATE TABLE [dbo].[AppValidationRule] (
  [AppValidationRuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsEnabled] tinyint  NULL,
  [RuleName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecordState] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleValue] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AppValidationRule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Attachment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Attachment]') AND type IN ('U'))
	DROP TABLE [dbo].[Attachment]
GO

CREATE TABLE [dbo].[Attachment] (
  [AttachmentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AttachmentData] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FileName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MimeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttachmentName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [partState] int  NULL,
  [AttachmentStatus] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Reason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [url] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookApplicationId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL,
  [AttachmentType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AllPartiesInd] bit  NULL
)
GO

ALTER TABLE [dbo].[Attachment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AttributeName
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AttributeName]') AND type IN ('U'))
	DROP TABLE [dbo].[AttributeName]
GO

CREATE TABLE [dbo].[AttributeName] (
  [AttributeNameId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDeleted] tinyint  NULL,
  [AttribName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CodeCategoryId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AttributeName] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AttributeNameTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AttributeNameTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[AttributeNameTXT]
GO

CREATE TABLE [dbo].[AttributeNameTXT] (
  [AttribNameTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeNameId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[AttributeNameTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AuditLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditLog]') AND type IN ('U'))
	DROP TABLE [dbo].[AuditLog]
GO

CREATE TABLE [dbo].[AuditLog] (
  [AuditLogId] bigint  IDENTITY(1,1) NOT NULL,
  [ApplicationName] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MessageContext] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModifierDate] datetime  NULL,
  [ModifierUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModifierUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NewContent] xml  NULL,
  [OldContent] xml  NULL,
  [TableName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuditDate] date  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Duid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModifierApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModifierDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AuditLog] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BatchEnrollment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BatchEnrollment]') AND type IN ('U'))
	DROP TABLE [dbo].[BatchEnrollment]
GO

CREATE TABLE [dbo].[BatchEnrollment] (
  [Id] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BatchStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Email] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FirstName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoginName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Password] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Phone] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReturnCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusDescription] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BatchEnrollment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BatchLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BatchLog]') AND type IN ('U'))
	DROP TABLE [dbo].[BatchLog]
GO

CREATE TABLE [dbo].[BatchLog] (
  [BatchLogId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CompletedRecordCount] numeric(19)  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EndDt] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TaskName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PendingRecordCount] numeric(19)  NULL,
  [pointNames] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReferenceKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RejectedRecordCount] numeric(19)  NULL,
  [ServiceName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StartDt] datetime  NOT NULL,
  [Status] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusDesc] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaskKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalRecordCount] numeric(19)  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BatchLog] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BatchTask
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BatchTask]') AND type IN ('U'))
	DROP TABLE [dbo].[BatchTask]
GO

CREATE TABLE [dbo].[BatchTask] (
  [BatchTaskId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DayOfMonth] int  NULL,
  [Enabled] tinyint  NULL,
  [EndDt] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Hour] smallint  NULL,
  [Minute] smallint  NULL,
  [Month] smallint  NULL,
  [TaskName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Params] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StartDt] datetime  NOT NULL,
  [TaskKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [WeekDayFlags] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FileId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BatchTask] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BestFitOffer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BestFitOffer]') AND type IN ('U'))
	DROP TABLE [dbo].[BestFitOffer]
GO

CREATE TABLE [dbo].[BestFitOffer] (
  [BestFitOfferId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctUsage] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatedDate] datetime  NULL,
  [MarketGroupId] numeric(19)  NULL,
  [ProductId] numeric(19)  NULL,
  [ImageDetailUrl] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ImageUrl] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BestFitOffer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BestFitOfferHist
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BestFitOfferHist]') AND type IN ('U'))
	DROP TABLE [dbo].[BestFitOfferHist]
GO

CREATE TABLE [dbo].[BestFitOfferHist] (
  [BestFitOfferHistId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Comment] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MarketGroupId] numeric(19)  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] numeric(19)  NULL,
  [RemaindOn] datetime  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BestFitOfferHist] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Bin
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Bin]') AND type IN ('U'))
	DROP TABLE [dbo].[Bin]
GO

CREATE TABLE [dbo].[Bin] (
  [BinId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BatchNbr] numeric(19)  NULL,
  [BatchedCount] numeric(19,2)  NULL,
  [BatchedValue] numeric(31)  NULL,
  [BinCount] numeric(19,2)  NULL,
  [BinDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BinValue] numeric(31)  NULL,
  [BinTypeId] numeric(19)  NULL,
  [DrawerId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Bin] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BinData
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BinData]') AND type IN ('U'))
	DROP TABLE [dbo].[BinData]
GO

CREATE TABLE [dbo].[BinData] (
  [BinDataId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BankName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BinNumber] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Brand] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [country] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [info] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [iso2CountryCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [iso3CountryCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [isoCountry] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Level] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [phone] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [webUrl] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BinData] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BinType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BinType]') AND type IN ('U'))
	DROP TABLE [dbo].[BinType]
GO

CREATE TABLE [dbo].[BinType] (
  [BinTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BinCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemClass] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ItemFlow] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BinTypeName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrwrTypeId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BinType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BinTypeTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BinTypeTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[BinTypeTXT]
GO

CREATE TABLE [dbo].[BinTypeTXT] (
  [BinTypeTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BinTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[BinTypeTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Bond
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Bond]') AND type IN ('U'))
	DROP TABLE [dbo].[Bond]
GO

CREATE TABLE [dbo].[Bond] (
  [BondId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BondType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BondValue] numeric(19,2)  NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeginMonth] numeric(19)  NULL,
  [BeginYear] numeric(19)  NULL,
  [EndMonth] numeric(19)  NULL,
  [EndYear] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Bond] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BookAccount
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BookAccount]') AND type IN ('U'))
	DROP TABLE [dbo].[BookAccount]
GO

CREATE TABLE [dbo].[BookAccount] (
  [BookAccountId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctPurpose] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctSubType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctTitle] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctTitleDesc] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctUsage] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignWireSupportCountry] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InterestBearing] tinyint  NULL,
  [InterestRate] numeric(15,5)  NULL,
  [SkipAcctInd] tinyint  NULL,
  [NickName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OpenDt] datetime  NULL,
  [OpeningAmt] numeric(28,4)  NULL,
  [PackageId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PurposeOther] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [StatementType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SubProductCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TempApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Term] int  NULL,
  [TermUnits] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookApplicationId] numeric(19)  NULL,
  [InterestDistributionMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InterestPaymentFerq] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InterestXferAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InterestXferAcctRoutingNbr] varchar(9) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InterestXferAcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaturityDt] datetime  NULL,
  [BalanceDisbursementMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BalanceXferAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BalanceXferAcctRoutingNbr] varchar(9) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BalanceXferAcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AutoRenewInd] tinyint  NULL,
  [IsRetirementAcct] tinyint  NULL,
  [AcctDesc] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PlanCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AffinityCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Bank] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Branch] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Company] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CostCenter] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginationMethod] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginationOfficerId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PromoCd] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResponsibleOfficerId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RouteNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceLevel] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WorkplaceSolutionCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctTitle1] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctTitle2] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsPrimaryAcct] tinyint  NULL,
  [InterestTerm] numeric(19)  NULL,
  [InterestTermUnits] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSameAsCustAddress] bit  NULL,
  [Apy] numeric(28,4)  NULL,
  [Campaign] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SalesCreditTo] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProdSubCategory] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RateIndex] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NewAcctHoldWaiveInd] tinyint  NULL,
  [OldApy] numeric(19)  NULL,
  [OldInterestRate] numeric(19)  NULL,
  [RateOverrideReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StarterChkInd] tinyint  NULL,
  [WaiveFeeInd] tinyint  NULL,
  [WaiveFeeReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WaiveFeeTimePeriod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [KYCStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BookAccount] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BookAccountAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BookAccountAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[BookAccountAttribute]
GO

CREATE TABLE [dbo].[BookAccountAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BookAccountAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BookApplication
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BookApplication]') AND type IN ('U'))
	DROP TABLE [dbo].[BookApplication]
GO

CREATE TABLE [dbo].[BookApplication] (
  [BookApplicationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ApplicationType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ArchiveDate] datetime  NULL,
  [ArchiveUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ArchiveUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuditTrailStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PageCursor] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Page] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookmarkParams] varchar(5120) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BsaStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BusinessProcessId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkOrderStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DebitCardOrderStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsEsignConsentAccepted] tinyint  NULL,
  [IsSaintsCardEligible] tinyint  NULL,
  [LockedDate] datetime  NULL,
  [LockedUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrimaryRelMgr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessAction] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Reason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecordState] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ReferenceKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReferenceKeyGenDate] datetime  NULL,
  [SessionId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusChgDate] datetime  NULL,
  [StatusReason] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Team] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TeamLead] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AffinityCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Bank] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Branch] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Company] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CostCenter] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginationMethod] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginationOfficerId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PromoCd] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResponsibleOfficerId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RouteNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceLevel] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WorkplaceSolutionCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ProcessStatusId] numeric(19)  NULL,
  [AlertTriggerReason] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Relationship] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompletedExtractStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EnvelopKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDataGatherCompleted] bit  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FundVerifyStatusReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EcommConsentAcceptedDt] datetime  NULL,
  [IsEcommConsentAccepted] bit  NULL,
  [TitleOptionId] numeric(19)  NULL,
  [TitleTypeName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ArchiveApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ArchiveDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsRequireDocumentation] bit  NULL,
  [DocStatusReason] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BookApplication] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BookParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BookParty]') AND type IN ('U'))
	DROP TABLE [dbo].[BookParty]
GO

CREATE TABLE [dbo].[BookParty] (
  [PartyType] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BookPartyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplRel] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BackupWithholdInd] tinyint  NULL,
  [CountryOfOrigin] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailSolicitationInd] tinyint  NULL,
  [ExistingPartyVerifyMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignCountry] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GovAssociationType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InquiryStatus] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InternetBankingId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InternetBankingInd] tinyint  NULL,
  [ExistingPartyInd] tinyint  NULL,
  [IsGoodPartyInd] tinyint  NULL,
  [SkipPartyInd] tinyint  NULL,
  [LegalName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PartyName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [OEDCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartySubType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PassKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneSolicitationInd] tinyint  NULL,
  [Position] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrefLang] varchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrivacyOptInd] tinyint  NULL,
  [RegDInd] tinyint  NULL,
  [Relationship] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResidenceCtry] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [CustSince] datetime  NULL,
  [SuppressInd] tinyint  NULL,
  [SuppressReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxCtry] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxStatus] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TempPartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EstDt] datetime  NULL,
  [EstablishedState] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrgDesc] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrgType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BrthDt] datetime  NULL,
  [CitizenshipCntry] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Dependents] int  NULL,
  [Gender] int  NULL,
  [GivenNames] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HomeOwnershipType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ImmigrationStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaidenName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaritalStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Nationality] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Spouse] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Suffix] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SurName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Title] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookApplicationId] numeric(19)  NULL,
  [LegalDesignation] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LegalFormationCountry] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LegalFormationState] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NaicsCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NaicsDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DbaName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NaicsCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AffinityCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Bank] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Branch] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Company] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CostCenter] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginationMethod] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginationOfficerId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PromoCd] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResponsibleOfficerId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RouteNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceLevel] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WorkplaceSolutionCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationDate] datetime  NULL,
  [Score] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsGroupBankingInd] tinyint  NULL,
  [OwnershipType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProspectId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCIPVerified] tinyint  NULL,
  [InquiryIDCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PaymentAmount] numeric(19,2)  NULL,
  [InsiderType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [W9AcceptInd] bit  NULL,
  [CitizenshipDocID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CitizenshipDocIssuedBy] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CitizenshipDocType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CitizenshipDocExpDt] datetime  NULL,
  [CitizenshipDocIssuedDate] datetime  NULL,
  [CitizenshipPurpose] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GreenCardInd] bit  NULL,
  [StudentInd] bit  NULL,
  [ThreeYearsInd] bit  NULL,
  [CitizenshipStatusDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreditCheckAuthInd] bit  NULL,
  [CitizenshipCountry] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InPersonIndicator] bit  NULL,
  [W9AcceptedDate] date  NULL,
  [KYCStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BookParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BookPartyAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BookPartyAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[BookPartyAttribute]
GO

CREATE TABLE [dbo].[BookPartyAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookPartyId] numeric(19)  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BookPartyAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BookPartyRel
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BookPartyRel]') AND type IN ('U'))
	DROP TABLE [dbo].[BookPartyRel]
GO

CREATE TABLE [dbo].[BookPartyRel] (
  [BookPartyRelId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficPerOwn] numeric(9,6)  NULL,
  [BeneficRel] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrderCard] tinyint  NULL,
  [ECOACd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ownership] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PercentOwn] numeric(9,6)  NULL,
  [Role] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [RelType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL,
  [IsSkipRelationshipInd] tinyint  NULL,
  [RelationshipKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BookPartyRel] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BrandingAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BrandingAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[BrandingAttribute]
GO

CREATE TABLE [dbo].[BrandingAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BrandingId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[BrandingAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BSAData
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BSAData]') AND type IN ('U'))
	DROP TABLE [dbo].[BSAData]
GO

CREATE TABLE [dbo].[BSAData] (
  [BSARecordId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctPurpose] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompanyAnnualSales] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignCountry] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignGovAssociationType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignGovServiceInd] tinyint  NULL,
  [ForeignParentCompanyLocation] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignPosition] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignWireSupportCountry] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IndustryType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCompanyHavingForeignParent] tinyint  NULL,
  [IsImportExportService] tinyint  NULL,
  [IsPrimaryCashBusiness] tinyint  NULL,
  [LocalGovAssociationType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LocalGovServiceInd] tinyint  NULL,
  [PurposeOther] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookPartyId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BSAData] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BusinessCalendar
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BusinessCalendar]') AND type IN ('U'))
	DROP TABLE [dbo].[BusinessCalendar]
GO

CREATE TABLE [dbo].[BusinessCalendar] (
  [BusinessCalId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HolidayCalc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NULL,
  [BusinessCalName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [IsFriday] tinyint  NULL,
  [IsMonday] tinyint  NULL,
  [IsSaturday] tinyint  NULL,
  [IsSunday] tinyint  NULL,
  [IsThursday] tinyint  NULL,
  [IsTuesday] tinyint  NULL,
  [IsWednesday] tinyint  NULL,
  [WeekEndRule] int  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BusinessCalendar] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BusinessCalendarTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BusinessCalendarTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[BusinessCalendarTXT]
GO

CREATE TABLE [dbo].[BusinessCalendarTXT] (
  [BusinessCalendarTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BusinessCalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[BusinessCalendarTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for BusinessCard
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BusinessCard]') AND type IN ('U'))
	DROP TABLE [dbo].[BusinessCard]
GO

CREATE TABLE [dbo].[BusinessCard] (
  [BusinessCardId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Card] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ProsId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ExtUserId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BusinessCard] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CalendarException
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CalendarException]') AND type IN ('U'))
	DROP TABLE [dbo].[CalendarException]
GO

CREATE TABLE [dbo].[CalendarException] (
  [CalExceptionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Day] smallint  NOT NULL,
  [Month] smallint  NOT NULL,
  [CalExceptName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Year] smallint  NULL,
  [BusinessCalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CalendarException] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CardData
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CardData]') AND type IN ('U'))
	DROP TABLE [dbo].[CardData]
GO

CREATE TABLE [dbo].[CardData] (
  [CardId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BusinessEmployeeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BusinessName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardNbr] varchar(19) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardTheme] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmbossName1] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmbossName2] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExpDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsAcceptForeignTran] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrimaryAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrimaryAcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShortName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceId] numeric(19)  NULL,
  [PinBlock] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardDesign] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DesignClass] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CardData] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CardDesign
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CardDesign]') AND type IN ('U'))
	DROP TABLE [dbo].[CardDesign]
GO

CREATE TABLE [dbo].[CardDesign] (
  [CardDesignId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BinNbr] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardImage] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardTheme] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DesignClass] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DesignName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DesignType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CardDesign] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CardPinValidation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CardPinValidation]') AND type IN ('U'))
	DROP TABLE [dbo].[CardPinValidation]
GO

CREATE TABLE [dbo].[CardPinValidation] (
  [CardPinValidationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BankerAction] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BankerMsg] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CustMsg] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ExceptionCd] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ExceptionMsg] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsAlertBanker] tinyint  NOT NULL,
  [IsAlertCust] tinyint  NOT NULL,
  [IsBankerAckReq] tinyint  NOT NULL,
  [IsProceedTrans] tinyint  NOT NULL,
  [Language] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ResultCd] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ResultMsg] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[CardPinValidation] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CardStatusValidation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CardStatusValidation]') AND type IN ('U'))
	DROP TABLE [dbo].[CardStatusValidation]
GO

CREATE TABLE [dbo].[CardStatusValidation] (
  [CardStatusValidationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BankerAction] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BankerMsg] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CardStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CustMsg] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsAlertBanker] tinyint  NOT NULL,
  [IsAlertCust] tinyint  NOT NULL,
  [IsBankerAckReq] tinyint  NOT NULL,
  [IsProceedTrans] tinyint  NOT NULL,
  [Language] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[CardStatusValidation] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CartProductLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CartProductLink]') AND type IN ('U'))
	DROP TABLE [dbo].[CartProductLink]
GO

CREATE TABLE [dbo].[CartProductLink] (
  [ShoppingCartId] numeric(19)  NOT NULL,
  [ProductId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[CartProductLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseAuditHistory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseAuditHistory]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseAuditHistory]
GO

CREATE TABLE [dbo].[CaseAuditHistory] (
  [CaseAuditHistId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Comments] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NewVal] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OldVal] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaskName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CaseFolderId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CaseAuditHistory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseCustomer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseCustomer]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseCustomer]
GO

CREATE TABLE [dbo].[CaseCustomer] (
  [CaseCustomerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustomerName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CaseFolderId] numeric(19)  NULL,
  [AcctIdTo] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Amount] numeric(31,3)  NULL,
  [UserId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyFiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ToFiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ToAcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CaseCustomer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseDataShadow
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseDataShadow]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseDataShadow]
GO

CREATE TABLE [dbo].[CaseDataShadow] (
  [ShadowContentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckAmount] numeric(30,4)  NULL,
  [ContentAddress] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Cuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Memo] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VarName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PayeeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CaseDataShadow] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseFolder
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseFolder]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseFolder]
GO

CREATE TABLE [dbo].[CaseFolder] (
  [CaseFolderId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AmlKycInd] tinyint  NULL,
  [Closed] datetime  NULL,
  [ComplaintInd] tinyint  NULL,
  [Creator] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Cuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Disposition] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [HostKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Opened] datetime  NULL,
  [OrigChannel] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Owner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ParentCuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RecordType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RedFlagInd] tinyint  NULL,
  [RefCuid] varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Restarts] int  NULL,
  [Status] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CaseTitle] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [GroupId] numeric(19)  NULL,
  [PointId] numeric(19)  NULL,
  [OwnerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Comment] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CaseFolder] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseForm
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseForm]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseForm]
GO

CREATE TABLE [dbo].[CaseForm] (
  [CaseFormId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FormName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CaseProcessId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CaseForm] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseMilestone
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseMilestone]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseMilestone]
GO

CREATE TABLE [dbo].[CaseMilestone] (
  [MilestoneId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Comment] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Completed] datetime  NOT NULL,
  [Cuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MilestoneName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[CaseMilestone] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseQueuePerm
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseQueuePerm]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseQueuePerm]
GO

CREATE TABLE [dbo].[CaseQueuePerm] (
  [CaseQueuePermId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CanUnlock] tinyint  NULL,
  [CanWork] tinyint  NULL,
  [RoleName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CaseTaskQueueId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CaseQueuePerm] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseTaskQueue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseTaskQueue]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseTaskQueue]
GO

CREATE TABLE [dbo].[CaseTaskQueue] (
  [CaseTaskQueueId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Department] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailUponEntry] tinyint  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GroupEmail] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [QueueName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CaseTaskQueue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseTaskQueueTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseTaskQueueTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseTaskQueueTXT]
GO

CREATE TABLE [dbo].[CaseTaskQueueTXT] (
  [CaseTaskQueueTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CaseTaskQueueId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CaseTaskQueueTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CaseUserTask
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CaseUserTask]') AND type IN ('U'))
	DROP TABLE [dbo].[CaseUserTask]
GO

CREATE TABLE [dbo].[CaseUserTask] (
  [UserTaskId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AssignedUser] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Completed] datetime  NULL,
  [CompCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Cuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Escalated] tinyint  NULL,
  [eventName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Expires] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ItemId] varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastUpdater] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastWorked] datetime  NULL,
  [LateDate] datetime  NULL,
  [LateSent] tinyint  NULL,
  [LockId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaskName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NotifyAddr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PreviousQueue] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Priority] int  NULL,
  [QueueName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Requested] datetime  NOT NULL,
  [WorkRole] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [WarnDate] datetime  NULL,
  [WarningSent] tinyint  NULL,
  [WorkDuration] numeric(19)  NULL,
  [CaseFolderId] numeric(19)  NULL,
  [Comment] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CaseUserTask] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CashAggregation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CashAggregation]') AND type IN ('U'))
	DROP TABLE [dbo].[CashAggregation]
GO

CREATE TABLE [dbo].[CashAggregation] (
  [AggregationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AggregationType] int  NULL,
  [CashInAmt] numeric(31,3)  NULL,
  [CashInAmtCurCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CashOutAmt] numeric(31,3)  NULL,
  [CashOutAmtCurCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstrumentAmt] numeric(31,3)  NULL,
  [InstrumentAmtCurCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsTrigger] tinyint  NULL,
  [AggregationDate] datetime  NULL,
  [TotalCashCheckAmt] numeric(31,3)  NULL,
  [TotalExchangeAmt] numeric(31,3)  NULL,
  [TotalWithdrawalAmt] numeric(31,3)  NULL
)
GO

ALTER TABLE [dbo].[CashAggregation] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CashDrawer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CashDrawer]') AND type IN ('U'))
	DROP TABLE [dbo].[CashDrawer]
GO

CREATE TABLE [dbo].[CashDrawer] (
  [DrawerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockingUser] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedDate] datetime  NULL,
  [DrawerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrwrTypeId] numeric(19)  NULL,
  [PointId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CashDrawer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ChannelError
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelError]') AND type IN ('U'))
	DROP TABLE [dbo].[ChannelError]
GO

CREATE TABLE [dbo].[ChannelError] (
  [ChannelErrorId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ErrorCode] numeric(19)  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [System] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ErrorType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ChannelError] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ChannelMessage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelMessage]') AND type IN ('U'))
	DROP TABLE [dbo].[ChannelMessage]
GO

CREATE TABLE [dbo].[ChannelMessage] (
  [ChannelMessageId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Category] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Code] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MessageContext] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [System] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Type] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ChannelMessage] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ChannelMessageTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelMessageTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ChannelMessageTXT]
GO

CREATE TABLE [dbo].[ChannelMessageTXT] (
  [ChannelMessageTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ChannelMessageId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ChannelMessageTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CheckData
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckData]') AND type IN ('U'))
	DROP TABLE [dbo].[CheckData]
GO

CREATE TABLE [dbo].[CheckData] (
  [CheckId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ChkAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkBackImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkCarAmt] numeric(19,2)  NULL,
  [ChkCarScore] int  NULL,
  [ChkAmt] numeric(19,2)  NULL,
  [ChkNbr] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkFrontImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RawMICRLine] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkRoutingNbr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FundingSourceId] numeric(19)  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreateApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreateDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdateApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdateDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CheckData] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ChildJournal
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ChildJournal]') AND type IN ('U'))
	DROP TABLE [dbo].[ChildJournal]
GO

CREATE TABLE [dbo].[ChildJournal] (
  [ChildId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ChildJournalId] numeric(19)  NULL,
  [SeqNumber] numeric(19)  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranAmt] numeric(31,3)  NULL,
  [TranAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCode] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TrnDt] datetime  NULL,
  [JournalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ChildJournal] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ChkItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ChkItem]') AND type IN ('U'))
	DROP TABLE [dbo].[ChkItem]
GO

CREATE TABLE [dbo].[ChkItem] (
  [ChkId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ChkAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkAmt] numeric(19,2)  NULL,
  [ChkEnteredAmt] numeric(19,2)  NULL,
  [ChkImgBack] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkImgFront] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkNum] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ConfidenceScore] int  NULL,
  [FeeAmt] numeric(19,2)  NULL,
  [FeeAmtCurCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkFIIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Memo] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Name] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrigDt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OverrideBy] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionReason] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RetsrictionCode] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionStatus] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [routingNum] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationResultCode] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationResultReason] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationStatus] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifiedBy] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifiedDate] datetime  NULL,
  [JournalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ChkItem] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ChkValidationRule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ChkValidationRule]') AND type IN ('U'))
	DROP TABLE [dbo].[ChkValidationRule]
GO

CREATE TABLE [dbo].[ChkValidationRule] (
  [RuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BankerMsg] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CustomerMsg] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsBankerReviewReq] tinyint  NULL,
  [IsRejectCheck] tinyint  NULL,
  [Language] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Threshold] numeric(19,2)  NOT NULL,
  [RuleKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[ChkValidationRule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CifCrossRef
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CifCrossRef]') AND type IN ('U'))
	DROP TABLE [dbo].[CifCrossRef]
GO

CREATE TABLE [dbo].[CifCrossRef] (
  [CIFNumber] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AddressLine1] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AddressLine2] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BusinessName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CustomerType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailAddress] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FirstName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrimaryPhone] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [State] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatedDate] datetime  NULL,
  [ZipCode] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CifCrossRef] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CityStatePostalCode
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CityStatePostalCode]') AND type IN ('U'))
	DROP TABLE [dbo].[CityStatePostalCode]
GO

CREATE TABLE [dbo].[CityStatePostalCode] (
  [CityStatePostalCodeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DaylightSavings] numeric(19)  NULL,
  [FinInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Latitude] float(53)  NULL,
  [Longitude] float(53)  NULL,
  [Offset] numeric(19)  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [State] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CityStatePostalCode] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Code
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Code]') AND type IN ('U'))
	DROP TABLE [dbo].[Code]
GO

CREATE TABLE [dbo].[Code] (
  [CodeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CodeOrder] int  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CodeLang] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CodeName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Status] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CodeValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CodeCategoryId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Code] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CodeAttrib
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CodeAttrib]') AND type IN ('U'))
	DROP TABLE [dbo].[CodeAttrib]
GO

CREATE TABLE [dbo].[CodeAttrib] (
  [CodeAttribId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttribValue] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CodeId] numeric(19)  NOT NULL,
  [AttributeNameId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[CodeAttrib] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CodeCategory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CodeCategory]') AND type IN ('U'))
	DROP TABLE [dbo].[CodeCategory]
GO

CREATE TABLE [dbo].[CodeCategory] (
  [CodeCategoryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDeleted] tinyint  NULL,
  [CategoryName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CodeCategory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CodeCategoryTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CodeCategoryTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[CodeCategoryTXT]
GO

CREATE TABLE [dbo].[CodeCategoryTXT] (
  [CodeCatTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CodeCategoryId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CodeCategoryTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Complaint
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Complaint]') AND type IN ('U'))
	DROP TABLE [dbo].[Complaint]
GO

CREATE TABLE [dbo].[Complaint] (
  [ComplaintId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Category] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintBranch] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintNumber] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ComplaintRegion] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurrentCustomer] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EscalatedCmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EscalatedDt] datetime  NULL,
  [EscalatedOwner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EscalatedTo] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Requested] tinyint  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FollowUpCustomer] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FollowUpDt] datetime  NULL,
  [FollowUpType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LineOfBusiness] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Location] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NotificationDate] datetime  NULL,
  [OpenedDt] datetime  NOT NULL,
  [Owner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Regulatory] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegulatorContact] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegulatorPhone] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegulatoryRisk] tinyint  NULL,
  [Repeated] tinyint  NULL,
  [ResolutionAction] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResolutionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResolvedDate] datetime  NULL,
  [Role] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Source] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Stage] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TargetDate] datetime  NULL,
  [ComplaintType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VendorName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VendorCaused] tinyint  NULL,
  [Version] numeric(19)  NULL,
  [OfferContactId] numeric(19)  NOT NULL,
  [OwnerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Complaint] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ComplaintAttachment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ComplaintAttachment]') AND type IN ('U'))
	DROP TABLE [dbo].[ComplaintAttachment]
GO

CREATE TABLE [dbo].[ComplaintAttachment] (
  [ComplaintAttachmentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AttachmentData] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FileName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MimeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttachmentName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttachmentStatus] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ComplaintAttachment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ComplaintComment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ComplaintComment]') AND type IN ('U'))
	DROP TABLE [dbo].[ComplaintComment]
GO

CREATE TABLE [dbo].[ComplaintComment] (
  [ComplaintCommentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintId] numeric(19)  NOT NULL,
  [CommentTXT] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ComplaintComment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ComplaintHistory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ComplaintHistory]') AND type IN ('U'))
	DROP TABLE [dbo].[ComplaintHistory]
GO

CREATE TABLE [dbo].[ComplaintHistory] (
  [ComplaintHistoryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ComplaintAction] int  NULL,
  [Category] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintBranch] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintRegion] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Escalated] tinyint  NULL,
  [EscalatedCmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EscalatedDt] datetime  NULL,
  [EscalatedOwner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EscalatedTo] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Requested] tinyint  NULL,
  [FollowUpCustomer] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FollowUpDt] datetime  NULL,
  [FollowUpType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LineOfBusiness] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Location] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OpenedDt] datetime  NULL,
  [Owner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ProductCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Regulatory] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegulatorContact] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegulatorPhone] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegulatoryRisk] tinyint  NULL,
  [Repeated] tinyint  NULL,
  [ResolutionAction] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResolutionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResolvedDt] datetime  NULL,
  [Role] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Source] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Stage] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TargetDate] datetime  NULL,
  [ComplaintType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VendorName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VendorCaused] tinyint  NULL,
  [ComplaintId] numeric(19)  NOT NULL,
  [OwnerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ComplaintHistory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ComplaintWatcher
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ComplaintWatcher]') AND type IN ('U'))
	DROP TABLE [dbo].[ComplaintWatcher]
GO

CREATE TABLE [dbo].[ComplaintWatcher] (
  [ComplaintWatcherId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [EmailAddr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [WatcherType] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ComplaintId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ComplaintWatcher] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Component
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Component]') AND type IN ('U'))
	DROP TABLE [dbo].[Component]
GO

CREATE TABLE [dbo].[Component] (
  [CompId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CompURL] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Component] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ComponentType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ComponentType]') AND type IN ('U'))
	DROP TABLE [dbo].[ComponentType]
GO

CREATE TABLE [dbo].[ComponentType] (
  [CompTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompTypeLevel] int  NULL,
  [CompTypeName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ComponentType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CompProperty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CompProperty]') AND type IN ('U'))
	DROP TABLE [dbo].[CompProperty]
GO

CREATE TABLE [dbo].[CompProperty] (
  [CompPropId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompPropName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompPropType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CompProperty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CompRelation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CompRelation]') AND type IN ('U'))
	DROP TABLE [dbo].[CompRelation]
GO

CREATE TABLE [dbo].[CompRelation] (
  [CompRelationid] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CompChild] numeric(19)  NULL,
  [CompParent] numeric(19)  NULL,
  [CompRoot] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CompRelation] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CompRoleValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CompRoleValue]') AND type IN ('U'))
	DROP TABLE [dbo].[CompRoleValue]
GO

CREATE TABLE [dbo].[CompRoleValue] (
  [CompRoleValueId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [RoleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompPropValue] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CompId] numeric(19)  NULL,
  [CompPropId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CompRoleValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ConfigProfile
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ConfigProfile]') AND type IN ('U'))
	DROP TABLE [dbo].[ConfigProfile]
GO

CREATE TABLE [dbo].[ConfigProfile] (
  [ProfileId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDefault] tinyint  NULL,
  [ProfileName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ApplId] numeric(19)  NULL,
  [UserId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ConfigProfile] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ContactEvent
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactEvent]') AND type IN ('U'))
	DROP TABLE [dbo].[ContactEvent]
GO

CREATE TABLE [dbo].[ContactEvent] (
  [CustEventId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [EventDeleted] tinyint  NOT NULL,
  [MaskFieldCount] int  NOT NULL,
  [FieldList] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [EventIndex] numeric(19,2)  NULL,
  [CustEventMask] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustEventName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EventCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsReportEvent] bit  NULL,
  [IsViewable] bit  NULL,
  [IsFailureEvent] bit  NULL
)
GO

ALTER TABLE [dbo].[ContactEvent] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ContactEventTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactEventTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ContactEventTXT]
GO

CREATE TABLE [dbo].[ContactEventTXT] (
  [EventTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustEventId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ContactEventTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ContactPoint
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactPoint]') AND type IN ('U'))
	DROP TABLE [dbo].[ContactPoint]
GO

CREATE TABLE [dbo].[ContactPoint] (
  [PointId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AddrSearchKey] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PointDeleted] tinyint  NOT NULL,
  [EmailAddress] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LastOpen] datetime  NULL,
  [Latitude] float(53)  NULL,
  [Longitude] float(53)  NULL,
  [PointName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PhoneNumber] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegionName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateTXT] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContactPointType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [BusinessCalId] numeric(19)  NULL,
  [GroupId] numeric(19)  NULL,
  [TimezoneId] numeric(19)  NULL,
  [CostCenter] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExtRefKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ContactPoint] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ContactPointAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactPointAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[ContactPointAttribute]
GO

CREATE TABLE [dbo].[ContactPointAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PointId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ContactPointAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ContactPointLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactPointLog]') AND type IN ('U'))
	DROP TABLE [dbo].[ContactPointLog]
GO

CREATE TABLE [dbo].[ContactPointLog] (
  [LogEntry] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CloseUser] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Closed] datetime  NULL,
  [IsCutOver] tinyint  NULL,
  [OpenUser] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Opened] datetime  NULL,
  [Today] datetime  NULL,
  [Tomorrow] datetime  NULL,
  [PointId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ContactPointLog] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ContactPointTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactPointTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ContactPointTXT]
GO

CREATE TABLE [dbo].[ContactPointTXT] (
  [PointTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PointId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ContactPointTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ContentType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentType]') AND type IN ('U'))
	DROP TABLE [dbo].[ContentType]
GO

CREATE TABLE [dbo].[ContentType] (
  [ContentTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [VarName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Namespace] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StoreProvider] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SchemaLocation] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ContentType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Correspondence
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Correspondence]') AND type IN ('U'))
	DROP TABLE [dbo].[Correspondence]
GO

CREATE TABLE [dbo].[Correspondence] (
  [CorrespondenceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreationDate] datetime  NULL,
  [DeliveryMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Destination] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Email] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FromLoc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Media] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CorrespondenceName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CorrespondenceNum] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSignatureReq] tinyint  NULL,
  [CorrespondenceType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [URL] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Correspondence] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Credential
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Credential]') AND type IN ('U'))
	DROP TABLE [dbo].[Credential]
GO

CREATE TABLE [dbo].[Credential] (
  [CredentialId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Authority] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Category] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CountyParish] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Expires] datetime  NULL,
  [Issued] datetime  NULL,
  [HolderName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OtherSource] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartialCred] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrimaryIdInd] tinyint  NULL,
  [SeqNum] int  NULL,
  [StateProv] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SubType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdInd] tinyint  NULL,
  [Credential] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CredNumber] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookPartyId] numeric(19)  NULL,
  [Description] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReferenceKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifiedDate] datetime  NULL,
  [VerifyCode] datetime  NULL,
  [IsSkipCredentialInd] tinyint  NULL
)
GO

ALTER TABLE [dbo].[Credential] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CurrencyInfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CurrencyInfo]') AND type IN ('U'))
	DROP TABLE [dbo].[CurrencyInfo]
GO

CREATE TABLE [dbo].[CurrencyInfo] (
  [CurrencyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DenomName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MaxCashOut] int  NULL,
  [MinCashOut] int  NULL,
  [Name] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FractionDigits] numeric(19)  NULL,
  [ImageKey] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CurrencyInfo] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CustMerge
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CustMerge]') AND type IN ('U'))
	DROP TABLE [dbo].[CustMerge]
GO

CREATE TABLE [dbo].[CustMerge] (
  [CustMergeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Agent] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MergeDate] datetime  NOT NULL,
  [NewCustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [OrigCustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Reversed] tinyint  NOT NULL,
  [EventCustId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[CustMerge] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CustomerAlerts
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerAlerts]') AND type IN ('U'))
	DROP TABLE [dbo].[CustomerAlerts]
GO

CREATE TABLE [dbo].[CustomerAlerts] (
  [CustomerAlertsId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AlertAddrType] int  NOT NULL,
  [Address] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProtocolLevel] int  NOT NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CustomerAlerts] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CustomerComment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerComment]') AND type IN ('U'))
	DROP TABLE [dbo].[CustomerComment]
GO

CREATE TABLE [dbo].[CustomerComment] (
  [CustomerCommentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Comment] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [QueuedCustomerId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[CustomerComment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for CustTypeTranMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CustTypeTranMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[CustTypeTranMatrix]
GO

CREATE TABLE [dbo].[CustTypeTranMatrix] (
  [CustTypeTranMatrixId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CustTypeCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustTypeDesc] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustTypeSubCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsAllowed] tinyint  NULL,
  [TranCode] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[CustTypeTranMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DefinitionType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DefinitionType]') AND type IN ('U'))
	DROP TABLE [dbo].[DefinitionType]
GO

CREATE TABLE [dbo].[DefinitionType] (
  [DefItemId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ItemOrder] int  NULL,
  [TranId] numeric(19)  NULL,
  [ItemTypeId] numeric(19)  NULL,
  [GlAcctId] numeric(19)  NULL,
  [DefinitionClass] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaxCount] int  NULL,
  [MinCount] int  NULL,
  [IsPrimary] bit  NULL
)
GO

ALTER TABLE [dbo].[DefinitionType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Denomination
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Denomination]') AND type IN ('U'))
	DROP TABLE [dbo].[Denomination]
GO

CREATE TABLE [dbo].[Denomination] (
  [DenomId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DenomBundle] numeric(25)  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DenomName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Specie] varchar(4) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DenomValue] numeric(31,3)  NULL,
  [CurrencyId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BundleCount] int  NULL
)
GO

ALTER TABLE [dbo].[Denomination] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DenominationTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DenominationTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[DenominationTXT]
GO

CREATE TABLE [dbo].[DenominationTXT] (
  [DenominationTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DenomId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[DenominationTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Device
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Device]') AND type IN ('U'))
	DROP TABLE [dbo].[Device]
GO

CREATE TABLE [dbo].[Device] (
  [DeviceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AppName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceUID] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DeviceName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DeviceStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [DrawerId] numeric(19)  NULL,
  [PointId] numeric(19)  NULL,
  [EncryptionKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdentityKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Profile] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SessionNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Device] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DeviceAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DeviceAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[DeviceAttribute]
GO

CREATE TABLE [dbo].[DeviceAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[DeviceAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DevicePeripheral
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DevicePeripheral]') AND type IN ('U'))
	DROP TABLE [dbo].[DevicePeripheral]
GO

CREATE TABLE [dbo].[DevicePeripheral] (
  [DeviceId] numeric(19)  NOT NULL,
  [PeripheralId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[DevicePeripheral] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DeviceTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DeviceTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[DeviceTXT]
GO

CREATE TABLE [dbo].[DeviceTXT] (
  [DeviceTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DeviceId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[DeviceTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Disclosure
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Disclosure]') AND type IN ('U'))
	DROP TABLE [dbo].[Disclosure]
GO

CREATE TABLE [dbo].[Disclosure] (
  [AppDisclosureId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DisclosureData] varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DocUrn] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsRequired] tinyint  NULL,
  [IsSignatureRequired] tinyint  NULL,
  [isSigned] tinyint  NULL,
  [DisclosureName] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DisclosureType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [BookApplicationId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsTagged] bit  NULL,
  [IsArchive] bit  NULL,
  [acctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SubType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsRepeatable] bit  NULL
)
GO

ALTER TABLE [dbo].[Disclosure] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DisclosureTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DisclosureTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[DisclosureTXT]
GO

CREATE TABLE [dbo].[DisclosureTXT] (
  [DisclosureTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegFormId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[DisclosureTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Document
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Document]') AND type IN ('U'))
	DROP TABLE [dbo].[Document]
GO

CREATE TABLE [dbo].[Document] (
  [DocumentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DocBase64Data] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DocumentKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DocumentFromLoc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DocumentName] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DocumentStoreType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DocumentURL] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EnvelopeId] numeric(19)  NOT NULL,
  [RecipientId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[Document] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DocumentRecipient
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentRecipient]') AND type IN ('U'))
	DROP TABLE [dbo].[DocumentRecipient]
GO

CREATE TABLE [dbo].[DocumentRecipient] (
  [RecipientId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDt] datetime  NULL,
  [DeliveredDt] datetime  NULL,
  [DeliveryMethod] int  NULL,
  [Email] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Fax] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IDCheckByAccessCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IDCheckByConfigName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsAccessCodeRequired] tinyint  NULL,
  [IsIdCheckRequired] tinyint  NULL,
  [Reason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecipientKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutingOrder] int  NULL,
  [SentDt] datetime  NULL,
  [SignerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecipientType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EnvelopeId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[DocumentRecipient] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DocumentTab
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentTab]') AND type IN ('U'))
	DROP TABLE [dbo].[DocumentTab]
GO

CREATE TABLE [dbo].[DocumentTab] (
  [TabId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AnchorTab] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LabelName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PageNumber] numeric(19)  NULL,
  [TabName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TabtKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TabType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XPosition] numeric(19)  NULL,
  [YPosition] numeric(19)  NULL,
  [DocumentId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[DocumentTab] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DocumentVerifyRule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentVerifyRule]') AND type IN ('U'))
	DROP TABLE [dbo].[DocumentVerifyRule]
GO

CREATE TABLE [dbo].[DocumentVerifyRule] (
  [DocumentVerifyRuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsRequireDcoumentation] tinyint  NULL,
  [Reason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[DocumentVerifyRule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DocVerifyRuleAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DocVerifyRuleAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[DocVerifyRuleAttribute]
GO

CREATE TABLE [dbo].[DocVerifyRuleAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DocumentVerifyRuleId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[DocVerifyRuleAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DrawerCashLimits
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DrawerCashLimits]') AND type IN ('U'))
	DROP TABLE [dbo].[DrawerCashLimits]
GO

CREATE TABLE [dbo].[DrawerCashLimits] (
  [DrwrCashLimitId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MaxCash] numeric(31,3)  NULL,
  [MinCash] numeric(31,3)  NULL,
  [RoleId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrwrTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[DrawerCashLimits] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DrawerTotals
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DrawerTotals]') AND type IN ('U'))
	DROP TABLE [dbo].[DrawerTotals]
GO

CREATE TABLE [dbo].[DrawerTotals] (
  [TotalsId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [adjCredit] numeric(31,3)  NULL,
  [adjDebit] numeric(31,3)  NULL,
  [feesWaived] numeric(31,3)  NULL,
  [ttlCashIn] numeric(31,3)  NULL,
  [ttlCashOut] numeric(31,3)  NULL,
  [ttlFees] numeric(31,3)  NULL,
  [ttlNonCashIn] numeric(31,3)  NULL,
  [ttlNonCashOut] numeric(31,3)  NULL,
  [BalanceUser] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Balanced] datetime  NULL,
  [BegCash] numeric(31,3)  NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalsDate] datetime  NULL,
  [EndCash] numeric(31,3)  NULL,
  [LastAudited] datetime  NULL,
  [LastAuditedBy] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastSettled] datetime  NULL,
  [LastSettledBy] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastUpdated] datetime  NULL,
  [LastUpdatedBy] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNumber] numeric(19)  NULL,
  [TotalValue] numeric(31,3)  NULL,
  [DrawerId] numeric(19)  NULL,
  [TtlRetractCashAmt] numeric(31,3)  NULL,
  [TtlRemainingAmt] numeric(31,3)  NULL,
  [IsBalanced] bit  NULL,
  [IsSettled] bit  NULL
)
GO

ALTER TABLE [dbo].[DrawerTotals] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DrawerType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DrawerType]') AND type IN ('U'))
	DROP TABLE [dbo].[DrawerType]
GO

CREATE TABLE [dbo].[DrawerType] (
  [DrwrTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DrawerCurrencies] varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DrwrTypeName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrwrTypeClass] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[DrawerType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DrawerTypeTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DrawerTypeTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[DrawerTypeTXT]
GO

CREATE TABLE [dbo].[DrawerTypeTXT] (
  [DrwrTypeTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DrwrTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[DrawerTypeTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ElecJournal
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ElecJournal]') AND type IN ('U'))
	DROP TABLE [dbo].[ElecJournal]
GO

CREATE TABLE [dbo].[ElecJournal] (
  [JournalId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AtmMachineNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AtmSequenceNbr] numeric(19)  NULL,
  [AuthNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BagNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BondOrigAmt] numeric(19,2)  NULL,
  [BuyRate] numeric(15,6)  NULL,
  [CaptureIdInfo] tinyint  NULL,
  [CardName] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardType] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ConvTranAmt] numeric(31,3)  NULL,
  [ConvTranAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ConvertedBankNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DraftNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExpDt] varchar(7) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ForeignAmt] numeric(31,3)  NULL,
  [ForeignAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForeignTlrNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HostActive] tinyint  NULL,
  [InfoText] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssueTo] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiscType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NewBeginCash] numeric(31,3)  NULL,
  [NewEndCash] numeric(31,3)  NULL,
  [OldBeginCash] numeric(31,3)  NULL,
  [OldEndCash] numeric(31,3)  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PassBookBal] numeric(31,3)  NULL,
  [PassBookLineNbr] numeric(19)  NULL,
  [PastDuePmtAmt] numeric(4)  NULL,
  [PastDuePmtAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PayTo] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PmtCount] numeric(19)  NULL,
  [PmtMethod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PmtType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RateType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Reason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecourseAcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecourseAcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegPmtAmt] numeric(31,3)  NULL,
  [RegPmtAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Remitter] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SellRate] numeric(15,6)  NULL,
  [SeqNumber] numeric(19)  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SysDt] datetime  NULL,
  [AcctCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GLNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranAmt] numeric(31,3)  NULL,
  [TranAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TrnDt] datetime  NULL,
  [TranSource] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TransactorUser] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VendorReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WaiveReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XferFromAcctCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XferFromAcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XferFromAcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CorrJrnlId] numeric(19)  NULL,
  [TranId] numeric(19)  NULL,
  [DrawerId] numeric(19)  NULL,
  [OtherDrawerId] numeric(19)  NULL,
  [ParentJrnlId] numeric(19)  NULL,
  [RevJrnlId] numeric(19)  NULL,
  [AcctTypeId] numeric(19)  NULL,
  [TradeDrawerId] numeric(19)  NULL,
  [CancelReason] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChildJournalId] numeric(19)  NULL,
  [CostCenter] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ForwardRetryCount] numeric(19,2)  NULL,
  [ForwardedDate] datetime  NULL,
  [GlAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GlAmt] numeric(31,3)  NULL,
  [HostSeqNbr] numeric(19,2)  NULL,
  [IsAcctOwner] tinyint  NULL,
  [IsAcctRiskPerformed] tinyint  NULL,
  [IsApproved] tinyint  NULL,
  [IsCTRTrigger] tinyint  NULL,
  [IsCustomer] tinyint  NULL,
  [IsDifferedTran] tinyint  NULL,
  [IsExistInHotFile] tinyint  NULL,
  [IsFaultInd] tinyint  NULL,
  [IsHoldFailed] tinyint  NULL,
  [IsIncludedInPartyCTR] tinyint  NULL,
  [IsLateStoredTran] tinyint  NULL,
  [IsPmMode] tinyint  NULL,
  [IsSignatureCaptured] tinyint  NULL,
  [IsSignatureVerified] tinyint  NULL,
  [IsStoredTran] tinyint  NULL,
  [IsTrailTran] tinyint  NULL,
  [IsUCFHoldRecommended] tinyint  NULL,
  [LockedDate] datetime  NULL,
  [LockedUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MessageKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RejectedDate] datetime  NULL,
  [RejectorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RejectorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SignatureData] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SpecialInstMsg] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctDesc] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSpecialtyProduct] tinyint  NULL,
  [TaxIdNbr] varchar(12) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TomorrowsDt] datetime  NULL,
  [TotalFeeAmt] numeric(31,3)  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VarianceAmt] numeric(19,2)  NULL,
  [VarianceAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [X9ReferenceKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [X9Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XferFromAcctDesc] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XferFromFiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AggregationId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [importDefId] varchar(62) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OtherAmtDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RejectedApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RejectedDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IpcRunId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IpcTranId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MirAggregationId] numeric(19)  NULL,
  [IsMIRTrigger] bit  NULL,
  [DifferedStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MonetaryExpOverrideApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MonetaryExpOverrideDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckDataModifiedApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckDataModifiedDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecourseFiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsReverseTran] bit  NULL
)
GO

ALTER TABLE [dbo].[ElecJournal] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for EmailMessage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailMessage]') AND type IN ('U'))
	DROP TABLE [dbo].[EmailMessage]
GO

CREATE TABLE [dbo].[EmailMessage] (
  [EmailMessageId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Body] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDisable] tinyint  NOT NULL,
  [IsSystem] tinyint  NOT NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MessageKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MimeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecipientEmail] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SenderEmail] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SenderName] varchar(80) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Signature] varchar(80) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Subject] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TemplateURL] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[EmailMessage] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Employment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Employment]') AND type IN ('U'))
	DROP TABLE [dbo].[Employment]
GO

CREATE TABLE [dbo].[Employment] (
  [EmploymentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AdditionalIncomeAmt] numeric(15,2)  NULL,
  [AdditionalIncomeFreqCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AdditionalIncomeSource] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IncomeAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Employer] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IncomeAmt] numeric(15,2)  NULL,
  [IncomeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Occupation] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Phone] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SalaryFreqCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SalaryRngeCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [SinceDt] datetime  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Title] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Type] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookPartyId] numeric(19)  NULL,
  [groupBankingId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [isGroupBankingInd] tinyint  NULL,
  [IndustryType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Employment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for EnrollmentEventRpt
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EnrollmentEventRpt]') AND type IN ('U'))
	DROP TABLE [dbo].[EnrollmentEventRpt]
GO

CREATE TABLE [dbo].[EnrollmentEventRpt] (
  [Id] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoginName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RqId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Severity] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusDesc] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [System] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Type] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[EnrollmentEventRpt] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Envelope
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Envelope]') AND type IN ('U'))
	DROP TABLE [dbo].[Envelope]
GO

CREATE TABLE [dbo].[Envelope] (
  [EnvelopeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AccountId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDt] datetime  NULL,
  [DeliveredDt] datetime  NULL,
  [Email] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailBlurb] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EnvelopeKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Reason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SentDt] datetime  NULL,
  [StatusCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusDesc] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Subject] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AccessCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Envelope] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Esignature
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Esignature]') AND type IN ('U'))
	DROP TABLE [dbo].[Esignature]
GO

CREATE TABLE [dbo].[Esignature] (
  [EsignatureId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BirthDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Branch] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ConductorName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExpiryDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [finInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FromAcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdNbr] varchar(12) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssuedDate] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Issuer] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SequenceNbr] numeric(19)  NULL,
  [Signature] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ToAcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranAmt] numeric(19,2)  NULL,
  [TranDate] datetime  NULL,
  [TranReferenceNbr] numeric(19)  NULL,
  [TranTime] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Esignature] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for EventCustomer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EventCustomer]') AND type IN ('U'))
	DROP TABLE [dbo].[EventCustomer]
GO

CREATE TABLE [dbo].[EventCustomer] (
  [EventCustId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AgentId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AgentName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltKeyOne] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltKeyTwo] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Cuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FirstNames] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProsId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SessionKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustSubType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoginIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AgentType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[EventCustomer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for EventHistory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EventHistory]') AND type IN ('U'))
	DROP TABLE [dbo].[EventHistory]
GO

CREATE TABLE [dbo].[EventHistory] (
  [HistoryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Amount] float(53)  NULL,
  [Comment] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurrencyCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Duration] numeric(19)  NULL,
  [EventDate] datetime  NOT NULL,
  [EventTime] datetime  NOT NULL,
  [OrigSource] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ToAcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EventCustId] numeric(19)  NULL,
  [CustEventId] numeric(19)  NOT NULL,
  [PointId] numeric(19)  NULL,
  [TerminalId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExtRefNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HostStatus] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReqUID] varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[EventHistory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ExceptionCode
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ExceptionCode]') AND type IN ('U'))
	DROP TABLE [dbo].[ExceptionCode]
GO

CREATE TABLE [dbo].[ExceptionCode] (
  [ExceptionCodeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Code] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TxType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ExceptionMatrixId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ExceptionCode] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ExceptionMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ExceptionMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[ExceptionMatrix]
GO

CREATE TABLE [dbo].[ExceptionMatrix] (
  [ExceptionMatrixId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Expression] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Message] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ExceptionMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ExtAcct
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ExtAcct]') AND type IN ('U'))
	DROP TABLE [dbo].[ExtAcct]
GO

CREATE TABLE [dbo].[ExtAcct] (
  [PhysicalId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Email] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FIIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FirstName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstBlockAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstCity] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstCountryCd] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstPostalCd] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstRoomFloorAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstStateProv] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstStreetAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LegalName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NickName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ownership] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RoutingNum] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SourceType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SwiftCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ExtAcct] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ExtInstitution
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ExtInstitution]') AND type IN ('U'))
	DROP TABLE [dbo].[ExtInstitution]
GO

CREATE TABLE [dbo].[ExtInstitution] (
  [ExtInstId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InstInfoURL] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExtInstName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutingNum] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SwiftCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ExtInstitution] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ExtXfer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ExtXfer]') AND type IN ('U'))
	DROP TABLE [dbo].[ExtXfer]
GO

CREATE TABLE [dbo].[ExtXfer] (
  [ExtXferId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FirstName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FromAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FromAcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FromFiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LegalName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Memo] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MicroDepId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NickName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ownership] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PhysicalId] numeric(19)  NULL,
  [PostedDate] datetime  NULL,
  [RoutingNum] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SourceType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SwiftCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ToAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ToAcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ToFiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XferAmt] numeric(19,2)  NULL,
  [XferName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ExtXfer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FeatureContent
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FeatureContent]') AND type IN ('U'))
	DROP TABLE [dbo].[FeatureContent]
GO

CREATE TABLE [dbo].[FeatureContent] (
  [ContentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Deleted] tinyint  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContentName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [FeatureId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[FeatureContent] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FeatureGroup
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FeatureGroup]') AND type IN ('U'))
	DROP TABLE [dbo].[FeatureGroup]
GO

CREATE TABLE [dbo].[FeatureGroup] (
  [GroupId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Disallow] tinyint  NOT NULL,
  [FeatureName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GroupIndex] numeric(19)  NULL,
  [IsDefault] tinyint  NULL,
  [ModuleName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GroupName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ParentGroupId] numeric(19)  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[FeatureGroup] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FeatureGroupTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FeatureGroupTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[FeatureGroupTXT]
GO

CREATE TABLE [dbo].[FeatureGroupTXT] (
  [GroupTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GroupId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[FeatureGroupTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FeatureProperty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FeatureProperty]') AND type IN ('U'))
	DROP TABLE [dbo].[FeatureProperty]
GO

CREATE TABLE [dbo].[FeatureProperty] (
  [FeaturePropertyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Disallow] tinyint  NOT NULL,
  [IsDefault] tinyint  NULL,
  [IsEnableForEmployee] tinyint  NULL,
  [IsEnableForNonCustomer] tinyint  NULL,
  [FeaturePropertyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ParentPropertyId] numeric(19)  NULL,
  [PropertyClass] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyIndex] numeric(19)  NULL,
  [FeaturePropertyValue] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeatureId] numeric(19)  NULL,
  [Image] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[FeatureProperty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FeaturePropertyTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FeaturePropertyTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[FeaturePropertyTXT]
GO

CREATE TABLE [dbo].[FeaturePropertyTXT] (
  [FeaturePropertyTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeaturePropertyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[FeaturePropertyTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FeeRule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FeeRule]') AND type IN ('U'))
	DROP TABLE [dbo].[FeeRule]
GO

CREATE TABLE [dbo].[FeeRule] (
  [FeeRuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FeePeriod] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Applicability] int  NOT NULL,
  [FeeCharge] float(53)  NOT NULL,
  [CmplxRuleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeeCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Derivation] int  NOT NULL,
  [LowBound] float(53)  NULL,
  [FeeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SettlementCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TopBound] float(53)  NULL,
  [PricingModelId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[FeeRule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FieldProperty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FieldProperty]') AND type IN ('U'))
	DROP TABLE [dbo].[FieldProperty]
GO

CREATE TABLE [dbo].[FieldProperty] (
  [FieldPropertyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Disallow] tinyint  NOT NULL,
  [PropertyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PropertyClass] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyFormat] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyValue] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[FieldProperty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Financial
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Financial]') AND type IN ('U'))
	DROP TABLE [dbo].[Financial]
GO

CREATE TABLE [dbo].[Financial] (
  [FinancialId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinancialAmt] numeric(28,2)  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinancialAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FrequencyCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RangeHighAmt] numeric(28,4)  NULL,
  [RangeLowAmt] numeric(28,4)  NULL,
  [SeqNum] int  NULL,
  [Source] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinancialSubType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinancialType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatedDate] datetime  NULL,
  [BookPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Financial] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FolderContent
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FolderContent]') AND type IN ('U'))
	DROP TABLE [dbo].[FolderContent]
GO

CREATE TABLE [dbo].[FolderContent] (
  [ContentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ContentAddress] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FileName] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VarName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Namespace] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Provider] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CaseFolderId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[FolderContent] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Form
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Form]') AND type IN ('U'))
	DROP TABLE [dbo].[Form]
GO

CREATE TABLE [dbo].[Form] (
  [FormId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctUsage] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ArchiveCabinet] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ArchiveDocType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ArchiveIndex] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Boilerplate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsArchive] tinyint  NULL,
  [IsEnabled] tinyint  NULL,
  [IsSignatureReq] tinyint  NULL,
  [IsTaggedForm] tinyint  NULL,
  [FormName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ownership] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductCategory] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateCode] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SubType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FormTemplate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FormType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [MergeProcessorId] numeric(19)  NULL,
  [PrintProcessorId] numeric(19)  NULL,
  [QueryProcessorId] numeric(19)  NULL,
  [ExtensionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsRepeatable] bit  NULL
)
GO

ALTER TABLE [dbo].[Form] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FormField
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FormField]') AND type IN ('U'))
	DROP TABLE [dbo].[FormField]
GO

CREATE TABLE [dbo].[FormField] (
  [FormFieldId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FieldComment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldCondition] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Mask] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldQuery] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FormId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[FormField] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FormFundingMethodLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FormFundingMethodLink]') AND type IN ('U'))
	DROP TABLE [dbo].[FormFundingMethodLink]
GO

CREATE TABLE [dbo].[FormFundingMethodLink] (
  [FormId] numeric(19)  NOT NULL,
  [FundingMethodId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[FormFundingMethodLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FormProcessor
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FormProcessor]') AND type IN ('U'))
	DROP TABLE [dbo].[FormProcessor]
GO

CREATE TABLE [dbo].[FormProcessor] (
  [ProcessorId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Comment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorFlags] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorModule] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorType] varchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorVersion] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[FormProcessor] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FormProdLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FormProdLink]') AND type IN ('U'))
	DROP TABLE [dbo].[FormProdLink]
GO

CREATE TABLE [dbo].[FormProdLink] (
  [FormId] numeric(19)  NOT NULL,
  [ProductId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[FormProdLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FormServiceLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FormServiceLink]') AND type IN ('U'))
	DROP TABLE [dbo].[FormServiceLink]
GO

CREATE TABLE [dbo].[FormServiceLink] (
  [FormId] numeric(19)  NOT NULL,
  [ProdServiceId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[FormServiceLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FormTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FormTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[FormTXT]
GO

CREATE TABLE [dbo].[FormTXT] (
  [FormTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FormId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[FormTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FundingMethod
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FundingMethod]') AND type IN ('U'))
	DROP TABLE [dbo].[FundingMethod]
GO

CREATE TABLE [dbo].[FundingMethod] (
  [FundingMethodId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [EligibilityCd] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Image] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDisclosureRequired] tinyint  NULL,
  [Name] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [AllowedVerifyMethods] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaxAmt] numeric(31,3)  NULL,
  [MinAmt] numeric(31,3)  NULL,
  [ApplName] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[FundingMethod] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FundingMethodTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FundingMethodTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[FundingMethodTXT]
GO

CREATE TABLE [dbo].[FundingMethodTXT] (
  [FundingMethodTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FundingMethodId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[FundingMethodTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for FundingSource
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[FundingSource]') AND type IN ('U'))
	DROP TABLE [dbo].[FundingSource]
GO

CREATE TABLE [dbo].[FundingSource] (
  [FundingSourceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Amount] numeric(28,4)  NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApprovalId] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuthRsCd] varchar(12) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffDt] datetime  NULL,
  [ExpDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SkipFunding] tinyint  NULL,
  [Method] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NameOnCard] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrigSource] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcPartialAcct] varchar(4) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReconId] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SecCode] varchar(4) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [SettleDt] datetime  NULL,
  [SourceType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcAcctId] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcAcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcInstitution] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcOwner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcRoutingNum] varchar(9) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SORName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TransactionId] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [ContributionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContributionYear] varchar(4) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsAuthorizedForFunding] bit  NULL,
  [IsSecure] bit  NULL,
  [AllowedVerifyMethods] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalCashAmt] numeric(31,3)  NULL,
  [TotalCheckAmt] numeric(31,3)  NULL
)
GO

ALTER TABLE [dbo].[FundingSource] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Geography
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Geography]') AND type IN ('U'))
	DROP TABLE [dbo].[Geography]
GO

CREATE TABLE [dbo].[Geography] (
  [GeographyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GeographyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PointId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Geography] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for GlAcct
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[GlAcct]') AND type IN ('U'))
	DROP TABLE [dbo].[GlAcct]
GO

CREATE TABLE [dbo].[GlAcct] (
  [GlAcctId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctFlow] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GlAcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GlAcctName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutingNbr] varchar(9) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[GlAcct] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for GroupPage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupPage]') AND type IN ('U'))
	DROP TABLE [dbo].[GroupPage]
GO

CREATE TABLE [dbo].[GroupPage] (
  [PageId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Disallow] tinyint  NOT NULL,
  [IsDefault] tinyint  NULL,
  [PageName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PageIndex] numeric(19)  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GroupId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[GroupPage] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for GroupPageTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupPageTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[GroupPageTXT]
GO

CREATE TABLE [dbo].[GroupPageTXT] (
  [PageTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PageId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[GroupPageTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for HoldItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[HoldItem]') AND type IN ('U'))
	DROP TABLE [dbo].[HoldItem]
GO

CREATE TABLE [dbo].[HoldItem] (
  [HoldItemId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AmountHeld] numeric(19,2)  NULL,
  [ContactReason] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EwsReasonCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HoldAmount] numeric(19,2)  NULL,
  [HoldDays] int  NULL,
  [HoldReason] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HoldType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Justification] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReleaseDate] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SourceInstContact] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SourceInstName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SourceInstPhone] varchar(12) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[HoldItem] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for HotFile
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[HotFile]') AND type IN ('U'))
	DROP TABLE [dbo].[HotFile]
GO

CREATE TABLE [dbo].[HotFile] (
  [HotFileIdId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BirthDt] datetime  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SocialSecurityNbr] varchar(9) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[HotFile] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for IDAuthMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[IDAuthMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[IDAuthMatrix]
GO

CREATE TABLE [dbo].[IDAuthMatrix] (
  [IDAuthMatrixId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [InvokeIDA] tinyint  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifyFlags] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[IDAuthMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Identification
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Identification]') AND type IN ('U'))
	DROP TABLE [dbo].[Identification]
GO

CREATE TABLE [dbo].[Identification] (
  [IdentificationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExpDt] datetime  NULL,
  [IdImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssueDt] datetime  NULL,
  [IssuedIdentType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssuedIdentValue] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssuedLoc] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Issuer] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Name] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JournalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Identification] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for IdInformation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[IdInformation]') AND type IN ('U'))
	DROP TABLE [dbo].[IdInformation]
GO

CREATE TABLE [dbo].[IdInformation] (
  [JournalId] numeric(19)  NULL,
  [IdComments] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdExpires] datetime  NULL,
  [IdImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdIssuedByCountry] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdIssuedByState] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdIssuedDt] datetime  NULL,
  [IdNumber] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdVerified] tinyint  NULL,
  [Name] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdentificationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BirthDate] datetime  NULL,
  [FirstName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsPrimaryId] tinyint  NULL,
  [LastName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleInitial] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Occupation] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[IdInformation] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Institution
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Institution]') AND type IN ('U'))
	DROP TABLE [dbo].[Institution]
GO

CREATE TABLE [dbo].[Institution] (
  [FinInstId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BaseCurrency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BaseLang] varchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FedGLNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [IsSupportMultiCurrency] tinyint  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutingNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WiresRoutingNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Institution] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for InstitutionAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[InstitutionAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[InstitutionAttribute]
GO

CREATE TABLE [dbo].[InstitutionAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[InstitutionAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for InstitutionTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[InstitutionTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[InstitutionTXT]
GO

CREATE TABLE [dbo].[InstitutionTXT] (
  [InstitutionTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[InstitutionTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for InventoryItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryItem]') AND type IN ('U'))
	DROP TABLE [dbo].[InventoryItem]
GO

CREATE TABLE [dbo].[InventoryItem] (
  [InvItemId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AssignedBy] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Assigned] datetime  NULL,
  [AssignedTo] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedBy] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Created] datetime  NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EndSerialNbr] numeric(19)  NULL,
  [Fee] numeric(31,3)  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IssuedBy] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Issued] datetime  NULL,
  [ItemValue] numeric(31,3)  NULL,
  [LastUpdatedBy] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastUpdated] datetime  NULL,
  [IsManualIssue] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginalItemId] numeric(19)  NULL,
  [PackId] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PayTo] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsReIssued] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Reason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Remitter] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SerialNbr] numeric(19)  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxId] numeric(19)  NULL,
  [VoidedBy] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Voided] datetime  NULL,
  [WaiveReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrawerId] numeric(19)  NULL,
  [InvItemTypeId] numeric(19)  NULL,
  [PointId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[InventoryItem] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for InventoryItemType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryItemType]') AND type IN ('U'))
	DROP TABLE [dbo].[InventoryItemType]
GO

CREATE TABLE [dbo].[InventoryItemType] (
  [InvItemTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ItemTypeClass] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PackCount] numeric(19)  NULL,
  [PackTypeName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PackValue] numeric(31)  NULL,
  [importDefId] varchar(62) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[InventoryItemType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for InventoryItemTypeTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryItemTypeTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[InventoryItemTypeTXT]
GO

CREATE TABLE [dbo].[InventoryItemTypeTXT] (
  [ItemTypeTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [InvItemTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[InventoryItemTypeTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for IssuedIdentRule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[IssuedIdentRule]') AND type IN ('U'))
	DROP TABLE [dbo].[IssuedIdentRule]
GO

CREATE TABLE [dbo].[IssuedIdentRule] (
  [IssuedIdentRuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Citizenship] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsPrimary] tinyint  NULL,
  [IsSecondary] tinyint  NULL,
  [IssuedIdentName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IssuedIdentType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MinAge] numeric(19)  NULL,
  [IsSecondaryReq] tinyint  NULL,
  [MaxAge] numeric(19)  NULL,
  [Age] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleOrderPrimary] numeric(19)  NULL,
  [RuleOrderSecondary] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[IssuedIdentRule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for IssuedIdentRuleAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[IssuedIdentRuleAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[IssuedIdentRuleAttribute]
GO

CREATE TABLE [dbo].[IssuedIdentRuleAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssuedIdentRuleId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[IssuedIdentRuleAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Journal
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Journal]') AND type IN ('U'))
	DROP TABLE [dbo].[Journal]
GO

CREATE TABLE [dbo].[Journal] (
  [JournalId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Address] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PointName] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardNbr] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CashInAmt] numeric(19,2)  NULL,
  [CashInAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DispenseAmt] numeric(19,2)  NULL,
  [DispenseAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Dob] datetime  NULL,
  [FinInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HostActive] tinyint  NULL,
  [IsFeesAccepted] tinyint  NULL,
  [LockedDate] datetime  NULL,
  [LockedUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MsgCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrigSource] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ParentJournalId] numeric(19)  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionReasonCode] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecourseAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecourseAcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SessionId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JournalStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Tin] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalChkAmt] numeric(19,2)  NULL,
  [TotalChkAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalFeeAmt] numeric(19,2)  NULL,
  [TotalFeeAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TrackingId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranAmt] numeric(19,2)  NULL,
  [TranAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranDt] datetime  NULL,
  [TranName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranSeqNbr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranState] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Journal] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JournalDenomination
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JournalDenomination]') AND type IN ('U'))
	DROP TABLE [dbo].[JournalDenomination]
GO

CREATE TABLE [dbo].[JournalDenomination] (
  [DenominationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DenominationCount] numeric(19,2)  NULL,
  [curCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DenominationSource] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DenominationType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DenominationValue] numeric(19,2)  NULL,
  [JournalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JournalDenomination] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JournalItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JournalItem]') AND type IN ('U'))
	DROP TABLE [dbo].[JournalItem]
GO

CREATE TABLE [dbo].[JournalItem] (
  [ItemId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AddlHoldAmount] numeric(31,3)  NULL,
  [AddlHoldDays] numeric(19)  NULL,
  [AddlHoldReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BatchNbr] numeric(19)  NULL,
  [BinId] numeric(19)  NULL,
  [BondFaceValue] numeric(19,2)  NULL,
  [BondIntEarned] numeric(19,2)  NULL,
  [BondIssuedDt] varchar(7) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BondMaturityValue] numeric(19,2)  NULL,
  [BondPurchaseValue] numeric(19,2)  NULL,
  [BondQty] numeric(19)  NULL,
  [BondType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkImgBack] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkImgFront] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ConvertedBankNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemCount] numeric(19,2)  NOT NULL,
  [Currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExchangeAmt] numeric(31,3)  NULL,
  [ExchangeAmtCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeeAmt] numeric(31,3)  NULL,
  [FeeAmtCurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HoldAmount] numeric(31,3)  NULL,
  [HoldDays] numeric(19)  NULL,
  [HoldReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Justification] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SerialNbr] numeric(19)  NULL,
  [SrcAcctCur] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcRouteNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SrcGLNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemValue] numeric(31,3)  NOT NULL,
  [ItemTypeId] numeric(19)  NULL,
  [JournalId] numeric(19)  NULL,
  [AcctTypeId] numeric(19)  NULL,
  [BackImageUrl] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CapturedChkAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CapturedChkAmt] numeric(31,3)  NULL,
  [CapturedChkNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CapturedChkRouteNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CarLarDifference] numeric(19,2)  NULL,
  [CarScore] int  NULL,
  [ChargeCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ChkDate] datetime  NULL,
  [ClientItemId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FrontImageUrl] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HoldCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HoldRecommendMsg] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HostSeqNbr] numeric(19,2)  NULL,
  [IsApproved] tinyint  NULL,
  [IsBankerCorrected] tinyint  NULL,
  [IsCCSUAcct] tinyint  NULL,
  [IsCustomerCorrected] tinyint  NULL,
  [IsEWSInquiryPerformed] tinyint  NULL,
  [IsEWSNegativeResp] tinyint  NULL,
  [IsMarkedForUCF] tinyint  NULL,
  [IsNewCustomer] tinyint  NULL,
  [IsPosted] tinyint  NULL,
  [LarScore] int  NULL,
  [MicrEPC] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MicrField4] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MicrSerialNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MicrTranCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Reason] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctDesc] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FiIdent] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSpecialtyProduct] tinyint  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InfoText] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RawData] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ImageDocId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PackType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PackId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[JournalItem] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JournalItemPost
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JournalItemPost]') AND type IN ('U'))
	DROP TABLE [dbo].[JournalItemPost]
GO

CREATE TABLE [dbo].[JournalItemPost] (
  [ItemPostId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [StatusCode] numeric(19)  NULL,
  [CrAcctDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctAvailBal] numeric(19,2)  NULL,
  [CrAcctAvailBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctCurrentBal] numeric(19,2)  NULL,
  [CrAcctCurrentBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctOpenDt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctAvailBal] numeric(19,2)  NULL,
  [DrAcctAvailBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctCurrentBal] numeric(19,2)  NULL,
  [DrAcctCurrentBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctOpenDt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostDate] datetime  NULL,
  [PostDuration] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JournalItemPost] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JournalItemPostExt
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JournalItemPostExt]') AND type IN ('U'))
	DROP TABLE [dbo].[JournalItemPostExt]
GO

CREATE TABLE [dbo].[JournalItemPostExt] (
  [ItemPostExtId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [StatusCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [System] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemPostId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JournalItemPostExt] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JournalPost
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JournalPost]') AND type IN ('U'))
	DROP TABLE [dbo].[JournalPost]
GO

CREATE TABLE [dbo].[JournalPost] (
  [PostId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [StatusCode] numeric(19)  NULL,
  [CrAcctDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctAvailBal] numeric(19,2)  NULL,
  [CrAcctAvailBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctCurrentBal] numeric(19,2)  NULL,
  [CrAcctCurrentBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CrAcctOpenDt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctAvailBal] numeric(19,2)  NULL,
  [DrAcctAvailBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctCurrentBal] numeric(19,2)  NULL,
  [DrAcctCurrentBalDt] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrAcctOpenDt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostDate] datetime  NULL,
  [PostDuration] numeric(19)  NULL,
  [JournalId] numeric(19)  NULL,
  [TranName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[JournalPost] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JournalPostExt
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JournalPostExt]') AND type IN ('U'))
	DROP TABLE [dbo].[JournalPostExt]
GO

CREATE TABLE [dbo].[JournalPostExt] (
  [PostExtId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [StatusCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusMsgType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [System] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [postId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JournalPostExt] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlAttribute]
GO

CREATE TABLE [dbo].[JrnlAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JournalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JrnlAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlInventory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlInventory]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlInventory]
GO

CREATE TABLE [dbo].[JrnlInventory] (
  [JournalId] numeric(19)  NOT NULL,
  [InvItemId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[JrnlInventory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlItemDispenser
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlItemDispenser]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlItemDispenser]
GO

CREATE TABLE [dbo].[JrnlItemDispenser] (
  [DispenserId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BundleCount] numeric(19,2)  NULL,
  [Count] numeric(19,2)  NULL,
  [DenomId] numeric(19)  NULL,
  [ItemId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JrnlItemDispenser] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlItemRestriction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlItemRestriction]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlItemRestriction]
GO

CREATE TABLE [dbo].[JrnlItemRestriction] (
  [JrnlItemRestrictionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [RestrictionReason] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionReasonCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Source] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SupervisorNote] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserNote] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JrnlItemRestriction] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlItemType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlItemType]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlItemType]
GO

CREATE TABLE [dbo].[JrnlItemType] (
  [ItemTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BinedItem] tinyint  NULL,
  [ClassHandler] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ItemClass] nvarchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ItemFlow] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemTypeName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BinTypeId] numeric(19)  NULL,
  [GlAcctNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OffsetFlow] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[JrnlItemType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlItemTypeTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlItemTypeTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlItemTypeTXT]
GO

CREATE TABLE [dbo].[JrnlItemTypeTXT] (
  [ItemTypeTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ItemTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JrnlItemTypeTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlItemVerification
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlItemVerification]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlItemVerification]
GO

CREATE TABLE [dbo].[JrnlItemVerification] (
  [ItemId] numeric(19)  NULL,
  [VerificationResultCode] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationResultReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationStatus] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifiedBy] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifiedDate] datetime  NULL,
  [JrnlItemVerificationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Source] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[JrnlItemVerification] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlItemVerifyAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlItemVerifyAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlItemVerifyAttribute]
GO

CREATE TABLE [dbo].[JrnlItemVerifyAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JrnlItemVerificationId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JrnlItemVerifyAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlOrder
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlOrder]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlOrder]
GO

CREATE TABLE [dbo].[JrnlOrder] (
  [JournalId] numeric(19)  NOT NULL,
  [OrderClass] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrderCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrderType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RushDt] datetime  NULL,
  [RushInd] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShipJrnlId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[JrnlOrder] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlRestriction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlRestriction]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlRestriction]
GO

CREATE TABLE [dbo].[JrnlRestriction] (
  [JrnlRestrictionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [RestrictionReason] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionReasonCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Source] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SupervisorNote] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserNote] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JournalId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[JrnlRestriction] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for JrnlTotals
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[JrnlTotals]') AND type IN ('U'))
	DROP TABLE [dbo].[JrnlTotals]
GO

CREATE TABLE [dbo].[JrnlTotals] (
  [JrnlTtlId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [adjCredit] numeric(31,3)  NULL,
  [adjDebit] numeric(31,3)  NULL,
  [feesWaived] numeric(31,3)  NULL,
  [ttlCashIn] numeric(31,3)  NULL,
  [ttlCashOut] numeric(31,3)  NULL,
  [ttlFees] numeric(31,3)  NULL,
  [ttlNonCashIn] numeric(31,3)  NULL,
  [ttlNonCashOut] numeric(31,3)  NULL,
  [Currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TtlValue] numeric(31,3)  NULL,
  [JournalId] numeric(19)  NULL,
  [TtlRetractCashAmt] numeric(31,3)  NULL,
  [BillsInCount] int  NULL,
  [BillsOutCount] int  NULL,
  [ChkItemCount] int  NULL,
  [CoinsInCount] int  NULL,
  [CoinsOutCount] int  NULL,
  [TtlRemainingAmt] numeric(31,3)  NULL,
  [TtlMIRCashIn] numeric(31,3)  NULL
)
GO

ALTER TABLE [dbo].[JrnlTotals] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for KeywordMap
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[KeywordMap]') AND type IN ('U'))
	DROP TABLE [dbo].[KeywordMap]
GO

CREATE TABLE [dbo].[KeywordMap] (
  [KeywordMapId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [MappedCommand] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [KeywordName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MapType] int  NULL,
  [Version] numeric(19)  NULL,
  [ParentMapId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[KeywordMap] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for LcrParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[LcrParty]') AND type IN ('U'))
	DROP TABLE [dbo].[LcrParty]
GO

CREATE TABLE [dbo].[LcrParty] (
  [LcrPartyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [IsConductor] tinyint  NULL,
  [JournalId] numeric(19)  NULL,
  [AggregationId] numeric(19)  NULL,
  [CapturedBy] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranDate] datetime  NULL,
  [AltPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[LcrParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for LcrPartyTran
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[LcrPartyTran]') AND type IN ('U'))
	DROP TABLE [dbo].[LcrPartyTran]
GO

CREATE TABLE [dbo].[LcrPartyTran] (
  [LcrPartyTranId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CashInAmt] numeric(19,2)  NULL,
  [CashOutAmt] numeric(19,2)  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsIncludedInPartyCTR] tinyint  NULL,
  [ItemId] numeric(19)  NULL,
  [JournalId] numeric(19)  NULL,
  [Point] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxId] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranAmt] numeric(19,2)  NULL,
  [TranCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranDate] datetime  NULL,
  [LcrPartyId] numeric(19)  NULL,
  [PointDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[LcrPartyTran] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for LoanEntry
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[LoanEntry]') AND type IN ('U'))
	DROP TABLE [dbo].[LoanEntry]
GO

CREATE TABLE [dbo].[LoanEntry] (
  [LoanEntryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AmortizationTerm] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AmtIn30Days] numeric(20,4)  NULL,
  [AmtIn60Days] numeric(20,4)  NULL,
  [AmtOver60Days] numeric(20,4)  NULL,
  [Cap] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CommentTxt] varchar(524) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContactName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Date30] datetime  NULL,
  [Date60] datetime  NULL,
  [DateOver60] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FixedOrVariable] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Floor] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCommited] tinyint  NULL,
  [LoanAmt] numeric(20,4)  NULL,
  [LoanRate] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanStage] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanStatus] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanType] varchar(80) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Maturity] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RateType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Spread] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalRate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[LoanEntry] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for LoanEntryHistory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[LoanEntryHistory]') AND type IN ('U'))
	DROP TABLE [dbo].[LoanEntryHistory]
GO

CREATE TABLE [dbo].[LoanEntryHistory] (
  [LoanEntryHistoryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AmortizationTerm] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AmtIn30Days] numeric(20,4)  NULL,
  [AmtIn60Days] numeric(20,4)  NULL,
  [AmtOver60Days] numeric(20,4)  NULL,
  [Cap] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CommentTxt] varchar(524) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContactName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Date30] datetime  NULL,
  [Date60] datetime  NULL,
  [DateOver60] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FixedOrVariable] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Floor] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCommited] tinyint  NULL,
  [LoanAmt] numeric(20,4)  NULL,
  [LoanRate] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanStage] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanStatus] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanType] varchar(80) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Maturity] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RateType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Spread] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalRate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanEntryId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[LoanEntryHistory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for LocalContent
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalContent]') AND type IN ('U'))
	DROP TABLE [dbo].[LocalContent]
GO

CREATE TABLE [dbo].[LocalContent] (
  [LocalContentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Cuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LockId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VarName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VarValue] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[LocalContent] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ManualTaskDef
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ManualTaskDef]') AND type IN ('U'))
	DROP TABLE [dbo].[ManualTaskDef]
GO

CREATE TABLE [dbo].[ManualTaskDef] (
  [ManualTaskDefId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AlternateQueue] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DefaultQueue] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EntryPoint] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EventName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LateDur] numeric(19)  NULL,
  [TaskName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NotifyAddr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Priority] int  NULL,
  [WorkRole] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaskSeq] int  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [WarnDur] numeric(19)  NULL,
  [BookMark] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ManualTaskDef] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ManualTaskDefTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ManualTaskDefTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ManualTaskDefTXT]
GO

CREATE TABLE [dbo].[ManualTaskDefTXT] (
  [ManualTaskDefTxtId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ManualTaskDefId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ManualTaskDefTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MarketGroup
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketGroup]') AND type IN ('U'))
	DROP TABLE [dbo].[MarketGroup]
GO

CREATE TABLE [dbo].[MarketGroup] (
  [MarketGroupId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [MarketCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsMarketDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MarketGrouping] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsEnableForAdd] tinyint  NULL,
  [IsLoan] tinyint  NULL,
  [IsService] tinyint  NULL,
  [MrktLevelNo] int  NOT NULL,
  [MarketName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MrktOrderNo] int  NOT NULL,
  [MarketTree] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [IsEnableForAO] bit  NULL
)
GO

ALTER TABLE [dbo].[MarketGroup] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MarketGroupTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketGroupTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[MarketGroupTXT]
GO

CREATE TABLE [dbo].[MarketGroupTXT] (
  [MarketTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MarketGroupId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[MarketGroupTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MarketingSource
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingSource]') AND type IN ('U'))
	DROP TABLE [dbo].[MarketingSource]
GO

CREATE TABLE [dbo].[MarketingSource] (
  [MarketingSourceId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ContactNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DisplayEndDate] datetime  NULL,
  [DisplayName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [EffectiveEndDate] datetime  NULL,
  [EffectiveStartDate] datetime  NULL,
  [FinInstKey] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsNational] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MubTridianContentId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MubTridianUri] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PriorityOrder] int  NULL,
  [ReportingName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SubsequentMarketingSourceId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MediaTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[MarketingSource] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MarketingSourceGeography
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingSourceGeography]') AND type IN ('U'))
	DROP TABLE [dbo].[MarketingSourceGeography]
GO

CREATE TABLE [dbo].[MarketingSourceGeography] (
  [MarketingSourceId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GeographyId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[MarketingSourceGeography] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MediaType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MediaType]') AND type IN ('U'))
	DROP TABLE [dbo].[MediaType]
GO

CREATE TABLE [dbo].[MediaType] (
  [MediaTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DisplayName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffectiveEndDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [isDisplay] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MediaTypeName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [EffectiveStartDate] datetime  NULL
)
GO

ALTER TABLE [dbo].[MediaType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MerchantCatCode
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantCatCode]') AND type IN ('U'))
	DROP TABLE [dbo].[MerchantCatCode]
GO

CREATE TABLE [dbo].[MerchantCatCode] (
  [CatCode] int  NOT NULL,
  [ExceptionRule] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CatLabel] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CatRange] int  NULL,
  [IsReportable] tinyint  NULL
)
GO

ALTER TABLE [dbo].[MerchantCatCode] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MIRAggregation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MIRAggregation]') AND type IN ('U'))
	DROP TABLE [dbo].[MIRAggregation]
GO

CREATE TABLE [dbo].[MIRAggregation] (
  [MirAggregationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AggregationDate] datetime  NULL,
  [CashInAmt] numeric(31,3)  NULL,
  [CashInAmtCurCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsTrigger] tinyint  NULL,
  [SessionNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[MIRAggregation] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MirParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MirParty]') AND type IN ('U'))
	DROP TABLE [dbo].[MirParty]
GO

CREATE TABLE [dbo].[MirParty] (
  [MirPartyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BirthDate] datetime  NULL,
  [BusinessName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ConductorType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Dba] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailAddress] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExpDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FirstName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdIssuedBy] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsConductor] tinyint  NULL,
  [IsForeignTin] tinyint  NULL,
  [IssuedCountry] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IssuedDate] datetime  NULL,
  [IssuedState] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleInitial] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NaicsCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Occupation] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OtherId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneNbrExt] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TinCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JournalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[MirParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MiscValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MiscValue]') AND type IN ('U'))
	DROP TABLE [dbo].[MiscValue]
GO

CREATE TABLE [dbo].[MiscValue] (
  [MiscValueId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropertyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiscValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [BookApplicationId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[MiscValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for MobileTokenAudit
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[MobileTokenAudit]') AND type IN ('U'))
	DROP TABLE [dbo].[MobileTokenAudit]
GO

CREATE TABLE [dbo].[MobileTokenAudit] (
  [UID] varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Application] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckValue] smallint  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DevMode] int  NULL,
  [DevType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DevVer] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Duid] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ext1] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ext2] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ext3] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ext4] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ext5] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GeoURI] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Latitude] float(53)  NULL,
  [Longitude] float(53)  NULL,
  [Month] int  NULL,
  [MsgHash] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Quarter] int  NULL,
  [SeqNbr] numeric(22)  NULL,
  [SessionId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SvcName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TimeStamp] datetime  NULL,
  [TokenVer] numeric(19)  NULL,
  [WeekDay] int  NULL,
  [Year] int  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[MobileTokenAudit] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ModuleFeature
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ModuleFeature]') AND type IN ('U'))
	DROP TABLE [dbo].[ModuleFeature]
GO

CREATE TABLE [dbo].[ModuleFeature] (
  [FeatureId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Disallow] tinyint  NOT NULL,
  [featureClass] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [featureIndex] numeric(19)  NULL,
  [IsDefault] tinyint  NULL,
  [IsEnableForEmployee] tinyint  NULL,
  [IsEnableForNonCustomer] tinyint  NULL,
  [FeatureName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModuleId] numeric(19)  NULL,
  [Image] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ModuleFeature] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ModuleFeatureTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ModuleFeatureTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ModuleFeatureTXT]
GO

CREATE TABLE [dbo].[ModuleFeatureTXT] (
  [ModFeatureTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeatureId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ModuleFeatureTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for NAICSCode
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[NAICSCode]') AND type IN ('U'))
	DROP TABLE [dbo].[NAICSCode]
GO

CREATE TABLE [dbo].[NAICSCode] (
  [NAICSCodeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [NAICSCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NAICSCodeTXT] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NAICSGroupKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[NAICSCode] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for NeedsAssessment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[NeedsAssessment]') AND type IN ('U'))
	DROP TABLE [dbo].[NeedsAssessment]
GO

CREATE TABLE [dbo].[NeedsAssessment] (
  [AssessmentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AssessmentDt] datetime  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NextPropositionId] numeric(19)  NULL,
  [ProsId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [SalesScriptId] numeric(19)  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AccountId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[NeedsAssessment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for NewAcct
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[NewAcct]') AND type IN ('U'))
	DROP TABLE [dbo].[NewAcct]
GO

CREATE TABLE [dbo].[NewAcct] (
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ProductCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsUsed] tinyint  NULL,
  [ProductCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[NewAcct] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Node
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Node]') AND type IN ('U'))
	DROP TABLE [dbo].[Node]
GO

CREATE TABLE [dbo].[Node] (
  [NodeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [NodeKey] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BranchId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Node] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Note
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Note]') AND type IN ('U'))
	DROP TABLE [dbo].[Note]
GO

CREATE TABLE [dbo].[Note] (
  [NoteId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NoteDate] datetime  NOT NULL,
  [isDeleted] tinyint  NOT NULL,
  [NoteExpDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NoteKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [KeyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NoteSubject] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NoteText] varchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NoteType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Note] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for NotificationDevice
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationDevice]') AND type IN ('U'))
	DROP TABLE [dbo].[NotificationDevice]
GO

CREATE TABLE [dbo].[NotificationDevice] (
  [NotificationDeviceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceToken] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[NotificationDevice] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for NotificationDeviceRegister
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationDeviceRegister]') AND type IN ('U'))
	DROP TABLE [dbo].[NotificationDeviceRegister]
GO

CREATE TABLE [dbo].[NotificationDeviceRegister] (
  [NotificationRegId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Application] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDisabled] tinyint  NULL,
  [LogicalDevName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegisterDt] datetime  NOT NULL,
  [TargetURL] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UniqueDevID] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[NotificationDeviceRegister] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for NotificationLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[NotificationLog]') AND type IN ('U'))
	DROP TABLE [dbo].[NotificationLog]
GO

CREATE TABLE [dbo].[NotificationLog] (
  [NotificationLogId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Application] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AuthType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MsgExtData] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [InfoTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JournalId] numeric(19)  NULL,
  [LockedDate] datetime  NULL,
  [LockedUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LogicalDevName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MsgContext] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MsgData] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MsgKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MsgDt] datetime  NOT NULL,
  [MsgId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MsgType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PointId] numeric(19)  NULL,
  [SessionNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NotifyState] int  NOT NULL,
  [TargetURL] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranAmount] numeric(19,2)  NULL,
  [TranName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UniqueDevID] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyImageId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LockedDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[NotificationLog] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Objection
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Objection]') AND type IN ('U'))
	DROP TABLE [dbo].[Objection]
GO

CREATE TABLE [dbo].[Objection] (
  [ObjectionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AnswerType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [HighRange] numeric(19)  NULL,
  [IncrementValue] numeric(19)  NULL,
  [LowRange] numeric(19)  NULL,
  [ObjectionText] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [ExtRefKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Objection] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ObjectionTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ObjectionTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ObjectionTXT]
GO

CREATE TABLE [dbo].[ObjectionTXT] (
  [ObjectionTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ObjectionId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ObjectionTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferAuditAction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferAuditAction]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferAuditAction]
GO

CREATE TABLE [dbo].[OfferAuditAction] (
  [OfferAuditActionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ActionType] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AuditActionName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OfferAuditAction] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferContact
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferContact]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferContact]
GO

CREATE TABLE [dbo].[OfferContact] (
  [OfferContactId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BestContact] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BirthDt] varchar(12) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BizCard] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BusinessName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContactId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDeleted] tinyint  NOT NULL,
  [EmailAddress] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FirstNames] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HomePhone] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [McifId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MobilePhone] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProspectId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SearchNames] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TaxIdent] varchar(12) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [WorkPhone] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OfferContact] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferContactAttr
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferContactAttr]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferContactAttr]
GO

CREATE TABLE [dbo].[OfferContactAttr] (
  [OfferConAttrId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DecValue] numeric(22,6)  NULL,
  [DteValue] datetime  NULL,
  [AttrName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [StrValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AttrType] int  NOT NULL,
  [OfferContactId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[OfferContactAttr] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferGoal
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferGoal]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferGoal]
GO

CREATE TABLE [dbo].[OfferGoal] (
  [OfferGoalId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffDate] datetime  NULL,
  [ExpDate] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GoalCount] numeric(19)  NULL,
  [GoalValue] numeric(22,4)  NULL,
  [GroupLevel] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GoalName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Owner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OwnerType] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessType] int  NULL,
  [ProductId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Reward] numeric(20,6)  NULL,
  [RewardType] int  NULL,
  [Supervisor] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [OfferAuditActionId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OfferGoal] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferGoalTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferGoalTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferGoalTXT]
GO

CREATE TABLE [dbo].[OfferGoalTXT] (
  [OfferGoalTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OfferGoalId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[OfferGoalTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferMsgBanner
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferMsgBanner]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferMsgBanner]
GO

CREATE TABLE [dbo].[OfferMsgBanner] (
  [OfferMsgBannerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsGlobal] tinyint  NULL,
  [MsgBannerName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RoleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OfferMsgBanner] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferMsgBannerTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferMsgBannerTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferMsgBannerTXT]
GO

CREATE TABLE [dbo].[OfferMsgBannerTXT] (
  [OfferMsgBannerTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OfferMsgBannerId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[OfferMsgBannerTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferStatus
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferStatus]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferStatus]
GO

CREATE TABLE [dbo].[OfferStatus] (
  [OfferStatusId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsSystem] tinyint  NOT NULL,
  [StatusName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsTerminal] tinyint  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OfferStatus] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfferWatcher
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfferWatcher]') AND type IN ('U'))
	DROP TABLE [dbo].[OfferWatcher]
GO

CREATE TABLE [dbo].[OfferWatcher] (
  [OfferWatcherId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [EmailAddr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [WatcherType] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductOfferId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[OfferWatcher] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OfficerSubject
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OfficerSubject]') AND type IN ('U'))
	DROP TABLE [dbo].[OfficerSubject]
GO

CREATE TABLE [dbo].[OfficerSubject] (
  [OfficerSubjectId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDefault] tinyint  NULL,
  [OfficerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PointName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OfficerSubject] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OrgGroup
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OrgGroup]') AND type IN ('U'))
	DROP TABLE [dbo].[OrgGroup]
GO

CREATE TABLE [dbo].[OrgGroup] (
  [GroupId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrgGroupDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GroupLevelStr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GroupLevelNo] int  NOT NULL,
  [GroupName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [GroupOrderNo] int  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OrgGroup] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OrgGroupTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OrgGroupTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[OrgGroupTXT]
GO

CREATE TABLE [dbo].[OrgGroupTXT] (
  [GroupTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GroupId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[OrgGroupTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for OverrideMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[OverrideMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[OverrideMatrix]
GO

CREATE TABLE [dbo].[OverrideMatrix] (
  [OverrideMatrixId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [OverrideCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OverrideCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OverrideDesc] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [OverrideType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[OverrideMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for P2PPayment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[P2PPayment]') AND type IN ('U'))
	DROP TABLE [dbo].[P2PPayment]
GO

CREATE TABLE [dbo].[P2PPayment] (
  [PaymentId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AccountId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Amount] numeric(19,2)  NULL,
  [ConfirmationKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffDt] datetime  NULL,
  [EmailAddress] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MessageText] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PaymentDt] datetime  NULL,
  [PaymentMethod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PaymentType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneNum] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecipientName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SendorName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[P2PPayment] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PageField
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PageField]') AND type IN ('U'))
	DROP TABLE [dbo].[PageField]
GO

CREATE TABLE [dbo].[PageField] (
  [FieldId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Disallow] tinyint  NOT NULL,
  [FieldIndex] numeric(19)  NULL,
  [IsDefault] tinyint  NULL,
  [FieldName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FieldType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PageId] numeric(19)  NULL,
  [FieldProperty] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PageField] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PageFieldTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PageFieldTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PageFieldTXT]
GO

CREATE TABLE [dbo].[PageFieldTXT] (
  [FieldTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PageFieldTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PartyBalance
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PartyBalance]') AND type IN ('U'))
	DROP TABLE [dbo].[PartyBalance]
GO

CREATE TABLE [dbo].[PartyBalance] (
  [PartyBalanceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Total] numeric(20,4)  NULL,
  [BalanceDate] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreditCount] numeric(20)  NULL,
  [DebitCount] numeric(20)  NULL,
  [DepositCount] numeric(20)  NULL,
  [InvoiceCount] numeric(20)  NULL,
  [PaymentCount] numeric(20)  NULL,
  [TtlCreditAmt] numeric(20,4)  NULL,
  [TtlDebitAmt] numeric(20,4)  NULL,
  [TtlDepositAmt] numeric(20,4)  NULL,
  [TtlInvoiceAmt] numeric(20,4)  NULL,
  [TtlPaymentAmt] numeric(20,4)  NULL,
  [TranPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PartyBalance] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PartyContact
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PartyContact]') AND type IN ('U'))
	DROP TABLE [dbo].[PartyContact]
GO

CREATE TABLE [dbo].[PartyContact] (
  [PartyContactId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Relationship] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContactPartyId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NOT NULL,
  [PercentOwn] numeric(19,2)  NULL,
  [EntityType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PartyContact] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PartyRelVerify
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PartyRelVerify]') AND type IN ('U'))
	DROP TABLE [dbo].[PartyRelVerify]
GO

CREATE TABLE [dbo].[PartyRelVerify] (
  [PartyRelVerifyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TitleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[PartyRelVerify] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PartyRelVerifyRes
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PartyRelVerifyRes]') AND type IN ('U'))
	DROP TABLE [dbo].[PartyRelVerifyRes]
GO

CREATE TABLE [dbo].[PartyRelVerifyRes] (
  [PtyRelVerifyResId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ExtVerifyStatus] int  NULL,
  [ExtVerStatusDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinalRelationship] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecordState] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifyCodeStr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyRelVerifyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PartyRelVerifyRes] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PartyRelVerifyRole
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PartyRelVerifyRole]') AND type IN ('U'))
	DROP TABLE [dbo].[PartyRelVerifyRole]
GO

CREATE TABLE [dbo].[PartyRelVerifyRole] (
  [PtyRelVerifyRoleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Max] int  NOT NULL,
  [Min] int  NOT NULL,
  [RoleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PartyRelVerifyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PartyRelVerifyRole] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PaymentTemplate
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PaymentTemplate]') AND type IN ('U'))
	DROP TABLE [dbo].[PaymentTemplate]
GO

CREATE TABLE [dbo].[PaymentTemplate] (
  [PayTemplateId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PaymentComment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffDt] datetime  NULL,
  [EndDt] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PayFrequency] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Method] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TemplateName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PayUsage] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [AbstractId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PaymentTemplate] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Peripheral
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Peripheral]') AND type IN ('U'))
	DROP TABLE [dbo].[Peripheral]
GO

CREATE TABLE [dbo].[Peripheral] (
  [PeripheralId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PeripheralModel] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PeripheralName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PeripheralProvider] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PeripheralType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Url] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Peripheral] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PeripheralAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PeripheralAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[PeripheralAttribute]
GO

CREATE TABLE [dbo].[PeripheralAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PeripheralId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PeripheralAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PermitEntry
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PermitEntry]') AND type IN ('U'))
	DROP TABLE [dbo].[PermitEntry]
GO

CREATE TABLE [dbo].[PermitEntry] (
  [PermitEntryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [IsAcctRelated] tinyint  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [EntryAccess] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [EntryName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [EntryType] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Grouping] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LevelNo] int  NOT NULL,
  [OrderNo] int  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [PermitModuleId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PermitEntry] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PermitEntryTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PermitEntryTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PermitEntryTXT]
GO

CREATE TABLE [dbo].[PermitEntryTXT] (
  [PermitEntryTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PermitEntryId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PermitEntryTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PermitModule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PermitModule]') AND type IN ('U'))
	DROP TABLE [dbo].[PermitModule]
GO

CREATE TABLE [dbo].[PermitModule] (
  [PermitModuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ModuleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PermitModule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PermitModuleTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PermitModuleTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PermitModuleTXT]
GO

CREATE TABLE [dbo].[PermitModuleTXT] (
  [PermitModuleTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PermitModuleId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PermitModuleTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PermitSubject
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PermitSubject]') AND type IN ('U'))
	DROP TABLE [dbo].[PermitSubject]
GO

CREATE TABLE [dbo].[PermitSubject] (
  [PermitSubjectId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PermitType] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UserType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PermitSubject] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PermitValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PermitValue]') AND type IN ('U'))
	DROP TABLE [dbo].[PermitValue]
GO

CREATE TABLE [dbo].[PermitValue] (
  [PermitValueId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PermitKey] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PermitValue] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [PermitEntryId] numeric(19)  NOT NULL,
  [PermitSubjectId] numeric(19)  NOT NULL,
  [IsEnabled] tinyint  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PermitValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PfmTrnTag
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PfmTrnTag]') AND type IN ('U'))
	DROP TABLE [dbo].[PfmTrnTag]
GO

CREATE TABLE [dbo].[PfmTrnTag] (
  [PfmTrnTagId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CompiledRule] image  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [OwnerId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Priority] int  NULL,
  [RuleProcessor] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleProvider] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleText] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PfmTrnTag] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PfmTrnText
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PfmTrnText]') AND type IN ('U'))
	DROP TABLE [dbo].[PfmTrnText]
GO

CREATE TABLE [dbo].[PfmTrnText] (
  [PfmTrnTextId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PfmTrnTagId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PfmTrnText] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PhysicalAddress
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PhysicalAddress]') AND type IN ('U'))
	DROP TABLE [dbo].[PhysicalAddress]
GO

CREATE TABLE [dbo].[PhysicalAddress] (
  [PysicalAddrId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AddressKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EndDate] datetime  NULL,
  [SeqNum] int  NULL,
  [AddressSince] int  NULL,
  [StartDate] datetime  NULL,
  [AddrType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL,
  [IsSkipAddressInd] tinyint  NULL,
  [AddressChgLetterInd] bit  NULL,
  [ReoccruanceInd] bit  NULL
)
GO

ALTER TABLE [dbo].[PhysicalAddress] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PkgProdProduct
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PkgProdProduct]') AND type IN ('U'))
	DROP TABLE [dbo].[PkgProdProduct]
GO

CREATE TABLE [dbo].[PkgProdProduct] (
  [PackageId] numeric(19)  NOT NULL,
  [ProductId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[PkgProdProduct] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PreferenceValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PreferenceValue]') AND type IN ('U'))
	DROP TABLE [dbo].[PreferenceValue]
GO

CREATE TABLE [dbo].[PreferenceValue] (
  [PreferenceValueId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ValueCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ValueGroup] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ValueKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ValueName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PrefValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ValueType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserPreferencesId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PreferenceValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PricingModel
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PricingModel]') AND type IN ('U'))
	DROP TABLE [dbo].[PricingModel]
GO

CREATE TABLE [dbo].[PricingModel] (
  [PricingModelId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CalcProvider] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ModelCurCd] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ModelDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MaxCharge] numeric(4)  NULL,
  [MinCharge] numeric(4)  NULL,
  [ModelName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RuleSetName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PricingModel] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PricingModelTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PricingModelTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PricingModelTXT]
GO

CREATE TABLE [dbo].[PricingModelTXT] (
  [ModelTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PricingModelId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PricingModelTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Printer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Printer]') AND type IN ('U'))
	DROP TABLE [dbo].[Printer]
GO

CREATE TABLE [dbo].[Printer] (
  [PrinterId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Make] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [model] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Module] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrinterName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SerialNum] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrinterType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [PointId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Printer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PrinterTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PrinterTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PrinterTXT]
GO

CREATE TABLE [dbo].[PrinterTXT] (
  [PrinterTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PrinterId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PrinterTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessContent
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessContent]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessContent]
GO

CREATE TABLE [dbo].[ProcessContent] (
  [CaseProcessId] numeric(19)  NOT NULL,
  [ContentTypeId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProcessContent] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessDefinition
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessDefinition]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessDefinition]
GO

CREATE TABLE [dbo].[ProcessDefinition] (
  [CaseProcessId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AlarmEnabled] tinyint  NOT NULL,
  [EventName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExtPackages] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LinkageType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Namespace] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Operation] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Password] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Port] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecordType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SecurityType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Service] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WsdlLocation] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProcessDefinition] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessDefTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessDefTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessDefTXT]
GO

CREATE TABLE [dbo].[ProcessDefTXT] (
  [ProcessTxtId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CaseProcessId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProcessDefTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessorRule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessorRule]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessorRule]
GO

CREATE TABLE [dbo].[ProcessorRule] (
  [ProcessorRuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CompiledRule] image  NULL,
  [StoreFormat] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorRuleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleSource] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorRuleSetId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProcessorRule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessorRuleSet
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessorRuleSet]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessorRuleSet]
GO

CREATE TABLE [dbo].[ProcessorRuleSet] (
  [ProcessorRuleSetId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProviderLoadClass] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleSetName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RuleSetProvider] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProcessorRuleSet] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessorTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessorTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessorTXT]
GO

CREATE TABLE [dbo].[ProcessorTXT] (
  [ProcessorTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessorId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProcessorTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessStatus
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessStatus]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessStatus]
GO

CREATE TABLE [dbo].[ProcessStatus] (
  [ProcessStatusId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffDt] datetime  NULL,
  [Memo] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OvrdDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusCd] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [StatusDesc] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProcessUrn] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [BookApplicationId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NoteTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProcessStatus] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProcessTask
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessTask]') AND type IN ('U'))
	DROP TABLE [dbo].[ProcessTask]
GO

CREATE TABLE [dbo].[ProcessTask] (
  [CaseProcessId] numeric(19)  NOT NULL,
  [ManualTaskDefId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProcessTask] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdAppLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdAppLink]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdAppLink]
GO

CREATE TABLE [dbo].[ProdAppLink] (
  [ProductId] numeric(19)  NOT NULL,
  [ApplId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProdAppLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdAttribute]
GO

CREATE TABLE [dbo].[ProdAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] numeric(19)  NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProdAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdFeature
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdFeature]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdFeature]
GO

CREATE TABLE [dbo].[ProdFeature] (
  [ProdFeatureId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FeatureName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsOptional] tinyint  NULL,
  [FeatureType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProdFeature] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdFeatureLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdFeatureLink]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdFeatureLink]
GO

CREATE TABLE [dbo].[ProdFeatureLink] (
  [ProductId] numeric(19)  NOT NULL,
  [ProdFeatureId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProdFeatureLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdFeatureTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdFeatureTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdFeatureTXT]
GO

CREATE TABLE [dbo].[ProdFeatureTXT] (
  [ProdFeatureTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProdFeatureId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProdFeatureTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdFundMethod
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdFundMethod]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdFundMethod]
GO

CREATE TABLE [dbo].[ProdFundMethod] (
  [ProductId] numeric(19)  NOT NULL,
  [FundingMethodId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProdFundMethod] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdOfferAttr
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdOfferAttr]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdOfferAttr]
GO

CREATE TABLE [dbo].[ProdOfferAttr] (
  [ProdOfferAttrId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DecValue] numeric(22,6)  NULL,
  [DteValue] datetime  NULL,
  [AttrName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [StrValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AttrType] int  NOT NULL,
  [ProductOfferId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProdOfferAttr] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdOfferContact
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdOfferContact]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdOfferContact]
GO

CREATE TABLE [dbo].[ProdOfferContact] (
  [ProdOfferContactId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Priority] int  NOT NULL,
  [Type] int  NOT NULL,
  [OfferContactId] numeric(19)  NOT NULL,
  [ProductOfferId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProdOfferContact] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdService
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdService]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdService]
GO

CREATE TABLE [dbo].[ProdService] (
  [ProdServiceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [EligibilityCd] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDisclosureRequired] tinyint  NULL,
  [ServiceName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsOptional] tinyint  NULL,
  [Priority] int  NULL,
  [ServiceType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsComplexService] bit  NULL,
  [ApplName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsRepeatable] bit  NULL,
  [DisplayOption] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceIndex] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProdService] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdServiceLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdServiceLink]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdServiceLink]
GO

CREATE TABLE [dbo].[ProdServiceLink] (
  [ProductId] numeric(19)  NOT NULL,
  [ProdServiceId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProdServiceLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdServiceTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdServiceTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdServiceTXT]
GO

CREATE TABLE [dbo].[ProdServiceTXT] (
  [ProdServiceTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProdServiceId] numeric(19)  NULL,
  [HelpTXT] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProdServiceTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProdTranDef
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProdTranDef]') AND type IN ('U'))
	DROP TABLE [dbo].[ProdTranDef]
GO

CREATE TABLE [dbo].[ProdTranDef] (
  [ProdTranDefId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctUsage] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsBankAckReq] tinyint  NULL,
  [RelType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ProductId] numeric(19)  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProdTranDef] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Product
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type IN ('U'))
	DROP TABLE [dbo].[Product]
GO

CREATE TABLE [dbo].[Product] (
  [ProductType] char(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ProductId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctLinkType] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AcctUsage] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BaseRateName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsBundleRoot] tinyint  NULL,
  [Category] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DisplayOrder] int  NULL,
  [DocURL] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ImageURL] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IntCalcMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IntDispMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [InterestBearing] tinyint  NULL,
  [IsActive] tinyint  NULL,
  [IsEnableForAdd] tinyint  NULL,
  [IsEnableForEdit] tinyint  NULL,
  [IsLoan] tinyint  NULL,
  [IsService] tinyint  NULL,
  [MaxAmt] numeric(10,4)  NULL,
  [MinInitAmt] numeric(10,4)  NULL,
  [MultimediaURL] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ProductCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ProductKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductSubCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RateOffset] numeric(9,6)  NULL,
  [SrvChrg] numeric(9,4)  NULL,
  [StmtLinkType] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SystemCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Term] int  NULL,
  [TermUnits] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ValidEnd] datetime  NULL,
  [ValidStart] datetime  NULL,
  [Version] numeric(19)  NULL,
  [WebURL] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsRetirement] tinyint  NULL,
  [IsEnableForAO] tinyint  NULL,
  [MaxTerm] int  NULL,
  [MinTerm] int  NULL
)
GO

ALTER TABLE [dbo].[Product] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductForm
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductForm]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductForm]
GO

CREATE TABLE [dbo].[ProductForm] (
  [ProductFormId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FormName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProductForm] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductOffer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductOffer]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductOffer]
GO

CREATE TABLE [dbo].[ProductOffer] (
  [ProductOfferId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ActualCloseDate] datetime  NULL,
  [AddToLoan] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AssignedOfficer] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AssignedUser] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Branch] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Category] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CloseProbability] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedOfficer] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Cuid] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustWaitingSince] datetime  NULL,
  [EffDate] datetime  NULL,
  [EstimatedValue] numeric(20,4)  NULL,
  [Expires] datetime  NULL,
  [FeeAmount] numeric(20,2)  NULL,
  [FeeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinalValue] numeric(20,4)  NULL,
  [FollowupDate] datetime  NULL,
  [FollowupType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GroupLevel] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ImageURL] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsPackage] tinyint  NULL,
  [LeadSource] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Cap] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Collateral] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CollateralType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DepositOppAmt] numeric(20,4)  NULL,
  [FixedOrVariable] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Floor] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FundAmtAfter30] numeric(20,4)  NULL,
  [FundAmtIn30Days] numeric(20,4)  NULL,
  [LoanPurpose] varchar(80) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanRate] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanTerm] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoanType] varchar(80) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RateType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NeededBy] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProjectedCloseDate] datetime  NULL,
  [Qualification] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Role] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Stage] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StageDate] datetime  NULL,
  [StatusReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SubCategory] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [PromotionId] numeric(19)  NULL,
  [OfferStatusId] numeric(19)  NULL,
  [Maturity] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Spread] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AssignedOfficerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AssignedUserName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedOfficerName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProductOffer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductOfferAudit
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductOfferAudit]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductOfferAudit]
GO

CREATE TABLE [dbo].[ProductOfferAudit] (
  [OfferAuditId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AuditDate] datetime  NULL,
  [AuditUser] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CurrentOwner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurrentStage] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DFDay] int  NULL,
  [DFMonth] int  NULL,
  [DFQtr] int  NULL,
  [DFWeekDay] int  NULL,
  [DFMonthWeek] int  NULL,
  [DFYearWeek] int  NULL,
  [DFYear] int  NULL,
  [PreviousOwner] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PreviousStage] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserEffort] numeric(19)  NULL,
  [UserRole] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OfferAuditActionId] numeric(19)  NOT NULL,
  [AfterStatusId] numeric(19)  NOT NULL,
  [BeforeStatusId] numeric(19)  NULL,
  [ProductOfferId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProductOfferAudit] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductOfferTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductOfferTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductOfferTXT]
GO

CREATE TABLE [dbo].[ProductOfferTXT] (
  [ProductOfferTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductOfferId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProductOfferTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductRole
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductRole]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductRole]
GO

CREATE TABLE [dbo].[ProductRole] (
  [ProductRoleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [RoleName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProductRole] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductTier
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductTier]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductTier]
GO

CREATE TABLE [dbo].[ProductTier] (
  [ProductTierId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [InterestAPY] numeric(19,2)  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [Description] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Rate] numeric(10,4)  NULL,
  [RateUnit] varchar(19) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(9) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TermUnit] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TermValue] int  NULL,
  [TierMaxAmount] numeric(10,4)  NULL,
  [TierMinAmount] numeric(10,4)  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ValidEnd] datetime  NULL,
  [ValidStart] datetime  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ProductTier] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductTree
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductTree]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductTree]
GO

CREATE TABLE [dbo].[ProductTree] (
  [ProdTreeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DisplayOrder] int  NULL,
  [DisplayGroupId] numeric(19)  NULL,
  [MarketGroupId] numeric(19)  NULL,
  [ProductId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProductTree] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProductTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ProductTXT]
GO

CREATE TABLE [dbo].[ProductTXT] (
  [ProdTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProductId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProductTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfileFeature
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileFeature]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfileFeature]
GO

CREATE TABLE [dbo].[ProfileFeature] (
  [FeatureKeyId] numeric(19)  NOT NULL,
  [ModuleKeyId] numeric(19)  NOT NULL,
  [ProfileKeyId] numeric(19)  NOT NULL,
  [Enabled] tinyint  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeatureId] numeric(19)  NOT NULL,
  [ModuleId] numeric(19)  NOT NULL,
  [ProfileId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProfileFeature] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfileFeatureGroup
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileFeatureGroup]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfileFeatureGroup]
GO

CREATE TABLE [dbo].[ProfileFeatureGroup] (
  [GroupKeyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Enabled] tinyint  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GroupId] numeric(19)  NOT NULL,
  [ProfileId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProfileFeatureGroup] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfileFeatureProperty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileFeatureProperty]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfileFeatureProperty]
GO

CREATE TABLE [dbo].[ProfileFeatureProperty] (
  [FeatureKeyId] numeric(19)  NOT NULL,
  [ModuleKeyId] numeric(19)  NOT NULL,
  [ProfileKeyId] numeric(19)  NOT NULL,
  [PropertyKeyId] numeric(19)  NOT NULL,
  [Enabled] tinyint  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeatureId] numeric(19)  NOT NULL,
  [ModuleId] numeric(19)  NOT NULL,
  [ProfileId] numeric(19)  NOT NULL,
  [FeaturePropertyId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProfileFeatureProperty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfileFieldProperty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileFieldProperty]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfileFieldProperty]
GO

CREATE TABLE [dbo].[ProfileFieldProperty] (
  [FieldPropKeyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Enabled] tinyint  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldKeyId] numeric(19)  NOT NULL,
  [FieldPropertyId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProfileFieldProperty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfileGroupPage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileGroupPage]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfileGroupPage]
GO

CREATE TABLE [dbo].[ProfileGroupPage] (
  [PageKeyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Enabled] tinyint  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PageId] numeric(19)  NOT NULL,
  [GroupKeyId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProfileGroupPage] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfileModule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileModule]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfileModule]
GO

CREATE TABLE [dbo].[ProfileModule] (
  [ModuleKeyId] numeric(19)  NOT NULL,
  [ProfileKeyId] numeric(19)  NOT NULL,
  [Enabled] tinyint  NOT NULL,
  [ModuleId] numeric(19)  NOT NULL,
  [ProfileId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ProfileModule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfilePageField
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfilePageField]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfilePageField]
GO

CREATE TABLE [dbo].[ProfilePageField] (
  [FieldKeyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Enabled] tinyint  NOT NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldId] numeric(19)  NOT NULL,
  [PageKeyId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProfilePageField] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfilePageFields
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfilePageFields]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfilePageFields]
GO

CREATE TABLE [dbo].[ProfilePageFields] (
  [FieldKeyId] float(53)  NULL,
  [Enabled] float(53)  NULL,
  [Value] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FieldId] float(53)  NULL,
  [PageKeyId] float(53)  NULL
)
GO

ALTER TABLE [dbo].[ProfilePageFields] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ProfilePropValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfilePropValue]') AND type IN ('U'))
	DROP TABLE [dbo].[ProfilePropValue]
GO

CREATE TABLE [dbo].[ProfilePropValue] (
  [ProfileKeyId] numeric(19)  NOT NULL,
  [PropertyKeyId] numeric(19)  NOT NULL,
  [PropertyValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProfileId] numeric(19)  NOT NULL,
  [PropertyId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ProfilePropValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PromoChannel
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PromoChannel]') AND type IN ('U'))
	DROP TABLE [dbo].[PromoChannel]
GO

CREATE TABLE [dbo].[PromoChannel] (
  [PromoChannelId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ChannelName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PromoChannel] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PromoChannelLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PromoChannelLink]') AND type IN ('U'))
	DROP TABLE [dbo].[PromoChannelLink]
GO

CREATE TABLE [dbo].[PromoChannelLink] (
  [PromotionId] numeric(19)  NOT NULL,
  [PromoChannelId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[PromoChannelLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PromoCode
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PromoCode]') AND type IN ('U'))
	DROP TABLE [dbo].[PromoCode]
GO

CREATE TABLE [dbo].[PromoCode] (
  [PromoCodeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PromoCodeKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PromoCodeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CampaignName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StartDate] datetime  NULL,
  [EndDate] datetime  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SubType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[PromoCode] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PromoCodeProdLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PromoCodeProdLink]') AND type IN ('U'))
	DROP TABLE [dbo].[PromoCodeProdLink]
GO

CREATE TABLE [dbo].[PromoCodeProdLink] (
  [PromoCodeId] numeric(19)  NOT NULL,
  [ProductId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[PromoCodeProdLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PromoCodeTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PromoCodeTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PromoCodeTXT]
GO

CREATE TABLE [dbo].[PromoCodeTXT] (
  [PromoCodeTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PromoCodeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PromoCodeTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PromoFundingMethodLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PromoFundingMethodLink]') AND type IN ('U'))
	DROP TABLE [dbo].[PromoFundingMethodLink]
GO

CREATE TABLE [dbo].[PromoFundingMethodLink] (
  [PromoCodeId] numeric(19)  NOT NULL,
  [FundingMethodId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[PromoFundingMethodLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Promotion
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Promotion]') AND type IN ('U'))
	DROP TABLE [dbo].[Promotion]
GO

CREATE TABLE [dbo].[Promotion] (
  [PromotionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Bank] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Branch] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CampaignId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Category] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PromoCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffDt] datetime  NOT NULL,
  [ExpDt] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PromoName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Region] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Rotation] int  NULL,
  [Scale] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PromoURL] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Promotion] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PromotionTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PromotionTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PromotionTXT]
GO

CREATE TABLE [dbo].[PromotionTXT] (
  [PromotionTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PromotionId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[PromotionTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Proposition
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Proposition]') AND type IN ('U'))
	DROP TABLE [dbo].[Proposition]
GO

CREATE TABLE [dbo].[Proposition] (
  [PropositionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AnswerType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [InfoLink] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PropText] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsOptional] tinyint  NULL,
  [SearchKeys] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Subject] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ExtRefKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Proposition] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for PropositionTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[PropositionTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[PropositionTXT]
GO

CREATE TABLE [dbo].[PropositionTXT] (
  [PropositionTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PropositionId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[PropositionTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QuestionAnswer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QuestionAnswer]') AND type IN ('U'))
	DROP TABLE [dbo].[QuestionAnswer]
GO

CREATE TABLE [dbo].[QuestionAnswer] (
  [QuestionAnswerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Answer] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Name] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Priority] int  NOT NULL,
  [Question] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [Type] int  NOT NULL,
  [BookPartyId] numeric(19)  NULL,
  [RegistrationId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QuestionAnswer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedCustomer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedCustomer]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedCustomer]
GO

CREATE TABLE [dbo].[QueuedCustomer] (
  [QueuedCustomerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AltKey1] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltKey2] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltKey3] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltKey4] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltKey5] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Appointment] datetime  NULL,
  [ApptmentEnd] datetime  NULL,
  [BankerIcsData] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Confirmation] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [QueueDay] datetime  NOT NULL,
  [EmailAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EstAppDur] numeric(19)  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinalWait] numeric(19)  NULL,
  [FirstName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IcsData] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCustomer] tinyint  NULL,
  [LastName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustomerName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Notice0] varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Officer] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrderKey] numeric(19,2)  NULL,
  [OrigQueue] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OrigSource] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Phone] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [QueueType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServeTime] numeric(19)  NULL,
  [ServedAt] datetime  NULL,
  [ServiceResult] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustomerStatus] varchar(11) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Team] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [WaitingSince] datetime  NULL,
  [WeekDay] int  NOT NULL,
  [IdentifierId] numeric(19)  NULL,
  [PointId] numeric(19)  NOT NULL,
  [StatusId] numeric(19)  NULL,
  [ReasonId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedCustomer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedCustProp
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedCustProp]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedCustProp]
GO

CREATE TABLE [dbo].[QueuedCustProp] (
  [QuedCustPropId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [PropName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PropValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [QueuedCustomerId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedCustProp] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedIdentifier
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedIdentifier]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedIdentifier]
GO

CREATE TABLE [dbo].[QueuedIdentifier] (
  [IdentifierId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [IdentifierDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IdentifierName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedIdentifier] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedIdentifierTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedIdentifierTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedIdentifierTXT]
GO

CREATE TABLE [dbo].[QueuedIdentifierTXT] (
  [IdentifierTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdentifierId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedIdentifierTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedReason
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedReason]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedReason]
GO

CREATE TABLE [dbo].[QueuedReason] (
  [ReasonId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Category] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReasonDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ReasonName] varchar(80) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [QueueType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedReason] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedReasonTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedReasonTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedReasonTXT]
GO

CREATE TABLE [dbo].[QueuedReasonTXT] (
  [ReasonTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReasonId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedReasonTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedStatus
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedStatus]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedStatus]
GO

CREATE TABLE [dbo].[QueuedStatus] (
  [StatusId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [StatusDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [StatusName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedStatus] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedStatusHist
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedStatusHist]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedStatusHist]
GO

CREATE TABLE [dbo].[QueuedStatusHist] (
  [QueStatusHistId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ChgDate] datetime  NOT NULL,
  [UID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UserName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BegStatusId] numeric(19)  NULL,
  [QueuedCustomerId] numeric(19)  NOT NULL,
  [EndStatusId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[QueuedStatusHist] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedStatusTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedStatusTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedStatusTXT]
GO

CREATE TABLE [dbo].[QueuedStatusTXT] (
  [StatusTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueuedStatusTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueuedTask
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueuedTask]') AND type IN ('U'))
	DROP TABLE [dbo].[QueuedTask]
GO

CREATE TABLE [dbo].[QueuedTask] (
  [QueuedTaskId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DequeuedDt] datetime  NULL,
  [QueuedDt] datetime  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CaseTaskQueueId] numeric(19)  NOT NULL,
  [UserTaskId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[QueuedTask] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueueZone
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueueZone]') AND type IN ('U'))
	DROP TABLE [dbo].[QueueZone]
GO

CREATE TABLE [dbo].[QueueZone] (
  [ZoneId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ZoneName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NoJumpLimit] int  NOT NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueueZone] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for QueueZoneTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[QueueZoneTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[QueueZoneTXT]
GO

CREATE TABLE [dbo].[QueueZoneTXT] (
  [ZoneTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ZoneId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[QueueZoneTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RareErrorMap
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RareErrorMap]') AND type IN ('U'))
	DROP TABLE [dbo].[RareErrorMap]
GO

CREATE TABLE [dbo].[RareErrorMap] (
  [RareErrMapId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ErrorCode] numeric(19)  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Message] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RareCode] numeric(19)  NOT NULL,
  [ErrorType] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[RareErrorMap] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ReasonOfficer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ReasonOfficer]') AND type IN ('U'))
	DROP TABLE [dbo].[ReasonOfficer]
GO

CREATE TABLE [dbo].[ReasonOfficer] (
  [OfficerSubjectId] numeric(19)  NOT NULL,
  [ReasonId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ReasonOfficer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ReasonPoint
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ReasonPoint]') AND type IN ('U'))
	DROP TABLE [dbo].[ReasonPoint]
GO

CREATE TABLE [dbo].[ReasonPoint] (
  [ReasonId] numeric(19)  NOT NULL,
  [PointId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ReasonPoint] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Recipient
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Recipient]') AND type IN ('U'))
	DROP TABLE [dbo].[Recipient]
GO

CREATE TABLE [dbo].[Recipient] (
  [RecipientId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Addenda] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Amount] numeric(14,4)  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom1] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom2] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom3] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom4] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom5] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Percentage] numeric(12,6)  NULL,
  [PayTemplateId] numeric(19)  NULL,
  [RecPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Recipient] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RecipientParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RecipientParty]') AND type IN ('U'))
	DROP TABLE [dbo].[RecipientParty]
GO

CREATE TABLE [dbo].[RecipientParty] (
  [RecPartyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctNbrRef] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctFormat] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctHash] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BankName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BranchCtry] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctNbr] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutingNbr] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSecure] tinyint  NULL,
  [City] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyComment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastPrenote] datetime  NULL,
  [LastTran] datetime  NULL,
  [PartyName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NickName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyIdNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsPrenoteSent] tinyint  NULL,
  [Status] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecUsage] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Verifications] int  NULL,
  [Version] numeric(19)  NULL,
  [EmailAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UnlockAttempts] numeric(2)  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[RecipientParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ReconciliationEvent
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ReconciliationEvent]') AND type IN ('U'))
	DROP TABLE [dbo].[ReconciliationEvent]
GO

CREATE TABLE [dbo].[ReconciliationEvent] (
  [EventId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [EventAmount] numeric(19,2)  NULL,
  [EventName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EventTime] datetime  NULL,
  [EventType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [JournalId] numeric(19)  NULL,
  [AdjCredit] numeric(31,3)  NULL,
  [AdjDebit] numeric(31,3)  NULL,
  [BegCash] numeric(31,3)  NULL,
  [DrawerName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrawerType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EndCash] numeric(31,3)  NULL,
  [LastBalancedBy] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastSettledBy] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TotalsDate] datetime  NULL,
  [TtlCashIn] numeric(31,3)  NULL,
  [TtlCashOut] numeric(31,3)  NULL,
  [TotalValue] numeric(31,3)  NULL,
  [TransactorUser] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ReconciliationEvent] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegCC
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegCC]') AND type IN ('U'))
	DROP TABLE [dbo].[RegCC]
GO

CREATE TABLE [dbo].[RegCC] (
  [RegCCId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Amount] numeric(31)  NULL,
  [CheckType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [HoldDays] numeric(19)  NULL,
  [HoldReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsLocal] tinyint  NULL
)
GO

ALTER TABLE [dbo].[RegCC] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegData
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegData]') AND type IN ('U'))
	DROP TABLE [dbo].[RegData]
GO

CREATE TABLE [dbo].[RegData] (
  [RegDataId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCertified] tinyint  NULL,
  [Signature] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SignedDt] datetime  NULL,
  [BookApplicationId] numeric(19)  NOT NULL,
  [RegFormId] numeric(19)  NOT NULL,
  [BookPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[RegData] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegDataField
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegDataField]') AND type IN ('U'))
	DROP TABLE [dbo].[RegDataField]
GO

CREATE TABLE [dbo].[RegDataField] (
  [RegDataFieldId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsChecked] tinyint  NULL,
  [Value1] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value2] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegDataId] numeric(19)  NULL,
  [RegFormFieldId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[RegDataField] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegDFile
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegDFile]') AND type IN ('U'))
	DROP TABLE [dbo].[RegDFile]
GO

CREATE TABLE [dbo].[RegDFile] (
  [RegDFileId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BirthDt] datetime  NULL,
  [partyName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RestrictionExpDt] datetime  NULL,
  [SocialSecurityNbr] varchar(9) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[RegDFile] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegEMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegEMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[RegEMatrix]
GO

CREATE TABLE [dbo].[RegEMatrix] (
  [RegEMatrixId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [HasMatrixODAddlFee] tinyint  NULL,
  [HasNSFAvailBal] tinyint  NULL,
  [IsAmountOverAvailBal] tinyint  NULL,
  [IsCustAcknowledgementReq] tinyint  NULL,
  [Language] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MsgTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[RegEMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegFormField
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegFormField]') AND type IN ('U'))
	DROP TABLE [dbo].[RegFormField]
GO

CREATE TABLE [dbo].[RegFormField] (
  [RegFormFieldId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCertification] tinyint  NOT NULL,
  [Name] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RegulatoryFormId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[RegFormField] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegFormFieldTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegFormFieldTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[RegFormFieldTXT]
GO

CREATE TABLE [dbo].[RegFormFieldTXT] (
  [DisclosureTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegFormFieldId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[RegFormFieldTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Region
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Region]') AND type IN ('U'))
	DROP TABLE [dbo].[Region]
GO

CREATE TABLE [dbo].[Region] (
  [RegionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NULL,
  [RegionName] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ExtRefKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Region] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegionAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegionAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[RegionAttribute]
GO

CREATE TABLE [dbo].[RegionAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegionId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[RegionAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegionTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegionTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[RegionTXT]
GO

CREATE TABLE [dbo].[RegionTXT] (
  [RegionTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RegionId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[RegionTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Registration
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Registration]') AND type IN ('U'))
	DROP TABLE [dbo].[Registration]
GO

CREATE TABLE [dbo].[Registration] (
  [RegistrationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AppName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CipherMode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EnrollState] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Password] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [UserName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BookPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[Registration] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RegulatoryForm
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RegulatoryForm]') AND type IN ('U'))
	DROP TABLE [dbo].[RegulatoryForm]
GO

CREATE TABLE [dbo].[RegulatoryForm] (
  [RegulatoryFormId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Name] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RequireSignature] tinyint  NOT NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[RegulatoryForm] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Reminder
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Reminder]') AND type IN ('U'))
	DROP TABLE [dbo].[Reminder]
GO

CREATE TABLE [dbo].[Reminder] (
  [ReminderId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BegDt] datetime  NOT NULL,
  [CommentTXT] varchar(512) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Confirmation] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaskedAcct] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctNum] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Amount] numeric(28,2)  NULL,
  [BankName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Flow] int  NULL,
  [AcctFormat] int  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [DoNotCall] tinyint  NULL,
  [DoNotEMail] tinyint  NULL,
  [DoNotMail] tinyint  NULL,
  [EmailAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EndDt] datetime  NULL,
  [EventCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EventDesc] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EventName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [EventType] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsCustomer] tinyint  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PhoneNbr] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DayOfMonth] int  NULL,
  [MaxRecur] int  NULL,
  [PatternOption] int  NULL,
  [Pattern] int  NULL,
  [SkipCount] int  NULL,
  [WeekDayFlags] varchar(7) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Recurring] tinyint  NOT NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TriggerTm] datetime  NULL,
  [ReminderType] int  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Username] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [ParentId] numeric(19)  NULL,
  [PointId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Reminder] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ResourceDirectory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ResourceDirectory]') AND type IN ('U'))
	DROP TABLE [dbo].[ResourceDirectory]
GO

CREATE TABLE [dbo].[ResourceDirectory] (
  [ResourceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [RoleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FullName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LocalSupervisor] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LocalURI] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResourceName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SearchName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ResourceType] int  NOT NULL,
  [PointId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ResourceDirectory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RoleAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RoleAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[RoleAttribute]
GO

CREATE TABLE [dbo].[RoleAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AltRoleId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[RoleAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RoleProfile
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RoleProfile]') AND type IN ('U'))
	DROP TABLE [dbo].[RoleProfile]
GO

CREATE TABLE [dbo].[RoleProfile] (
  [RoleProfileId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RoleName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ProfileId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[RoleProfile] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RoutingNbr
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RoutingNbr]') AND type IN ('U'))
	DROP TABLE [dbo].[RoutingNbr]
GO

CREATE TABLE [dbo].[RoutingNbr] (
  [RoutingNbrId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BankName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BankNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PointId] numeric(19)  NULL,
  [RoutingNbr] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RoutingNbrType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[RoutingNbr] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SalesScript
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesScript]') AND type IN ('U'))
	DROP TABLE [dbo].[SalesScript]
GO

CREATE TABLE [dbo].[SalesScript] (
  [SalesScriptId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ScriptName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ScriptType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [PropositionId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[SalesScript] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SalesScriptTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesScriptTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[SalesScriptTXT]
GO

CREATE TABLE [dbo].[SalesScriptTXT] (
  [SalesScriptTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SalesScriptId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[SalesScriptTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Sanction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sanction]') AND type IN ('U'))
	DROP TABLE [dbo].[Sanction]
GO

CREATE TABLE [dbo].[Sanction] (
  [EntityId] numeric(19)  NOT NULL,
  [ListId] int  NOT NULL,
  [ListName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Programs] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Type] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Sanction] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SanctionAddr
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SanctionAddr]') AND type IN ('U'))
	DROP TABLE [dbo].[SanctionAddr]
GO

CREATE TABLE [dbo].[SanctionAddr] (
  [EntityId] numeric(19)  NOT NULL,
  [ListId] int  NOT NULL,
  [UID] numeric(19)  NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [other] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SanctionAddr] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SanctionIdNumber
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SanctionIdNumber]') AND type IN ('U'))
	DROP TABLE [dbo].[SanctionIdNumber]
GO

CREATE TABLE [dbo].[SanctionIdNumber] (
  [EntityId] numeric(19)  NOT NULL,
  [ListId] int  NOT NULL,
  [UID] numeric(19)  NOT NULL,
  [Expires] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdCtry] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdNumber] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Issued] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SanctionIdNumber] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SanctionName
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SanctionName]') AND type IN ('U'))
	DROP TABLE [dbo].[SanctionName]
GO

CREATE TABLE [dbo].[SanctionName] (
  [EntityId] numeric(19)  NOT NULL,
  [ListId] int  NOT NULL,
  [UID] numeric(19)  NOT NULL,
  [Alphabet] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Binding] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FormalName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NameFunction] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Gender] int  NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OtherNames] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrimaryName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Title] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SanctionName] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ScheduledTran
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ScheduledTran]') AND type IN ('U'))
	DROP TABLE [dbo].[ScheduledTran]
GO

CREATE TABLE [dbo].[ScheduledTran] (
  [SchedTranId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BillAmount] numeric(14,4)  NULL,
  [AcctNbrRef] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctFormat] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BankName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BranchCtry] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctNbr] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BillDestination] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Frequency] smallint  NULL,
  [BillPeriod] int  NOT NULL,
  [SchedComment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DayOfPeriod] smallint  NULL,
  [Effective] datetime  NULL,
  [Expires] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FundSource] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaxFailCount] int  NULL,
  [SchedTranName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NotifyAddress] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NotifyMethod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SourceOwner] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SchedTranStatus] int  NOT NULL,
  [TranFlow] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TrialAmount] numeric(14,4)  NULL,
  [TrialCancelOnFail] tinyint  NULL,
  [AcctHash] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutingNbr] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSecure] tinyint  NULL
)
GO

ALTER TABLE [dbo].[ScheduledTran] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ScriptDefTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ScriptDefTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ScriptDefTXT]
GO

CREATE TABLE [dbo].[ScriptDefTXT] (
  [ScriptDefTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ScriptDefId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ScriptDefTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ScriptDefType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ScriptDefType]') AND type IN ('U'))
	DROP TABLE [dbo].[ScriptDefType]
GO

CREATE TABLE [dbo].[ScriptDefType] (
  [ScriptDefId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AllowOverride] tinyint  NULL,
  [ScriptCategory] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [formName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ScriptName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ScriptType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ScriptDefType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ScriptFlow
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ScriptFlow]') AND type IN ('U'))
	DROP TABLE [dbo].[ScriptFlow]
GO

CREATE TABLE [dbo].[ScriptFlow] (
  [ScriptFlowId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Action] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [ActionLink] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DisplayOrder] int  NOT NULL,
  [Version] numeric(19)  NULL,
  [NextPropositionId] numeric(19)  NULL,
  [ObjectionId] numeric(19)  NOT NULL,
  [PropositionId] numeric(19)  NOT NULL,
  [SalesScriptId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ScriptFlow] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ScriptHistory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ScriptHistory]') AND type IN ('U'))
	DROP TABLE [dbo].[ScriptHistory]
GO

CREATE TABLE [dbo].[ScriptHistory] (
  [ScriptHistoryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Comment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AssessmentId] numeric(19)  NULL,
  [ObjectionId] numeric(19)  NULL,
  [PropositionId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ScriptHistory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SearchKey
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchKey]') AND type IN ('U'))
	DROP TABLE [dbo].[SearchKey]
GO

CREATE TABLE [dbo].[SearchKey] (
  [SearchKeyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Deleted] tinyint  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SearchKeyName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SearchKeyType] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[SearchKey] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SecMsgAttach
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SecMsgAttach]') AND type IN ('U'))
	DROP TABLE [dbo].[SecMsgAttach]
GO

CREATE TABLE [dbo].[SecMsgAttach] (
  [SecMsgAttachId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Content] image  NULL,
  [ContentType] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FileName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsLocation] tinyint  NULL,
  [SecMsgId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[SecMsgAttach] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SecureMessage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SecureMessage]') AND type IN ('U'))
	DROP TABLE [dbo].[SecureMessage]
GO

CREATE TABLE [dbo].[SecureMessage] (
  [SecMsgId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Content] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Department] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SecMsgFlow] int  NOT NULL,
  [FwdFromUser] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FwdFromName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MsgDate] datetime  NOT NULL,
  [ReplyMsgId] numeric(19)  NULL,
  [SecMsgState] int  NOT NULL,
  [Subject] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatedDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SecureMessage] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Service
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Service]') AND type IN ('U'))
	DROP TABLE [dbo].[Service]
GO

CREATE TABLE [dbo].[Service] (
  [ServiceId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BusDayOption] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CardImageId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DayOfMonth] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EmailAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Frequency] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StmtInterval] numeric(19)  NULL,
  [ServiceName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NickName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PinBlock] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [ServiceStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StmtGrp] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SORName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [BookPartyId] numeric(19)  NULL,
  [ReferenceKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceDesc] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Service] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ServiceTran
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ServiceTran]') AND type IN ('U'))
	DROP TABLE [dbo].[ServiceTran]
GO

CREATE TABLE [dbo].[ServiceTran] (
  [ServiceTranId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TranName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ServiceTran] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ServiceTranTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ServiceTranTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[ServiceTranTXT]
GO

CREATE TABLE [dbo].[ServiceTranTXT] (
  [ServiceTranTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServiceTranId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ServiceTranTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SessionPartyImage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionPartyImage]') AND type IN ('U'))
	DROP TABLE [dbo].[SessionPartyImage]
GO

CREATE TABLE [dbo].[SessionPartyImage] (
  [SessionPartyImageId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyImage] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SessionNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranDate] datetime  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SessionPartyImage] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SettingCategory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SettingCategory]') AND type IN ('U'))
	DROP TABLE [dbo].[SettingCategory]
GO

CREATE TABLE [dbo].[SettingCategory] (
  [SettingCatId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CategoryName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SettingCategory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SettingCatTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SettingCatTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[SettingCatTXT]
GO

CREATE TABLE [dbo].[SettingCatTXT] (
  [SettingCatTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SettingCatId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[SettingCatTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SettingEntry
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SettingEntry]') AND type IN ('U'))
	DROP TABLE [dbo].[SettingEntry]
GO

CREATE TABLE [dbo].[SettingEntry] (
  [SettingEntryId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ValueDataType] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsDeleted] tinyint  NOT NULL,
  [Grouping] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LevelNo] int  NOT NULL,
  [OrderNo] int  NOT NULL,
  [EntryName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [SettingCatId] numeric(19)  NULL,
  [EntryType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeatureName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SettingEntry] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SettingEntryTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SettingEntryTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[SettingEntryTXT]
GO

CREATE TABLE [dbo].[SettingEntryTXT] (
  [SettingEntryTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SettingEntryId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[SettingEntryTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SettingSubject
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SettingSubject]') AND type IN ('U'))
	DROP TABLE [dbo].[SettingSubject]
GO

CREATE TABLE [dbo].[SettingSubject] (
  [SettingSubjectId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SettingType] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UserType] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SettingSubject] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SettingValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SettingValue]') AND type IN ('U'))
	DROP TABLE [dbo].[SettingValue]
GO

CREATE TABLE [dbo].[SettingValue] (
  [SettingValueId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsEnabled] tinyint  NOT NULL,
  [SettingKey] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SettingValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [SettingEntryId] numeric(19)  NOT NULL,
  [SettingSubjectId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SettingValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowAcctDtls
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowAcctDtls]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowAcctDtls]
GO

CREATE TABLE [dbo].[ShadowAcctDtls] (
  [AcctDtlsId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [acctNbr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [acctStatus] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [acctType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [checkAmount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [closeReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [closingLedgerBalance] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [currentBalance] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [currentInterestRate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [currentTerm] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [disbursementAccount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [disbursmentType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [extAcctInd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ledgerBalance] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [memo] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [notes] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [notesInd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [payee1] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [permanentHoldInd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [relType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [restrictionsInd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [stopPaymentInd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowAcctDtls] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowAcctFees
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowAcctFees]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowAcctFees]
GO

CREATE TABLE [dbo].[ShadowAcctFees] (
  [AcctFeesId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [feeType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [fromDate] datetime  NULL,
  [postedDt] datetime  NULL,
  [reason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [toDate] datetime  NULL,
  [totalAmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [trnId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowAcctFees] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowAcctHold
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowAcctHold]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowAcctHold]
GO

CREATE TABLE [dbo].[ShadowAcctHold] (
  [AcctHoldId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [amount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [balanceType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [effectiveDt] datetime  NULL,
  [holdReason] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [holdType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [initiatingParty] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [itemAmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [seqNum] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [startDt] datetime  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowAcctHold] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowAcctNotes
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowAcctNotes]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowAcctNotes]
GO

CREATE TABLE [dbo].[ShadowAcctNotes] (
  [AcctNotesId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [expiryDate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [expiryType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [specialInstruction] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowAcctNotes] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowAddr
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowAddr]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowAddr]
GO

CREATE TABLE [dbo].[ShadowAddr] (
  [AddrId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [bankID] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [bankName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [block] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [city] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [country] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [line1] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [line2] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [postalCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [stateProv] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [street] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [type] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowAddr] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowCDMatRenewals
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowCDMatRenewals]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowCDMatRenewals]
GO

CREATE TABLE [dbo].[ShadowCDMatRenewals] (
  [CDMatRenewalsId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [additionalFunds] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [apy] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [currentMatDt] datetime  NULL,
  [currentProduct] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [disbursementMethod] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [fundingAccount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [fundingMethod] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [interestDistMethod] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [maturityDt] datetime  NULL,
  [netWithdrawalAmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [newMaturityDt] datetime  NULL,
  [newProduct] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [penaltyAmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [rate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [remainingBal] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [renewalAmount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [renewalDt] datetime  NULL,
  [reviewComments] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [targetAcct] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [transferToAcct] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [waivePenalty] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [withdrawalAmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [withdrawalDt] datetime  NULL,
  [withdrawalType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowCDMatRenewals] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowExtAcctDtls
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowExtAcctDtls]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowExtAcctDtls]
GO

CREATE TABLE [dbo].[ShadowExtAcctDtls] (
  [ExtAcctDtlsId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [applName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [deleteOwnerTxt1] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [instName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [legalName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [nickName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [routingNum] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sourceType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [userFullName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [verificationMethod] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowExtAcctDtls] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowFieldValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowFieldValue]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowFieldValue]
GO

CREATE TABLE [dbo].[ShadowFieldValue] (
  [ShadowFieldId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FieldName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FieldValue] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowFieldValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowFunding
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowFunding]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowFunding]
GO

CREATE TABLE [dbo].[ShadowFunding] (
  [FundingId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [amt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [bankName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [beneAcctNbr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [beneBankID] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [beneName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [curCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [debitAccount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [frequency] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [fromAcctFiIdent] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [fromAcctId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [fromAcctType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [paymentMethod] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [purposeOfWire] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [toAcctFiIdent] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [toAcctId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [toAcctType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [toBankName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [valueDt] datetime  NULL,
  [wireAmount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowFunding] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowParty]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowParty]
GO

CREATE TABLE [dbo].[ShadowParty] (
  [PartyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [birthDt] datetime  NULL,
  [custName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [emailAddr] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [emailType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [firstName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [lastName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [partyType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [phoneNum] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [phoneType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowProductDtls
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowProductDtls]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowProductDtls]
GO

CREATE TABLE [dbo].[ShadowProductDtls] (
  [ProductDtlsId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [productCode] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [productName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [termUnit] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [termValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tierMaxAmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tierMinAmt] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowProductDtls] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShadowStmtDtls
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShadowStmtDtls]') AND type IN ('U'))
	DROP TABLE [dbo].[ShadowStmtDtls]
GO

CREATE TABLE [dbo].[ShadowStmtDtls] (
  [StmtDtlsId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [acctId] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [acctType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [businessDayOption] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [dayOfMonth] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [nextStmtDt] datetime  NULL,
  [nickName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [primaryAccount] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [setBusinessDayOption] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [setDecrementDays] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [setDesc] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [setFreq] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [setIncrementDays] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [statementAddress] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [stmtGroup] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShadowContentId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ShadowStmtDtls] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ShoppingCart
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingCart]') AND type IN ('U'))
	DROP TABLE [dbo].[ShoppingCart]
GO

CREATE TABLE [dbo].[ShoppingCart] (
  [ShoppingCartId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BegDt] datetime  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EndDt] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Name] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ProsId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ShoppingCart] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SrchKeyValue
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SrchKeyValue]') AND type IN ('U'))
	DROP TABLE [dbo].[SrchKeyValue]
GO

CREATE TABLE [dbo].[SrchKeyValue] (
  [SrchKeyValueId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DblValue] float(53)  NULL,
  [DteValue] datetime  NULL,
  [IntValue] int  NULL,
  [StrValue] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CaseFolderId] numeric(19)  NULL,
  [SearchKeyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[SrchKeyValue] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ssis_log
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ssis_log]') AND type IN ('U'))
	DROP TABLE [dbo].[ssis_log]
GO

CREATE TABLE [dbo].[ssis_log] (
  [SSIS_Package] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RunTime] datetime  NULL,
  [RecordsProcessed] int  NULL,
  [FileName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[ssis_log] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SubUserProfile
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SubUserProfile]') AND type IN ('U'))
	DROP TABLE [dbo].[SubUserProfile]
GO

CREATE TABLE [dbo].[SubUserProfile] (
  [SubUserId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ContactEmail] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ContactPhone] varchar(24) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DailyTranLimit] numeric(26,4)  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FirstName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsAdmin] tinyint  NOT NULL,
  [LastName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoginName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SubUserStatus] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifyNum] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [ExtUserId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsPermitFlag] tinyint  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SubUserProfile] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SvcTranFee
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SvcTranFee]') AND type IN ('U'))
	DROP TABLE [dbo].[SvcTranFee]
GO

CREATE TABLE [dbo].[SvcTranFee] (
  [SvcTranFeeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CurCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Derivation] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FeeAmt] numeric(25,4)  NULL,
  [FinInstKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsApprovalReqForWaive] tinyint  NULL,
  [IsWaive] tinyint  NULL,
  [MaxAmt] numeric(31,4)  NULL,
  [MinAmt] numeric(31,4)  NULL,
  [PointName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCode] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ValidFrDt] datetime  NOT NULL,
  [ValidToDt] datetime  NULL,
  [Version] numeric(19)  NULL,
  [TranFeeTypeId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDisabled] bit  NULL,
  [GlAcctId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SvcTranFee] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for sysssislog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[sysssislog]') AND type IN ('U'))
	DROP TABLE [dbo].[sysssislog]
GO

CREATE TABLE [dbo].[sysssislog] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [event] sysname  NOT NULL,
  [computer] nvarchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [operator] nvarchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [source] nvarchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [sourceid] uniqueidentifier  NOT NULL,
  [executionid] uniqueidentifier  NOT NULL,
  [starttime] datetime  NOT NULL,
  [endtime] datetime  NOT NULL,
  [datacode] int  NOT NULL,
  [databytes] image  NULL,
  [message] nvarchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[sysssislog] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SystemEnv
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SystemEnv]') AND type IN ('U'))
	DROP TABLE [dbo].[SystemEnv]
GO

CREATE TABLE [dbo].[SystemEnv] (
  [SystemEnvId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FarmType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Hardware] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Host] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HostURL] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PingMethod] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PingURL] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServerIPAddress] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ServerName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Params] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SystemEnv] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for SystemVersion
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SystemVersion]') AND type IN ('U'))
	DROP TABLE [dbo].[SystemVersion]
GO

CREATE TABLE [dbo].[SystemVersion] (
  [SystemVersionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Application] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Sprint] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Build] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeployScript] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Checksum] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RollbackScript] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Owner] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SystemVersion] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TargetCriteria
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TargetCriteria]') AND type IN ('U'))
	DROP TABLE [dbo].[TargetCriteria]
GO

CREATE TABLE [dbo].[TargetCriteria] (
  [TargetCriteriaId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FieldOperand] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FieldType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LinkOf] tinyint  NOT NULL,
  [ValueOperand] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Operator] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CriteriaOrder] int  NOT NULL,
  [TargetGroupId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[TargetCriteria] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TargetGroup
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TargetGroup]') AND type IN ('U'))
	DROP TABLE [dbo].[TargetGroup]
GO

CREATE TABLE [dbo].[TargetGroup] (
  [TargetGroupId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Memo] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GroupName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [PromotionId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TargetGroup] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TaxPlan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TaxPlan]') AND type IN ('U'))
	DROP TABLE [dbo].[TaxPlan]
GO

CREATE TABLE [dbo].[TaxPlan] (
  [TaxPlanId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsExistingPlan] tinyint  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxPlanCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxPlanDesc] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AutoStartDistCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DailyFirstDistCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DistCalcMethodCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DistFreq] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DistFreqCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DistInterestCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MinRDAAgeOverideCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OwnerDistCalcMethodCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecalcDistAmtCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReducedLastDistCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [WaiveDistCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryDistMethod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OwnerPartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SoleSpousBenf] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SpousePlanCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxPlanType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [FullName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GivenName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LegalName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SurName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BirthDt] datetime  NULL,
  [IsSkipPlanInd] tinyint  NULL,
  [DeceasedDt] datetime  NULL,
  [ReferenceKey] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LastYearRoth] numeric(10)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TaxPlan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TaxPlanBen
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TaxPlanBen]') AND type IN ('U'))
	DROP TABLE [dbo].[TaxPlanBen]
GO

CREATE TABLE [dbo].[TaxPlanBen] (
  [TaxPlanBenId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryPercentage] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryRel] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BirthDt] datetime  NULL,
  [GivenName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FullName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LegalName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [Suffix] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SurName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Title] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Type] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxPlanId] numeric(19)  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiarySpouseCd] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IncludeBenfOnMail] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Gender] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryPartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsExistingBeneficiary] tinyint  NULL,
  [IsSkipBeneficiaryInd] tinyint  NULL,
  [DeceasedDt] datetime  NULL
)
GO

ALTER TABLE [dbo].[TaxPlanBen] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TaxPlanContingenBen
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TaxPlanContingenBen]') AND type IN ('U'))
	DROP TABLE [dbo].[TaxPlanContingenBen]
GO

CREATE TABLE [dbo].[TaxPlanContingenBen] (
  [TaxPlanContingenBenId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryPartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryPercentage] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiaryRel] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BeneficiarySpouseCd] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BirthDt] datetime  NULL,
  [DathDt] datetime  NULL,
  [FullName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Gender] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [GivenName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IncludeBenfOnMail] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsExistingBeneficiary] tinyint  NULL,
  [IsSkipBeneficiaryInd] tinyint  NULL,
  [LegalName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MiddleName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [Suffix] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SurName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Title] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Type] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxPlanBenId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TaxPlanContingenBen] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TaxPlanEmployer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TaxPlanEmployer]') AND type IN ('U'))
	DROP TABLE [dbo].[TaxPlanEmployer]
GO

CREATE TABLE [dbo].[TaxPlanEmployer] (
  [TaxPlanEmployerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BlockAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoomFloorAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LegalName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [TaxId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Type] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxPlanId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TaxPlanEmployer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TerminalNotificationActivity
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TerminalNotificationActivity]') AND type IN ('U'))
	DROP TABLE [dbo].[TerminalNotificationActivity]
GO

CREATE TABLE [dbo].[TerminalNotificationActivity] (
  [TerminalNotificationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [DeviceId] numeric(19)  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [NotificationLogId] numeric(19)  NULL,
  [Status] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TargetURL] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TerminalNotificationActivity] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Timezone
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Timezone]') AND type IN ('U'))
	DROP TABLE [dbo].[Timezone]
GO

CREATE TABLE [dbo].[Timezone] (
  [TimezoneId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ZoneCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DaylightSavingsInd] tinyint  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsDeleted] tinyint  NULL,
  [ZoneName] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Offset] numeric(19)  NULL,
  [ShortCodeWDS] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ShortCodeWODS] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Timezone] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TimezoneTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TimezoneTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[TimezoneTXT]
GO

CREATE TABLE [dbo].[TimezoneTXT] (
  [TimezoneTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TimezoneId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TimezoneTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TitleGeneration
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TitleGeneration]') AND type IN ('U'))
	DROP TABLE [dbo].[TitleGeneration]
GO

CREATE TABLE [dbo].[TitleGeneration] (
  [TitleGenerationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Connector] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsTitleLineReq] tinyint  NULL,
  [Language] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LineNbr] numeric(19,2)  NULL,
  [Prefix] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Special] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateCode] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Suffix] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [TitleRelationshipMatrixId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TitleGeneration] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TitleOption
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TitleOption]') AND type IN ('U'))
	DROP TABLE [dbo].[TitleOption]
GO

CREATE TABLE [dbo].[TitleOption] (
  [TitleOptionId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctUsage] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AllStatesInd] tinyint  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsCommonTitle] tinyint  NULL,
  [ProdCategory] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TitleTypeName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TitleOption] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TitleOptionTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TitleOptionTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[TitleOptionTXT]
GO

CREATE TABLE [dbo].[TitleOptionTXT] (
  [TitleOptionTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TitleOptionId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TitleOptionTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TitleProdLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TitleProdLink]') AND type IN ('U'))
	DROP TABLE [dbo].[TitleProdLink]
GO

CREATE TABLE [dbo].[TitleProdLink] (
  [TitleOptionId] numeric(19)  NOT NULL,
  [ProductId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[TitleProdLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TitleRelationshipMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TitleRelationshipMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[TitleRelationshipMatrix]
GO

CREATE TABLE [dbo].[TitleRelationshipMatrix] (
  [TitleRelationshipMatrixId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsCreditCheckReq] tinyint  NOT NULL,
  [IsEWSVerificationReq] tinyint  NULL,
  [IsEligibleForServices] tinyint  NULL,
  [IsExternalVerificationReq] tinyint  NULL,
  [IsFraudCheckReq] tinyint  NULL,
  [IsHotFileCheckReq] tinyint  NULL,
  [IsIDVVerificationReq] tinyint  NOT NULL,
  [IsInternalVerificationReq] tinyint  NULL,
  [IsKYCReq] tinyint  NOT NULL,
  [IsMustBeInFootprint] tinyint  NULL,
  [IsPDMVerificationReq] tinyint  NULL,
  [IsPartyRecordReq] tinyint  NOT NULL,
  [IsPrimaryOwner] tinyint  NOT NULL,
  [IsRegDCheckReq] tinyint  NULL,
  [IsRelatedParty] tinyint  NOT NULL,
  [IsSigner] tinyint  NULL,
  [IsTermsCondReq] tinyint  NULL,
  [IsTinOnTitleReq] tinyint  NOT NULL,
  [LegalDesignation] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyRelCode] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [PartyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RoleMax] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoleMin] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [TitleOptionId] numeric(19)  NULL,
  [IsReceiveEmail] tinyint  NULL,
  [MinAge] numeric(19)  NULL,
  [MaxAge] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TitleRelationshipMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TitleRelationshipTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TitleRelationshipTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[TitleRelationshipTXT]
GO

CREATE TABLE [dbo].[TitleRelationshipTXT] (
  [TitleRelationshipTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TitleRelationshipMatrixId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TitleRelationshipTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TraceAuditSeq
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TraceAuditSeq]') AND type IN ('U'))
	DROP TABLE [dbo].[TraceAuditSeq]
GO

CREATE TABLE [dbo].[TraceAuditSeq] (
  [TraceAuditId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MaxSeq] numeric(20)  NOT NULL,
  [TraceAuditName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TraceSeq] numeric(20)  NOT NULL,
  [Version] datetime  NULL
)
GO

ALTER TABLE [dbo].[TraceAuditSeq] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranAcctTypeLink
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranAcctTypeLink]') AND type IN ('U'))
	DROP TABLE [dbo].[TranAcctTypeLink]
GO

CREATE TABLE [dbo].[TranAcctTypeLink] (
  [TranAcctTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctTypeOrder] int  NULL,
  [AcctTypeId] numeric(19)  NULL,
  [TranId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TranAcctTypeLink] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranApprovalLimits
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranApprovalLimits]') AND type IN ('U'))
	DROP TABLE [dbo].[TranApprovalLimits]
GO

CREATE TABLE [dbo].[TranApprovalLimits] (
  [TranApprovalLimitId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaxCashIn] numeric(31,3)  NULL,
  [MaxCashOut] numeric(31,3)  NULL,
  [MaxFeesWaived] numeric(31,3)  NULL,
  [MaxTotalValue] numeric(31,3)  NULL,
  [TranMode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoleId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TranApprovalLimits] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranAudit
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranAudit]') AND type IN ('U'))
	DROP TABLE [dbo].[TranAudit]
GO

CREATE TABLE [dbo].[TranAudit] (
  [JournalId] numeric(19)  NOT NULL,
  [AuthType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Environment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Language] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [NetworkAddress] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [OriginatorName] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReceiptDeliveryMethod] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Role] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SessionNbr] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TerminalType] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranStart] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuthenticatedDate] datetime  NULL,
  [AuthenticatedUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuthenticatedUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckDataModifiedDate] datetime  NULL,
  [CheckDataModifiedUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckDataModifiedUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffectiveDt] datetime  NULL,
  [IsADATran] tinyint  NULL,
  [IsManualAcctInd] tinyint  NULL,
  [LimitsOverrideDate] datetime  NULL,
  [LimitsOverrideUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LimitsOverrideUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MonetaryExpOverrideDate] datetime  NULL,
  [MonetaryExpOverrideUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MonetaryExpOverrideUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReqUID] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuthenticatedApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuthenticatedDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MonetaryExpOverrideApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MonetaryExpOverrideDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckDataModifiedApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CheckDataModifiedDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LimitsOverrideApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LimitsOverrideDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PointName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Region] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsNewAcct] bit  NULL,
  [TraceId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MultiSessionNbr] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranMode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuditorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuditorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuditedDate] datetime  NULL,
  [AuditorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AuditorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TranAudit] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranCategory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCategory]') AND type IN ('U'))
	DROP TABLE [dbo].[TranCategory]
GO

CREATE TABLE [dbo].[TranCategory] (
  [TranCatId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TranCatName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CategoryOrder] int  NULL
)
GO

ALTER TABLE [dbo].[TranCategory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranCodeMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCodeMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[TranCodeMatrix]
GO

CREATE TABLE [dbo].[TranCodeMatrix] (
  [TranCodeMatrixId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SourceTranCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [SystemType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TargetTranCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TranCodeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TranFLow] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TranCodeMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranDefAttribute
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranDefAttribute]') AND type IN ('U'))
	DROP TABLE [dbo].[TranDefAttribute]
GO

CREATE TABLE [dbo].[TranDefAttribute] (
  [AttributeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AttributeName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [AttributeType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Value] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TranDefAttribute] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranFeeType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranFeeType]') AND type IN ('U'))
	DROP TABLE [dbo].[TranFeeType]
GO

CREATE TABLE [dbo].[TranFeeType] (
  [TranFeeTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Category] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FeeName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FeeType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TranFeeType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranFeeTypeTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranFeeTypeTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[TranFeeTypeTXT]
GO

CREATE TABLE [dbo].[TranFeeTypeTXT] (
  [TranFeeTypeTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranFeeTypeId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TranFeeTypeTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranItemConstraintRule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranItemConstraintRule]') AND type IN ('U'))
	DROP TABLE [dbo].[TranItemConstraintRule]
GO

CREATE TABLE [dbo].[TranItemConstraintRule] (
  [TranItemConstraintRuleId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsPrimary] tinyint  NULL,
  [ItemSubType] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ItemType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [MaxCount] numeric(19)  NULL,
  [pointName] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TargetItemSubType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TargetItemType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCode] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[TranItemConstraintRule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranJournal
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranJournal]') AND type IN ('U'))
	DROP TABLE [dbo].[TranJournal]
GO

CREATE TABLE [dbo].[TranJournal] (
  [TranJournalId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [TranAmount] numeric(14,4)  NULL,
  [Committed] datetime  NULL,
  [TranCurrency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Effective] datetime  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [OrigAmount] numeric(14,4)  NULL,
  [RecvAmount] numeric(14,4)  NULL,
  [RecvFlow] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RevTranId] numeric(19)  NULL,
  [Settled] datetime  NULL,
  [Status] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FileId] numeric(19)  NULL,
  [SessId] numeric(19)  NULL,
  [TranType] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Transmitted] datetime  NULL,
  [TtlFees] float(53)  NULL,
  [OrigPartyId] numeric(19)  NULL,
  [RecvPartyId] numeric(19)  NULL,
  [Addenda] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplRefId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApprovalCount] int  NULL,
  [ApprovalsReqd] int  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom1] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom2] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom3] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom4] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Custom5] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Ownership] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyIdNbr] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RqUID] varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TemplateName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecipientId] numeric(19)  NULL,
  [TemplateId] numeric(19)  NULL,
  [Method] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PayUsage] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Version] numeric(19)  NULL,
  [FileName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TranJournal] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranParty
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranParty]') AND type IN ('U'))
	DROP TABLE [dbo].[TranParty]
GO

CREATE TABLE [dbo].[TranParty] (
  [TranPartyId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AcctNbrRef] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctFormat] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BankName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BranchCtry] varchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctNbr] varchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [City] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Country] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PostalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StateProvince] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StreetAddr] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranPartyName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PartyType] int  NULL,
  [AcctHash] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AcctType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoutingNbr] varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSecure] tinyint  NULL,
  [TranComment] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TranParty] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TransactionDef
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionDef]') AND type IN ('U'))
	DROP TABLE [dbo].[TransactionDef]
GO

CREATE TABLE [dbo].[TransactionDef] (
  [TranId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AllowReverse] tinyint  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [HostTranName] varchar(42) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranClass] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TranFlow] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranMode] varchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranCatId] numeric(19)  NULL,
  [TranSubType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AllowStoredReversal] bit  NULL
)
GO

ALTER TABLE [dbo].[TransactionDef] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TransactionDefTXT
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionDefTXT]') AND type IN ('U'))
	DROP TABLE [dbo].[TransactionDefTXT]
GO

CREATE TABLE [dbo].[TransactionDefTXT] (
  [TranTXTId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TxtLanguage] char(2) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TranId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TransactionDefTXT] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TransactionLimits
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionLimits]') AND type IN ('U'))
	DROP TABLE [dbo].[TransactionLimits]
GO

CREATE TABLE [dbo].[TransactionLimits] (
  [TransactionLimitId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [AuthType] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DailyMaxAmt] numeric(12,4)  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsCustomer] tinyint  NULL,
  [MachBillsIn] int  NULL,
  [MachBillsOut] int  NULL,
  [MachChecks] int  NULL,
  [MachCoinsIn] int  NULL,
  [MachCoinsOut] int  NULL,
  [MaxAmtCashIn] numeric(12,4)  NULL,
  [MaxAmtCashOut] numeric(12,4)  NULL,
  [MaxAmtCheck] numeric(12,4)  NULL,
  [MaxAmtCoin] numeric(12,4)  NULL,
  [MaxAmtTotal] numeric(12,4)  NULL,
  [MaxBillsIn] int  NULL,
  [MaxBillsOut] int  NULL,
  [MaxChecks] int  NULL,
  [MaxCoinsIn] int  NULL,
  [MaxCoinsOut] int  NULL,
  [OvrdAmtCashIn] numeric(12,4)  NULL,
  [OvrdAmtCashOut] numeric(12,4)  NULL,
  [OvrdAmtCheck] numeric(12,4)  NULL,
  [OvrdAmtCoin] numeric(12,4)  NULL,
  [OvrdAmtTotal] numeric(12,4)  NULL,
  [OvrdBillsIn] int  NULL,
  [OvrdBillsOut] int  NULL,
  [OvrdChecks] int  NULL,
  [OvrdCoinsIn] int  NULL,
  [OvrdCoinsOut] int  NULL,
  [PointName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReviewLimit] numeric(12,4)  NULL,
  [ReviewRule] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Role] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TranMode] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Verify2ndIDAmt] numeric(12,4)  NULL,
  [VerifyeSigAmt] numeric(12,4)  NULL,
  [Version] numeric(19)  NULL,
  [ISOCode] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [TranId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[TransactionLimits] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranScriptDef
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranScriptDef]') AND type IN ('U'))
	DROP TABLE [dbo].[TranScriptDef]
GO

CREATE TABLE [dbo].[TranScriptDef] (
  [TranScriptId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ScriptOrder] int  NULL,
  [TranId] numeric(19)  NULL,
  [GlAcctId] numeric(19)  NULL,
  [ScriptDefId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[TranScriptDef] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TranSeqNum
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TranSeqNum]') AND type IN ('U'))
	DROP TABLE [dbo].[TranSeqNum]
GO

CREATE TABLE [dbo].[TranSeqNum] (
  [TranSeqId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [BusinessDt] datetime  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Seqnum] numeric(20)  NOT NULL,
  [TermId] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatedDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Version] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TranSeqNum] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Tray
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Tray]') AND type IN ('U'))
	DROP TABLE [dbo].[Tray]
GO

CREATE TABLE [dbo].[Tray] (
  [TrayId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [TrayBegBundle] numeric(19,2)  NULL,
  [TrayBegCount] numeric(19,2)  NULL,
  [TrayBundleCount] numeric(19,2)  NULL,
  [TrayCount] numeric(19,2)  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TrayDate] datetime  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DrawerId] numeric(19)  NOT NULL,
  [TrayTypeId] numeric(19)  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Tray] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for TrayType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[TrayType]') AND type IN ('U'))
	DROP TABLE [dbo].[TrayType]
GO

CREATE TABLE [dbo].[TrayType] (
  [TrayTypeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TrayOrder] int  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DenomId] numeric(19)  NULL,
  [DrwrTypeId] numeric(19)  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[TrayType] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for UCFHoldMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[UCFHoldMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[UCFHoldMatrix]
GO

CREATE TABLE [dbo].[UCFHoldMatrix] (
  [HoldReasonCodeId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [HoldRecmdMsg] varchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HoldRecmdResponseCd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsEwsNegativeResp] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsNewCustomer] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsSmallBusiness] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsUCFHoldDefault] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Language] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[UCFHoldMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for UserDrawer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDrawer]') AND type IN ('U'))
	DROP TABLE [dbo].[UserDrawer]
GO

CREATE TABLE [dbo].[UserDrawer] (
  [UserDrawerId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [UserName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [DrawerId] numeric(19)  NULL,
  [PointId] numeric(19)  NULL,
  [IsDefault] bit  NULL
)
GO

ALTER TABLE [dbo].[UserDrawer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for UserPreferences
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[UserPreferences]') AND type IN ('U'))
	DROP TABLE [dbo].[UserPreferences]
GO

CREATE TABLE [dbo].[UserPreferences] (
  [UserPreferencesId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CustId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UserId] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[UserPreferences] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for UserRequestHistory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRequestHistory]') AND type IN ('U'))
	DROP TABLE [dbo].[UserRequestHistory]
GO

CREATE TABLE [dbo].[UserRequestHistory] (
  [Duid] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UserName] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LastCount] numeric(19,2)  NOT NULL,
  [LastAccess] datetime  NOT NULL,
  [LastSequence] numeric(19,2)  NOT NULL,
  [OrigAccess] datetime  NOT NULL,
  [OutOfSync] numeric(19,2)  NOT NULL
)
GO

ALTER TABLE [dbo].[UserRequestHistory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for Verification
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Verification]') AND type IN ('U'))
	DROP TABLE [dbo].[Verification]
GO

CREATE TABLE [dbo].[Verification] (
  [VerficationId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Bureau] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceFingerPrint] varchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [DeviceIp] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EffDt] datetime  NULL,
  [VerificationMethod] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Reason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReasonType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RefNo] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RqUID] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Score] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [SeqNum] int  NULL,
  [StatusCd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Strategy] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReportText] text COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ReportTextType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerifyType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [IsVerfied] tinyint  NULL,
  [VerifyCd] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookPartyId] numeric(19)  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookAccountId] numeric(19)  NULL,
  [TranId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[Verification] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for VerificationRollupMatrix
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificationRollupMatrix]') AND type IN ('U'))
	DROP TABLE [dbo].[VerificationRollupMatrix]
GO

CREATE TABLE [dbo].[VerificationRollupMatrix] (
  [VerificationRollupId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [PDMStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime  NULL,
  [CreatorUID] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreditCheckStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [EwsStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [ExtVerificationStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [FraudCheckStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [HotFileStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdaStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IdvStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RecordState] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RegDStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RelationshipType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RollupVerificationStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [StatusReason] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TaxIdCheckStatus] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDate] datetime  NULL,
  [UpdatorUID] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorUSR] varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [VerificationType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorApplName] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [UpdatorDuid] varchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[VerificationRollupMatrix] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for VirtualAddress
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[VirtualAddress]') AND type IN ('U'))
	DROP TABLE [dbo].[VirtualAddress]
GO

CREATE TABLE [dbo].[VirtualAddress] (
  [VirtAddrId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [ApplicationId] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [AddrName] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [IsPreferred] tinyint  NULL,
  [SeqNum] int  NULL,
  [AddrType] int  NULL,
  [Address] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [BookPartyId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[VirtualAddress] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for XchgRate
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[XchgRate]') AND type IN ('U'))
	DROP TABLE [dbo].[XchgRate]
GO

CREATE TABLE [dbo].[XchgRate] (
  [XchgRateId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [FinInstKey] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [XchgRate] numeric(15,6)  NULL,
  [BaseCurrency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XchgFrom] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [XchgTo] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[XchgRate] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ZoneFeed
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ZoneFeed]') AND type IN ('U'))
	DROP TABLE [dbo].[ZoneFeed]
GO

CREATE TABLE [dbo].[ZoneFeed] (
  [ZoneFeedId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [NoJump] tinyint  NOT NULL,
  [Priority] int  NOT NULL,
  [QueueType] varchar(32) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Ratio] int  NOT NULL,
  [ZoneId] numeric(19)  NULL
)
GO

ALTER TABLE [dbo].[ZoneFeed] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for ZoneStat
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ZoneStat]') AND type IN ('U'))
	DROP TABLE [dbo].[ZoneStat]
GO

CREATE TABLE [dbo].[ZoneStat] (
  [ZoneStatId] numeric(19)  IDENTITY(1,1) NOT NULL,
  [count] numeric(19,2)  NOT NULL,
  [Day] datetime  NOT NULL,
  [Month] int  NOT NULL,
  [OverCounter] numeric(19,2)  NOT NULL,
  [Pass] numeric(19,2)  NOT NULL,
  [Quarter] int  NOT NULL,
  [Version] numeric(19)  NULL,
  [WeekDay] int  NOT NULL,
  [PointId] numeric(19)  NOT NULL,
  [ZoneFeedId] numeric(19)  NOT NULL
)
GO

ALTER TABLE [dbo].[ZoneStat] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Function structure for udf_CalculateDistance
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_CalculateDistance]') AND type IN ('FN', 'FS', 'FT', 'IF', 'TF'))
	DROP FUNCTION[dbo].[udf_CalculateDistance]
GO

CREATE FUNCTION [dbo].[udf_CalculateDistance] 
(
        -- Add the parameters for the function here
        @lat1 float, @long1 float, 
    @lat2 float, @long2 float

)
RETURNS float
AS
BEGIN
        -- Declare the return variable here
        -- Add the T-SQL statements to compute the return value here
        declare @dlon float, @dlat float, @rlat1 float, 
                 @rlat2 float, @rlong1 float, @rlong2 float, 
                 @a float, @c float, @R float, @d float, @DtoR float

    select @DtoR = 0.017453293
    select @R = 3959      -- Earth radius

    select 
        @rlat1 = @lat1 * @DtoR,
        @rlong1 = @long1 * @DtoR,
        @rlat2 = @lat2 * @DtoR,
        @rlong2 = @long2 * @DtoR

    select 
        @dlon = @rlong1 - @rlong2,
        @dlat = @rlat1 - @rlat2

    select @a = power(sin(@dlat/2), 2) + cos(@rlat1) * 
                     cos(@rlat2) * power(sin(@dlon/2), 2)
    select @c = 2 * atn2(sqrt(@a), sqrt(1-@a))
    select @d = @R * @c

    return @d 

END
GO


-- ----------------------------
-- Primary Key structure for table Account
-- ----------------------------
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [PK__Account__4FF14A03FE2FC7FA] PRIMARY KEY CLUSTERED ([PhysicalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AccountType
-- ----------------------------
ALTER TABLE [dbo].[AccountType] ADD CONSTRAINT [PK__AccountT__B63F9710FC272A6E] PRIMARY KEY CLUSTERED ([AcctTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AccountTypeTXT
-- ----------------------------
ALTER TABLE [dbo].[AccountTypeTXT] ADD CONSTRAINT [PK__AccountT__32206DA3298F398C] PRIMARY KEY CLUSTERED ([AcctTypeTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AcctAccessLevel
-- ----------------------------
ALTER TABLE [dbo].[AcctAccessLevel] ADD CONSTRAINT [PK__AcctAcce__5E44DFD57D810222] PRIMARY KEY CLUSTERED ([AccessLevelId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AcctCrossRef
-- ----------------------------
ALTER TABLE [dbo].[AcctCrossRef] ADD CONSTRAINT [PK__AcctCros__4423091F4D65C34D] PRIMARY KEY CLUSTERED ([AccountNumber], [AccountType], [CIFNumber])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertCategory
-- ----------------------------
ALTER TABLE [dbo].[AlertCategory] ADD CONSTRAINT [PK__AlertCat__A765F48BD3B02924] PRIMARY KEY CLUSTERED ([AlertCatId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertDeviceRegister
-- ----------------------------
ALTER TABLE [dbo].[AlertDeviceRegister] ADD CONSTRAINT [PK__AlertDev__05554660F694972B] PRIMARY KEY CLUSTERED ([AlertDeviceRegisterId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertLog
-- ----------------------------
ALTER TABLE [dbo].[AlertLog] ADD CONSTRAINT [PK__AlertLog__4D759FE2D1CFF1D9] PRIMARY KEY CLUSTERED ([AlertLogId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertLogMetaData
-- ----------------------------
ALTER TABLE [dbo].[AlertLogMetaData] ADD CONSTRAINT [PK__AlertLog__27F877398253717E] PRIMARY KEY CLUSTERED ([AlertMetaDataId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertMessage
-- ----------------------------
ALTER TABLE [dbo].[AlertMessage] ADD CONSTRAINT [PK__AlertMes__DD16F5C38936B0BF] PRIMARY KEY CLUSTERED ([AlertMessageId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertMessageField
-- ----------------------------
ALTER TABLE [dbo].[AlertMessageField] ADD CONSTRAINT [PK__AlertMes__033977940D78C16D] PRIMARY KEY CLUSTERED ([AlertMsgFieldId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertTemplate
-- ----------------------------
ALTER TABLE [dbo].[AlertTemplate] ADD CONSTRAINT [PK__AlertTem__EFB171D555C4F320] PRIMARY KEY CLUSTERED ([AlertTemplateId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AlertTrigger
-- ----------------------------
ALTER TABLE [dbo].[AlertTrigger] ADD CONSTRAINT [PK__AlertTri__AF638C35BBFA526E] PRIMARY KEY CLUSTERED ([AlertTriggerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AltLcrParty
-- ----------------------------
ALTER TABLE [dbo].[AltLcrParty] ADD CONSTRAINT [PK__AltLcrPa__73AA5DD4220219F0] PRIMARY KEY CLUSTERED ([AltPartyId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AltParty
-- ----------------------------
ALTER TABLE [dbo].[AltParty] ADD CONSTRAINT [PK__AltParty__8D8EC432C7A67850] PRIMARY KEY CLUSTERED ([AltPartylId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AltRole
-- ----------------------------
ALTER TABLE [dbo].[AltRole] ADD CONSTRAINT [PK__AltRole__507EA63BAE6F7781] PRIMARY KEY CLUSTERED ([AltRoleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AltRoleTXT
-- ----------------------------
ALTER TABLE [dbo].[AltRoleTXT] ADD CONSTRAINT [PK__AltRoleT__8DAA446640D8DB51] PRIMARY KEY CLUSTERED ([AltRoleTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AltUserAttribute
-- ----------------------------
ALTER TABLE [dbo].[AltUserAttribute] ADD CONSTRAINT [PK__AltUserA__C18929EAC5CA7B68] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AltUserAuth
-- ----------------------------
ALTER TABLE [dbo].[AltUserAuth] ADD CONSTRAINT [PK__AltUserA__FE244C5C043DF6A5] PRIMARY KEY CLUSTERED ([AltUserId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AltUserRole
-- ----------------------------
ALTER TABLE [dbo].[AltUserRole] ADD CONSTRAINT [PK__AltUserR__5B23A63FEEB2E074] PRIMARY KEY CLUSTERED ([AltUserId], [AltRoleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppBranding
-- ----------------------------
ALTER TABLE [dbo].[AppBranding] ADD CONSTRAINT [PK__AppBrand__BCD3C1272AD4DB40] PRIMARY KEY CLUSTERED ([BrandingId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppBrandingTXT
-- ----------------------------
ALTER TABLE [dbo].[AppBrandingTXT] ADD CONSTRAINT [PK__AppBrand__738ADC7A0CB3715B] PRIMARY KEY CLUSTERED ([BrandingTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table Application
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_Application]
ON [dbo].[Application]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Application'
		,d.ApplId
		,'Application record deleted'
		,d.ApplName
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ApplId = d.ApplId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_Application]
ON [dbo].[Application]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Application'
		,d.ApplId
		,'Application record updated'
		,d.ApplName
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ApplId = d.ApplId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE ApplId = d.ApplId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN Application lu ON d.ApplId = lu.ApplId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE ApplId = d.ApplId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE ApplId = d.ApplId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table Application
-- ----------------------------
ALTER TABLE [dbo].[Application] ADD CONSTRAINT [PK__Applicat__EF5C77F2041701BD] PRIMARY KEY CLUSTERED ([ApplId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table ApplicationTXT
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_ApplicationTXT]
ON [dbo].[ApplicationTXT]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ApplicationTXT'
		,d.ApplTXTId
		,'ApplicationTXT record deleted'
		,d.Description
		,d.TxtLanguage
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ApplTXTId = d.ApplTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_ApplicationTXT]
ON [dbo].[ApplicationTXT]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ApplicationTXT'
		,d.ApplTXTId
		,'ApplicationTXT record updated'
		,d.Description
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ApplTXTId = d.ApplTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE ApplTXTId = d.ApplTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN ApplicationTXT lu ON d.ApplTXTId = lu.ApplTXTId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE ApplTXTId = d.ApplTXTId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE ApplTXTId = d.ApplTXTId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table ApplicationTXT
-- ----------------------------
ALTER TABLE [dbo].[ApplicationTXT] ADD CONSTRAINT [PK__Applicat__381B0500D77A8316] PRIMARY KEY CLUSTERED ([ApplTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table AppModule
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_AppModule]
ON [dbo].[AppModule]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'AppModule'
		,d.ModuleId
		,'AppModule record deleted'
		,d.ModuleName
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  ModuleId = d.ModuleId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_AppModule]
ON [dbo].[AppModule]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'AppModule'
		,d.ModuleId
		,'AppModule record updated'
		,d.ModuleName
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ModuleId = d.ModuleId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE ModuleId = d.ModuleId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN AppModule lu ON d.ModuleId = lu.ModuleId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE ModuleId = d.ModuleId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE ModuleId = d.ModuleId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table AppModule
-- ----------------------------
ALTER TABLE [dbo].[AppModule] ADD CONSTRAINT [PK__AppModul__2B7477A783573F28] PRIMARY KEY CLUSTERED ([ModuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table AppModuleTXT
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_AppModuleTXT]
ON [dbo].[AppModuleTXT]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'AppModuleTXT'
		,d.ModuleTXTId
		,'AppModuleTXT record deleted'
		,d.Description
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  ModuleId = d.ModuleId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_AppModuleTXT]
ON [dbo].[AppModuleTXT]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'AppModuleTXT'
		,d.ModuleTXTId
		,'AppModuleTXT record updated'
		,d.Description
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ModuleTXTId = d.ModuleTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE ModuleTXTId = d.ModuleTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN AppModuleTXT lu ON d.ModuleTXTId = lu.ModuleTXTId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE ModuleTXTId = d.ModuleTXTId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE ModuleTXTId = d.ModuleTXTId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table AppModuleTXT
-- ----------------------------
ALTER TABLE [dbo].[AppModuleTXT] ADD CONSTRAINT [PK__AppModul__FE06FB1A85FAC1A7] PRIMARY KEY CLUSTERED ([ModuleTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table AppNote
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_AppNote]
ON [dbo].[AppNote]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'AppNote'
		,@ApplId
		,'AppNote record deleted'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE AppNoteId = d.AppNoteId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_AppNote]
ON [dbo].[AppNote]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'AppNote'
		,@ApplId
		,'AppNote record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE AppNoteId = d.AppNoteId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE AppNoteId = d.AppNoteId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN AppNote lu ON d.AppNoteId = lu.AppNoteId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE AppNoteId = d.AppNoteId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE AppNoteId = d.AppNoteId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table AppNote
-- ----------------------------
ALTER TABLE [dbo].[AppNote] ADD CONSTRAINT [PK__AppNote__308CF7087453653F] PRIMARY KEY CLUSTERED ([AppNoteId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppProcessAction
-- ----------------------------
ALTER TABLE [dbo].[AppProcessAction] ADD CONSTRAINT [PK__AppProce__A0E17D58AD9B4BAD] PRIMARY KEY CLUSTERED ([AppProcessActionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppProperty
-- ----------------------------
ALTER TABLE [dbo].[AppProperty] ADD CONSTRAINT [PK__AppPrope__70C9A73550137715] PRIMARY KEY CLUSTERED ([PropertyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppPropertyTXT
-- ----------------------------
ALTER TABLE [dbo].[AppPropertyTXT] ADD CONSTRAINT [PK__AppPrope__93851F5E64474368] PRIMARY KEY CLUSTERED ([AppPropTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppRestriction
-- ----------------------------
ALTER TABLE [dbo].[AppRestriction] ADD CONSTRAINT [PK__AppRestr__A711E92BAB8BF413] PRIMARY KEY CLUSTERED ([AppRestrictionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppThresholdMatrix
-- ----------------------------
ALTER TABLE [dbo].[AppThresholdMatrix] ADD CONSTRAINT [PK__AppThres__AF6DCB2495C33ACE] PRIMARY KEY CLUSTERED ([AppThresholdId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AppValidationRule
-- ----------------------------
ALTER TABLE [dbo].[AppValidationRule] ADD CONSTRAINT [PK__AppValid__05BE90AD69924D7A] PRIMARY KEY CLUSTERED ([AppValidationRuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Attachment
-- ----------------------------
ALTER TABLE [dbo].[Attachment] ADD CONSTRAINT [PK__Attachme__442C64BE0E907DC0] PRIMARY KEY CLUSTERED ([AttachmentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AttributeName
-- ----------------------------
ALTER TABLE [dbo].[AttributeName] ADD CONSTRAINT [PK__Attribut__F5849CA0AC8F41B6] PRIMARY KEY CLUSTERED ([AttributeNameId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AttributeNameTXT
-- ----------------------------
ALTER TABLE [dbo].[AttributeNameTXT] ADD CONSTRAINT [PK__Attribut__C88DA418D47D3432] PRIMARY KEY CLUSTERED ([AttribNameTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AuditLog
-- ----------------------------
ALTER TABLE [dbo].[AuditLog] ADD CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED ([AuditLogId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BatchEnrollment
-- ----------------------------
ALTER TABLE [dbo].[BatchEnrollment] ADD CONSTRAINT [PK__BatchEnr__3214EC0741A5FED7] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BatchLog
-- ----------------------------
ALTER TABLE [dbo].[BatchLog] ADD CONSTRAINT [PK__BatchLog__5927836E81B272E5] PRIMARY KEY CLUSTERED ([BatchLogId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BatchTask
-- ----------------------------
ALTER TABLE [dbo].[BatchTask] ADD CONSTRAINT [PK__BatchTas__5FA5F7C6FDCB3400] PRIMARY KEY CLUSTERED ([BatchTaskId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BestFitOffer
-- ----------------------------
ALTER TABLE [dbo].[BestFitOffer] ADD CONSTRAINT [PK__BestFitO__AB4E1719C546D7F0] PRIMARY KEY CLUSTERED ([BestFitOfferId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BestFitOfferHist
-- ----------------------------
ALTER TABLE [dbo].[BestFitOfferHist] ADD CONSTRAINT [PK__BestFitO__326A133F0CBC09CB] PRIMARY KEY CLUSTERED ([BestFitOfferHistId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Bin
-- ----------------------------
ALTER TABLE [dbo].[Bin] ADD CONSTRAINT [PK__Bin__4BFF5BAEC21AD2EC] PRIMARY KEY CLUSTERED ([BinId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BinData
-- ----------------------------
ALTER TABLE [dbo].[BinData] ADD CONSTRAINT [PK__BinData__377456638660E415] PRIMARY KEY CLUSTERED ([BinDataId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BinType
-- ----------------------------
ALTER TABLE [dbo].[BinType] ADD CONSTRAINT [PK__BinType__3D42D21F4851FAAE] PRIMARY KEY CLUSTERED ([BinTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BinTypeTXT
-- ----------------------------
ALTER TABLE [dbo].[BinTypeTXT] ADD CONSTRAINT [PK__BinTypeT__76F290E3C2532908] PRIMARY KEY CLUSTERED ([BinTypeTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Bond
-- ----------------------------
ALTER TABLE [dbo].[Bond] ADD CONSTRAINT [PK__Bond__D9B3B70E813AE172] PRIMARY KEY CLUSTERED ([BondId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table BookAccount
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_BookAccount]
ON [dbo].[BookAccount]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookAccount'
		,@ApplId
		,'BookAccount record deleted'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookAccountId = d.BookAccountId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_BookAccount]
ON [dbo].[BookAccount]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookAccount'
		,@ApplId
		,'BookAccount record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookAccountId = d.BookAccountId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE BookAccountId = d.BookAccountId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN BookAccount lu ON d.BookAccountId = lu.BookAccountId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE BookAccountId = d.BookAccountId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE BookAccountId = d.BookAccountId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table BookAccount
-- ----------------------------
ALTER TABLE [dbo].[BookAccount] ADD CONSTRAINT [PK__BookAcco__50AE2E289DADF9D2] PRIMARY KEY CLUSTERED ([BookAccountId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BookAccountAttribute
-- ----------------------------
ALTER TABLE [dbo].[BookAccountAttribute] ADD CONSTRAINT [PK__BookAcco__C18929EA75B8CCF0] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table BookApplication
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_BookApplication]
ON [dbo].[BookApplication]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookApplication'
		,d.ApplicationId
		,'BookApplication record deleted'
		,d.ApplName
		,d.FinInstKey
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookApplicationId = d.BookApplicationId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_BookApplication]
ON [dbo].[BookApplication]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookApplication'
		,d.ApplicationId
		,'BookApplication record updated'
		,d.ApplName
		,d.FinInstKey
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookApplicationId = d.BookApplicationId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE BookApplicationId = d.BookApplicationId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN BookApplication lu ON d.BookApplicationId = lu.BookApplicationId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE BookApplicationId = d.BookApplicationId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE BookApplicationId = d.BookApplicationId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table BookApplication
-- ----------------------------
ALTER TABLE [dbo].[BookApplication] ADD CONSTRAINT [PK__BookAppl__5AEFEBBCE68E659D] PRIMARY KEY CLUSTERED ([BookApplicationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table BookParty
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_BookParty]
ON [dbo].[BookParty]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookParty'
		,@ApplId
		,'BookParty record deleted'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookPartyId = d.BookPartyId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_BookParty]
ON [dbo].[BookParty]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookParty'
		,@ApplId
		,'BookParty record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookPartyId = d.BookPartyId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE BookPartyId = d.BookPartyId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN BookParty lu ON d.BookPartyId = lu.BookPartyId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE BookPartyId = d.BookPartyId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE BookPartyId = d.BookPartyId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table BookParty
-- ----------------------------
ALTER TABLE [dbo].[BookParty] ADD CONSTRAINT [PK__BookPart__23A2DE9015BA2826] PRIMARY KEY CLUSTERED ([BookPartyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BookPartyAttribute
-- ----------------------------
ALTER TABLE [dbo].[BookPartyAttribute] ADD CONSTRAINT [PK__BookPart__C18929EABB8F10B9] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table BookPartyRel
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_BookPartyRel]
ON [dbo].[BookPartyRel]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookPartyRel'
		,@ApplId
		,'BookPartyRel record deleted'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookPartyRelId = d.BookPartyRelId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_BookPartyRel]
ON [dbo].[BookPartyRel]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'BookPartyRel'
		,@ApplId
		,'BookPartyRel record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE BookPartyRelId = d.BookPartyRelId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE BookPartyRelId = d.BookPartyRelId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN BookPartyRel lu ON d.BookPartyRelId = lu.BookPartyRelId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE BookPartyRelId = d.BookPartyRelId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE BookPartyRelId = d.BookPartyRelId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table BookPartyRel
-- ----------------------------
ALTER TABLE [dbo].[BookPartyRel] ADD CONSTRAINT [PK__BookPart__A4976A2AA6E1BCA5] PRIMARY KEY CLUSTERED ([BookPartyRelId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BrandingAttribute
-- ----------------------------
ALTER TABLE [dbo].[BrandingAttribute] ADD CONSTRAINT [PK__Branding__C18929EAB1FBB9D1] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BSAData
-- ----------------------------
ALTER TABLE [dbo].[BSAData] ADD CONSTRAINT [PK__BSAData__B6A344B73411E785] PRIMARY KEY CLUSTERED ([BSARecordId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BusinessCalendar
-- ----------------------------
ALTER TABLE [dbo].[BusinessCalendar] ADD CONSTRAINT [PK__Business__E5189EB2076B6F6D] PRIMARY KEY CLUSTERED ([BusinessCalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BusinessCalendarTXT
-- ----------------------------
ALTER TABLE [dbo].[BusinessCalendarTXT] ADD CONSTRAINT [PK__Business__F76AE95E3612328A] PRIMARY KEY CLUSTERED ([BusinessCalendarTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BusinessCard
-- ----------------------------
ALTER TABLE [dbo].[BusinessCard] ADD CONSTRAINT [PK__Business__1156236EB74EEFA0] PRIMARY KEY CLUSTERED ([BusinessCardId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CalendarException
-- ----------------------------
ALTER TABLE [dbo].[CalendarException] ADD CONSTRAINT [PK__Calendar__46997D4AC814E9DF] PRIMARY KEY CLUSTERED ([CalExceptionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CardData
-- ----------------------------
ALTER TABLE [dbo].[CardData] ADD CONSTRAINT [PK__CardData__55FECDAE34F85EF5] PRIMARY KEY CLUSTERED ([CardId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CardDesign
-- ----------------------------
ALTER TABLE [dbo].[CardDesign] ADD CONSTRAINT [PK__CardDesi__49F3482F638E8D7B] PRIMARY KEY CLUSTERED ([CardDesignId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CardPinValidation
-- ----------------------------
ALTER TABLE [dbo].[CardPinValidation] ADD CONSTRAINT [PK__CardPinV__9791D3C548EEAD06] PRIMARY KEY CLUSTERED ([CardPinValidationId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CardStatusValidation
-- ----------------------------
ALTER TABLE [dbo].[CardStatusValidation] ADD CONSTRAINT [PK__CardStat__E051BA78CA96158A] PRIMARY KEY CLUSTERED ([CardStatusValidationId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseAuditHistory
-- ----------------------------
ALTER TABLE [dbo].[CaseAuditHistory] ADD CONSTRAINT [PK__CaseAudi__C87C8A0978C6F0C2] PRIMARY KEY CLUSTERED ([CaseAuditHistId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseCustomer
-- ----------------------------
ALTER TABLE [dbo].[CaseCustomer] ADD CONSTRAINT [PK__CaseCust__5EF665E4779E41E6] PRIMARY KEY CLUSTERED ([CaseCustomerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseDataShadow
-- ----------------------------
ALTER TABLE [dbo].[CaseDataShadow] ADD CONSTRAINT [PK__CaseData__49BCE212A978212C] PRIMARY KEY CLUSTERED ([ShadowContentId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseFolder
-- ----------------------------
ALTER TABLE [dbo].[CaseFolder] ADD CONSTRAINT [PK__CaseFold__D44EE173F6DCB079] PRIMARY KEY CLUSTERED ([CaseFolderId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseForm
-- ----------------------------
ALTER TABLE [dbo].[CaseForm] ADD CONSTRAINT [PK__CaseForm__E46EC47C26964671] PRIMARY KEY CLUSTERED ([CaseFormId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseMilestone
-- ----------------------------
ALTER TABLE [dbo].[CaseMilestone] ADD CONSTRAINT [PK__CaseMile__09C48078521A5FD2] PRIMARY KEY CLUSTERED ([MilestoneId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseQueuePerm
-- ----------------------------
ALTER TABLE [dbo].[CaseQueuePerm] ADD CONSTRAINT [PK__CaseQueu__73149BBA996F52F6] PRIMARY KEY CLUSTERED ([CaseQueuePermId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseTaskQueue
-- ----------------------------
ALTER TABLE [dbo].[CaseTaskQueue] ADD CONSTRAINT [PK__CaseTask__81780137EA61D9D3] PRIMARY KEY CLUSTERED ([CaseTaskQueueId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseTaskQueueTXT
-- ----------------------------
ALTER TABLE [dbo].[CaseTaskQueueTXT] ADD CONSTRAINT [PK__CaseTask__0DA7D5A6D77FA673] PRIMARY KEY CLUSTERED ([CaseTaskQueueTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CaseUserTask
-- ----------------------------
ALTER TABLE [dbo].[CaseUserTask] ADD CONSTRAINT [PK__CaseUser__4EF5961FC9CE5C6A] PRIMARY KEY CLUSTERED ([UserTaskId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CashAggregation
-- ----------------------------
ALTER TABLE [dbo].[CashAggregation] ADD CONSTRAINT [PK__CashAggr__D7397831A339E61D] PRIMARY KEY CLUSTERED ([AggregationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CashDrawer
-- ----------------------------
ALTER TABLE [dbo].[CashDrawer] ADD CONSTRAINT [PK__CashDraw__BB86AAC1B7ADE3A9] PRIMARY KEY CLUSTERED ([DrawerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ChannelError
-- ----------------------------
ALTER TABLE [dbo].[ChannelError] ADD CONSTRAINT [PK__ChannelE__EAF5915A6FF74DF9] PRIMARY KEY CLUSTERED ([ChannelErrorId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ChannelMessage
-- ----------------------------
ALTER TABLE [dbo].[ChannelMessage] ADD CONSTRAINT [PK__ChannelM__372F9195DE9B33BB] PRIMARY KEY CLUSTERED ([ChannelMessageId])
WITH (PAD_INDEX = ON, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ChannelMessageTXT
-- ----------------------------
ALTER TABLE [dbo].[ChannelMessageTXT] ADD CONSTRAINT [PK__ChannelM__105500D543DFB948] PRIMARY KEY CLUSTERED ([ChannelMessageTXTId])
WITH (PAD_INDEX = ON, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CheckData
-- ----------------------------
ALTER TABLE [dbo].[CheckData] ADD CONSTRAINT [PK__CheckDat__86815766F223BF4F] PRIMARY KEY CLUSTERED ([CheckId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ChildJournal
-- ----------------------------
ALTER TABLE [dbo].[ChildJournal] ADD CONSTRAINT [PK__ChildJou__BEFA07167FAAE0B6] PRIMARY KEY CLUSTERED ([ChildId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ChkItem
-- ----------------------------
ALTER TABLE [dbo].[ChkItem] ADD CONSTRAINT [PK__ChkItem__8225FF435011CB52] PRIMARY KEY CLUSTERED ([ChkId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ChkValidationRule
-- ----------------------------
ALTER TABLE [dbo].[ChkValidationRule] ADD CONSTRAINT [PK__ChkValid__110458E29FDA08CE] PRIMARY KEY CLUSTERED ([RuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CifCrossRef
-- ----------------------------
ALTER TABLE [dbo].[CifCrossRef] ADD CONSTRAINT [PK__CifCross__765228FF859C93C0] PRIMARY KEY CLUSTERED ([CIFNumber])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CityStatePostalCode
-- ----------------------------
ALTER TABLE [dbo].[CityStatePostalCode] ADD CONSTRAINT [PK__CityStat__CB24E80154DA1A8E] PRIMARY KEY CLUSTERED ([CityStatePostalCodeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table Code
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_Code]
ON [dbo].[Code]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Code'
		,d.CodeId
		,'Code record deleted'
		,d.[CodeName]
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  CodeId = d.CodeId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_Code]
ON [dbo].[Code]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Code'
		,d.CodeId
		,'Code record updated'
		,d.[CodeName]
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE CodeId = d.CodeId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE CodeId = d.CodeId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN Code lu ON d.CodeId = lu.CodeId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE CodeId = d.CodeId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE CodeId = d.CodeId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table Code
-- ----------------------------
ALTER TABLE [dbo].[Code] ADD CONSTRAINT [PK__Code__C6DE2C152890AC15] PRIMARY KEY CLUSTERED ([CodeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CodeAttrib
-- ----------------------------
ALTER TABLE [dbo].[CodeAttrib] ADD CONSTRAINT [PK__CodeAttr__C06FA1A4A9B7D002] PRIMARY KEY CLUSTERED ([CodeAttribId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table CodeCategory
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_CodeCategory]
ON [dbo].[CodeCategory]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'CodeCategory'
		,d.CodeCategoryId
		,'CodeCategory record deleted'
		,d.[CategoryName]
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  CodeCategoryId = d.CodeCategoryId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_CodeCategory]
ON [dbo].[CodeCategory]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'CodeCategory'
		,d.CodeCategoryId
		,'CodeCategory record updated'
		,d.[CategoryName]
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE CodeCategoryId = d.CodeCategoryId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE CodeCategoryId = d.CodeCategoryId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN CodeCategory lu ON d.CodeCategoryId = lu.CodeCategoryId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE CodeCategoryId = d.CodeCategoryId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE CodeCategoryId = d.CodeCategoryId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table CodeCategory
-- ----------------------------
ALTER TABLE [dbo].[CodeCategory] ADD CONSTRAINT [PK__CodeCate__7CF80DAEDF9133D8] PRIMARY KEY CLUSTERED ([CodeCategoryId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table CodeCategoryTXT
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_CodeCategoryTXT]
ON [dbo].[CodeCategoryTXT]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'CodeCategoryTXT'
		,d.CodeCatTXTId
		,'CodeCategoryTXT record deleted'
		,d.[Description]
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  CodeCatTXTId = d.CodeCatTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_CodeCategoryTXT]
ON [dbo].[CodeCategoryTXT]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'CodeCategoryTXT'
		,d.[CodeCatTXTId]
		,'CodeCategoryTXT record updated'
		,d.[Description]
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE [CodeCatTXTId] = d.[CodeCatTXTId]
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE [CodeCatTXTId] = d.[CodeCatTXTId]
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN CodeCategoryTXT lu ON d.[CodeCatTXTId] = lu.[CodeCatTXTId]
	WHERE (
			SELECT *
			FROM DELETED
			WHERE [CodeCatTXTId] = d.[CodeCatTXTId]
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE [CodeCatTXTId] = d.[CodeCatTXTId]
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table CodeCategoryTXT
-- ----------------------------
ALTER TABLE [dbo].[CodeCategoryTXT] ADD CONSTRAINT [PK__CodeCate__FA29FF0A16401CD9] PRIMARY KEY CLUSTERED ([CodeCatTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Complaint
-- ----------------------------
ALTER TABLE [dbo].[Complaint] ADD CONSTRAINT [PK__Complain__740D898F6C61E17E] PRIMARY KEY CLUSTERED ([ComplaintId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ComplaintAttachment
-- ----------------------------
ALTER TABLE [dbo].[ComplaintAttachment] ADD CONSTRAINT [PK__Complain__D80CA149CA7F3BF6] PRIMARY KEY CLUSTERED ([ComplaintAttachmentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ComplaintComment
-- ----------------------------
ALTER TABLE [dbo].[ComplaintComment] ADD CONSTRAINT [PK__Complain__B502720187FCDCA2] PRIMARY KEY CLUSTERED ([ComplaintCommentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ComplaintHistory
-- ----------------------------
ALTER TABLE [dbo].[ComplaintHistory] ADD CONSTRAINT [PK__Complain__0075B84151B5D05B] PRIMARY KEY CLUSTERED ([ComplaintHistoryId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ComplaintWatcher
-- ----------------------------
ALTER TABLE [dbo].[ComplaintWatcher] ADD CONSTRAINT [PK__Complain__A5DFEA3048AE6238] PRIMARY KEY CLUSTERED ([ComplaintWatcherId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Component
-- ----------------------------
ALTER TABLE [dbo].[Component] ADD CONSTRAINT [PK__Componen__AD362A16F0EBF673] PRIMARY KEY CLUSTERED ([CompId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ComponentType
-- ----------------------------
ALTER TABLE [dbo].[ComponentType] ADD CONSTRAINT [PK__Componen__A42CADB07069EABE] PRIMARY KEY CLUSTERED ([CompTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CompProperty
-- ----------------------------
ALTER TABLE [dbo].[CompProperty] ADD CONSTRAINT [PK__CompProp__6D306163F49A68D3] PRIMARY KEY CLUSTERED ([CompPropId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CompRelation
-- ----------------------------
ALTER TABLE [dbo].[CompRelation] ADD CONSTRAINT [PK__CompRela__01ACB6A27BFDAEEE] PRIMARY KEY CLUSTERED ([CompRelationid])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CompRoleValue
-- ----------------------------
ALTER TABLE [dbo].[CompRoleValue] ADD CONSTRAINT [PK__CompRole__6D7E2369208C0527] PRIMARY KEY CLUSTERED ([CompRoleValueId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ConfigProfile
-- ----------------------------
ALTER TABLE [dbo].[ConfigProfile] ADD CONSTRAINT [PK__ConfigPr__290C88E432C8DF5C] PRIMARY KEY CLUSTERED ([ProfileId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ContactEvent
-- ----------------------------
ALTER TABLE [dbo].[ContactEvent] ADD CONSTRAINT [PK__ContactE__76964C5C3C8A65E5] PRIMARY KEY CLUSTERED ([CustEventId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ContactEventTXT
-- ----------------------------
ALTER TABLE [dbo].[ContactEventTXT] ADD CONSTRAINT [PK__ContactE__02694512F3C77F87] PRIMARY KEY CLUSTERED ([EventTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ContactPoint
-- ----------------------------
ALTER TABLE [dbo].[ContactPoint] ADD CONSTRAINT [PK__ContactP__40A977E1EF9F1B16] PRIMARY KEY CLUSTERED ([PointId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ContactPointAttribute
-- ----------------------------
ALTER TABLE [dbo].[ContactPointAttribute] ADD CONSTRAINT [PK__ContactP__C18929EA4B403FAF] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ContactPointLog
-- ----------------------------
ALTER TABLE [dbo].[ContactPointLog] ADD CONSTRAINT [PK__ContactP__2650DEEF1F6EA778] PRIMARY KEY CLUSTERED ([LogEntry])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ContactPointTXT
-- ----------------------------
ALTER TABLE [dbo].[ContactPointTXT] ADD CONSTRAINT [PK__ContactP__845A5FEAD6C93805] PRIMARY KEY CLUSTERED ([PointTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ContentType
-- ----------------------------
ALTER TABLE [dbo].[ContentType] ADD CONSTRAINT [PK__ContentT__2026064A5AE3253C] PRIMARY KEY CLUSTERED ([ContentTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Correspondence
-- ----------------------------
ALTER TABLE [dbo].[Correspondence] ADD CONSTRAINT [PK__Correspo__709EC6025AB9771C] PRIMARY KEY CLUSTERED ([CorrespondenceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table Credential
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_Credential]
ON [dbo].[Credential]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Credential'
		,@ApplId
		,'Credential record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE CredentialId = d.CredentialId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_Credential]
ON [dbo].[Credential]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Credential'
		,@ApplId
		,'Credential record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE CredentialId = d.CredentialId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE CredentialId = d.CredentialId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN Credential lu ON d.CredentialId = lu.CredentialId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE CredentialId = d.CredentialId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE CredentialId = d.CredentialId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table Credential
-- ----------------------------
ALTER TABLE [dbo].[Credential] ADD CONSTRAINT [PK__Credenti__2C58F9CC2859987D] PRIMARY KEY CLUSTERED ([CredentialId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CurrencyInfo
-- ----------------------------
ALTER TABLE [dbo].[CurrencyInfo] ADD CONSTRAINT [PK__Currency__14470AF08AB0649A] PRIMARY KEY CLUSTERED ([CurrencyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CustMerge
-- ----------------------------
ALTER TABLE [dbo].[CustMerge] ADD CONSTRAINT [PK__CustMerg__4BF5016A0792CF68] PRIMARY KEY CLUSTERED ([CustMergeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CustomerAlerts
-- ----------------------------
ALTER TABLE [dbo].[CustomerAlerts] ADD CONSTRAINT [PK__Customer__0EB2660335B11DC7] PRIMARY KEY CLUSTERED ([CustomerAlertsId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CustomerComment
-- ----------------------------
ALTER TABLE [dbo].[CustomerComment] ADD CONSTRAINT [PK__Customer__5FFEACF7B790EB6F] PRIMARY KEY CLUSTERED ([CustomerCommentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CustTypeTranMatrix
-- ----------------------------
ALTER TABLE [dbo].[CustTypeTranMatrix] ADD CONSTRAINT [PK__CustType__D40E768457C910B2] PRIMARY KEY CLUSTERED ([CustTypeTranMatrixId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DefinitionType
-- ----------------------------
ALTER TABLE [dbo].[DefinitionType] ADD CONSTRAINT [PK__Definiti__058319397D8415DD] PRIMARY KEY CLUSTERED ([DefItemId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Denomination
-- ----------------------------
ALTER TABLE [dbo].[Denomination] ADD CONSTRAINT [PK__Denomina__721829063E564702] PRIMARY KEY CLUSTERED ([DenomId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DenominationTXT
-- ----------------------------
ALTER TABLE [dbo].[DenominationTXT] ADD CONSTRAINT [PK__Denomina__7ABE983B4E0635F9] PRIMARY KEY CLUSTERED ([DenominationTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Device
-- ----------------------------
ALTER TABLE [dbo].[Device] ADD CONSTRAINT [PK__Device__49E1231112527DA7] PRIMARY KEY CLUSTERED ([DeviceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DeviceAttribute
-- ----------------------------
ALTER TABLE [dbo].[DeviceAttribute] ADD CONSTRAINT [PK__DeviceAt__C18929EA1BF11519] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DeviceTXT
-- ----------------------------
ALTER TABLE [dbo].[DeviceTXT] ADD CONSTRAINT [PK__DeviceTX__0AA7A4EC203C95F1] PRIMARY KEY CLUSTERED ([DeviceTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table Disclosure
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_Disclosure]
ON [dbo].[Disclosure]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Disclosure'
		,@ApplId
		,'Disclosure record delete'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE AppDisclosureId = d.AppDisclosureId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_Disclosure]
ON [dbo].[Disclosure]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'Disclosure'
		,@ApplId
		,'Disclosure record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE AppDisclosureId = d.AppDisclosureId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE AppDisclosureId = d.AppDisclosureId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN Disclosure lu ON d.AppDisclosureId = lu.AppDisclosureId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE AppDisclosureId = d.AppDisclosureId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE AppDisclosureId = d.AppDisclosureId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table Disclosure
-- ----------------------------
ALTER TABLE [dbo].[Disclosure] ADD CONSTRAINT [PK__Disclosu__C9BD7660D4AAFE96] PRIMARY KEY CLUSTERED ([AppDisclosureId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DisclosureTXT
-- ----------------------------
ALTER TABLE [dbo].[DisclosureTXT] ADD CONSTRAINT [PK__Disclosu__67A36721714EDF76] PRIMARY KEY CLUSTERED ([DisclosureTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Document
-- ----------------------------
ALTER TABLE [dbo].[Document] ADD CONSTRAINT [PK__Document__1ABEEF0F6E71B1D3] PRIMARY KEY CLUSTERED ([DocumentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DocumentRecipient
-- ----------------------------
ALTER TABLE [dbo].[DocumentRecipient] ADD CONSTRAINT [PK__Document__F0A6024D33B9CF84] PRIMARY KEY CLUSTERED ([RecipientId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DocumentTab
-- ----------------------------
ALTER TABLE [dbo].[DocumentTab] ADD CONSTRAINT [PK__Document__80E37C18D0946A90] PRIMARY KEY CLUSTERED ([TabId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DocumentVerifyRule
-- ----------------------------
ALTER TABLE [dbo].[DocumentVerifyRule] ADD CONSTRAINT [PK__Document__FA6C0DA23FD1D10F] PRIMARY KEY CLUSTERED ([DocumentVerifyRuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DocVerifyRuleAttribute
-- ----------------------------
ALTER TABLE [dbo].[DocVerifyRuleAttribute] ADD CONSTRAINT [PK__DocVerif__C18929EA9FD60467] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DrawerCashLimits
-- ----------------------------
ALTER TABLE [dbo].[DrawerCashLimits] ADD CONSTRAINT [PK__DrawerCa__A97943481ED7CB13] PRIMARY KEY CLUSTERED ([DrwrCashLimitId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DrawerTotals
-- ----------------------------
ALTER TABLE [dbo].[DrawerTotals] ADD CONSTRAINT [PK__DrawerTo__CF10917F7924B3C3] PRIMARY KEY CLUSTERED ([TotalsId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DrawerType
-- ----------------------------
ALTER TABLE [dbo].[DrawerType] ADD CONSTRAINT [PK__DrawerTy__A996B3103C372483] PRIMARY KEY CLUSTERED ([DrwrTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DrawerTypeTXT
-- ----------------------------
ALTER TABLE [dbo].[DrawerTypeTXT] ADD CONSTRAINT [PK__DrawerTy__1E97D5236BADA950] PRIMARY KEY CLUSTERED ([DrwrTypeTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ElecJournal
-- ----------------------------
ALTER TABLE [dbo].[ElecJournal] ADD CONSTRAINT [PK__ElecJour__250103E6DB9C9BC3] PRIMARY KEY CLUSTERED ([JournalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table EmailMessage
-- ----------------------------
ALTER TABLE [dbo].[EmailMessage] ADD CONSTRAINT [PK__EmailMes__2F4E92AEF5878BE9] PRIMARY KEY CLUSTERED ([EmailMessageId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Employment
-- ----------------------------
ALTER TABLE [dbo].[Employment] ADD CONSTRAINT [PK__Employme__FDC872B60851AE82] PRIMARY KEY CLUSTERED ([EmploymentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table EnrollmentEventRpt
-- ----------------------------
ALTER TABLE [dbo].[EnrollmentEventRpt] ADD CONSTRAINT [PK__Enrollme__3214EC07B1B336D6] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Envelope
-- ----------------------------
ALTER TABLE [dbo].[Envelope] ADD CONSTRAINT [PK__Envelope__5E149FF2CC197C22] PRIMARY KEY CLUSTERED ([EnvelopeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Esignature
-- ----------------------------
ALTER TABLE [dbo].[Esignature] ADD CONSTRAINT [PK__Esignatu__FD29886BC89C0A28] PRIMARY KEY CLUSTERED ([EsignatureId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table EventCustomer
-- ----------------------------
ALTER TABLE [dbo].[EventCustomer] ADD CONSTRAINT [PK__EventCus__67261AAAE7167E81] PRIMARY KEY CLUSTERED ([EventCustId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table EventHistory
-- ----------------------------
ALTER TABLE [dbo].[EventHistory] ADD CONSTRAINT [PK__EventHis__4D7B4ABD5E96FB66] PRIMARY KEY CLUSTERED ([HistoryId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ExceptionCode
-- ----------------------------
ALTER TABLE [dbo].[ExceptionCode] ADD CONSTRAINT [PK__Exceptio__C22B5C5E21A4AAFB] PRIMARY KEY CLUSTERED ([ExceptionCodeId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ExceptionMatrix
-- ----------------------------
ALTER TABLE [dbo].[ExceptionMatrix] ADD CONSTRAINT [PK__Exceptio__3F69917344540E07] PRIMARY KEY CLUSTERED ([ExceptionMatrixId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ExtAcct
-- ----------------------------
ALTER TABLE [dbo].[ExtAcct] ADD CONSTRAINT [PK__ExtAcct__4FF14A033C787B6A] PRIMARY KEY CLUSTERED ([PhysicalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ExtInstitution
-- ----------------------------
ALTER TABLE [dbo].[ExtInstitution] ADD CONSTRAINT [PK__ExtInsti__07ECDD80D0A62E73] PRIMARY KEY CLUSTERED ([ExtInstId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ExtXfer
-- ----------------------------
ALTER TABLE [dbo].[ExtXfer] ADD CONSTRAINT [PK__ExtXfer__8797CD6E94AEF947] PRIMARY KEY CLUSTERED ([ExtXferId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FeatureContent
-- ----------------------------
ALTER TABLE [dbo].[FeatureContent] ADD CONSTRAINT [PK__FeatureC__2907A81E5EEA596A] PRIMARY KEY CLUSTERED ([ContentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FeatureGroup
-- ----------------------------
ALTER TABLE [dbo].[FeatureGroup] ADD CONSTRAINT [PK__FeatureG__149AF36A3BF8AEC0] PRIMARY KEY CLUSTERED ([GroupId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FeatureGroupTXT
-- ----------------------------
ALTER TABLE [dbo].[FeatureGroupTXT] ADD CONSTRAINT [PK__FeatureG__B95A1A274E23BC11] PRIMARY KEY CLUSTERED ([GroupTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FeatureProperty
-- ----------------------------
ALTER TABLE [dbo].[FeatureProperty] ADD CONSTRAINT [PK__FeatureP__CF0C71AC6DB355F0] PRIMARY KEY CLUSTERED ([FeaturePropertyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table FeaturePropertyTXT
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_FeaturePropertyTXT]
ON [dbo].[FeaturePropertyTXT]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'FeaturePropertyTXT'
		,d.FeaturePropertyTXTId
		,'FeaturePropertyTXT record deleted'
		,d.Description
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  FeaturePropertyTXTId = d.FeaturePropertyTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_FeaturePropertyTXT]
ON [dbo].[FeaturePropertyTXT]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'FeaturePropertyTXT'
		,d.FeaturePropertyTXTId
		,'FeaturePropertyTXT record updated'
		,d.Description
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE FeaturePropertyTXTId = d.FeaturePropertyTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE FeaturePropertyTXTId = d.FeaturePropertyTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN FeaturePropertyTXT lu ON d.FeaturePropertyTXTId = lu.FeaturePropertyTXTId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE FeaturePropertyTXTId = d.FeaturePropertyTXTId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE FeaturePropertyTXTId = d.FeaturePropertyTXTId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table FeaturePropertyTXT
-- ----------------------------
ALTER TABLE [dbo].[FeaturePropertyTXT] ADD CONSTRAINT [PK__FeatureP__1956FE1D89796EA0] PRIMARY KEY CLUSTERED ([FeaturePropertyTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FeeRule
-- ----------------------------
ALTER TABLE [dbo].[FeeRule] ADD CONSTRAINT [PK__FeeRule__6A4753A9F31983FC] PRIMARY KEY CLUSTERED ([FeeRuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FieldProperty
-- ----------------------------
ALTER TABLE [dbo].[FieldProperty] ADD CONSTRAINT [PK__FieldPro__16D41E8B0D57EAC9] PRIMARY KEY CLUSTERED ([FieldPropertyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Financial
-- ----------------------------
ALTER TABLE [dbo].[Financial] ADD CONSTRAINT [PK__Financia__292681E040BF092D] PRIMARY KEY CLUSTERED ([FinancialId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FolderContent
-- ----------------------------
ALTER TABLE [dbo].[FolderContent] ADD CONSTRAINT [PK__FolderCo__2907A81E8FD9C5E0] PRIMARY KEY CLUSTERED ([ContentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Form
-- ----------------------------
ALTER TABLE [dbo].[Form] ADD CONSTRAINT [PK__Form__FB05B7DD908C7DEC] PRIMARY KEY CLUSTERED ([FormId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FormField
-- ----------------------------
ALTER TABLE [dbo].[FormField] ADD CONSTRAINT [PK__FormFiel__531C0C33666E2C67] PRIMARY KEY CLUSTERED ([FormFieldId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FormProcessor
-- ----------------------------
ALTER TABLE [dbo].[FormProcessor] ADD CONSTRAINT [PK__FormProc__CE8FE1891B8A3080] PRIMARY KEY CLUSTERED ([ProcessorId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FormTXT
-- ----------------------------
ALTER TABLE [dbo].[FormTXT] ADD CONSTRAINT [PK__FormTXT__6FFA71C420BA2721] PRIMARY KEY CLUSTERED ([FormTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FundingMethod
-- ----------------------------
ALTER TABLE [dbo].[FundingMethod] ADD CONSTRAINT [PK__FundingM__C4FA69B6D5BEE8CD] PRIMARY KEY CLUSTERED ([FundingMethodId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FundingMethodTXT
-- ----------------------------
ALTER TABLE [dbo].[FundingMethodTXT] ADD CONSTRAINT [PK__FundingM__94E6A60A7DEF1FBA] PRIMARY KEY CLUSTERED ([FundingMethodTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table FundingSource
-- ----------------------------
ALTER TABLE [dbo].[FundingSource] ADD CONSTRAINT [PK__FundingS__84F3EF41B07A477E] PRIMARY KEY CLUSTERED ([FundingSourceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Geography
-- ----------------------------
ALTER TABLE [dbo].[Geography] ADD CONSTRAINT [PK__Geograph__414FBF171D554046] PRIMARY KEY CLUSTERED ([GeographyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table GlAcct
-- ----------------------------
ALTER TABLE [dbo].[GlAcct] ADD CONSTRAINT [PK__GlAcct__F606CF28F61F3B59] PRIMARY KEY CLUSTERED ([GlAcctId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table GroupPage
-- ----------------------------
ALTER TABLE [dbo].[GroupPage] ADD CONSTRAINT [PK__GroupPag__C565B104CB1A307C] PRIMARY KEY CLUSTERED ([PageId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table GroupPageTXT
-- ----------------------------
ALTER TABLE [dbo].[GroupPageTXT] ADD CONSTRAINT [PK__GroupPag__954D16EA73C73118] PRIMARY KEY CLUSTERED ([PageTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table HoldItem
-- ----------------------------
ALTER TABLE [dbo].[HoldItem] ADD CONSTRAINT [PK__HoldItem__C43C6953EE0CAFFA] PRIMARY KEY CLUSTERED ([HoldItemId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table HotFile
-- ----------------------------
ALTER TABLE [dbo].[HotFile] ADD CONSTRAINT [PK__HotFile__5B1A60668E3AD1F6] PRIMARY KEY CLUSTERED ([HotFileIdId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table IDAuthMatrix
-- ----------------------------
ALTER TABLE [dbo].[IDAuthMatrix] ADD CONSTRAINT [PK__IDAuthMa__E6A395AA1BA529B2] PRIMARY KEY CLUSTERED ([IDAuthMatrixId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Identification
-- ----------------------------
ALTER TABLE [dbo].[Identification] ADD CONSTRAINT [PK__Identifi__48C6F98D5888F0C8] PRIMARY KEY CLUSTERED ([IdentificationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table IdInformation
-- ----------------------------
ALTER TABLE [dbo].[IdInformation] ADD CONSTRAINT [PK__IdInform__48C6F98D343F3278] PRIMARY KEY CLUSTERED ([IdentificationId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Institution
-- ----------------------------
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [PK__Institut__C262FD46AFB36DC5] PRIMARY KEY CLUSTERED ([FinInstId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table InstitutionAttribute
-- ----------------------------
ALTER TABLE [dbo].[InstitutionAttribute] ADD CONSTRAINT [PK__Institut__C18929EAF15C27A9] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table InstitutionTXT
-- ----------------------------
ALTER TABLE [dbo].[InstitutionTXT] ADD CONSTRAINT [PK__Institut__8B8B5DE71300E7C9] PRIMARY KEY CLUSTERED ([InstitutionTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table InventoryItem
-- ----------------------------
ALTER TABLE [dbo].[InventoryItem] ADD CONSTRAINT [PK__Inventor__1BB29F5CF8FC4659] PRIMARY KEY CLUSTERED ([InvItemId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table InventoryItemType
-- ----------------------------
ALTER TABLE [dbo].[InventoryItemType] ADD CONSTRAINT [PK__Inventor__DAE57BB87E6119B6] PRIMARY KEY CLUSTERED ([InvItemTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table InventoryItemTypeTXT
-- ----------------------------
ALTER TABLE [dbo].[InventoryItemTypeTXT] ADD CONSTRAINT [PK__Inventor__6E891F366901A0C3] PRIMARY KEY CLUSTERED ([ItemTypeTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table IssuedIdentRule
-- ----------------------------
ALTER TABLE [dbo].[IssuedIdentRule] ADD CONSTRAINT [PK__IssuedId__8667C8DB24BFF8ED] PRIMARY KEY CLUSTERED ([IssuedIdentRuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table IssuedIdentRuleAttribute
-- ----------------------------
ALTER TABLE [dbo].[IssuedIdentRuleAttribute] ADD CONSTRAINT [PK__IssuedId__C18929EAA40ADEC1] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Journal
-- ----------------------------
ALTER TABLE [dbo].[Journal] ADD CONSTRAINT [PK__Journal__250103E61012078E] PRIMARY KEY CLUSTERED ([JournalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JournalDenomination
-- ----------------------------
ALTER TABLE [dbo].[JournalDenomination] ADD CONSTRAINT [PK__JournalD__22DF18E54C573A5B] PRIMARY KEY CLUSTERED ([DenominationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JournalItem
-- ----------------------------
ALTER TABLE [dbo].[JournalItem] ADD CONSTRAINT [PK__JournalI__727E838B7DC668CE] PRIMARY KEY CLUSTERED ([ItemId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JournalItemPost
-- ----------------------------
ALTER TABLE [dbo].[JournalItemPost] ADD CONSTRAINT [PK__JournalI__F4C09B05E7366FFA] PRIMARY KEY CLUSTERED ([ItemPostId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JournalItemPostExt
-- ----------------------------
ALTER TABLE [dbo].[JournalItemPostExt] ADD CONSTRAINT [PK__JournalI__1A9074BD3F11706E] PRIMARY KEY CLUSTERED ([ItemPostExtId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JournalPost
-- ----------------------------
ALTER TABLE [dbo].[JournalPost] ADD CONSTRAINT [PK__JournalP__AA1260181DCC620D] PRIMARY KEY CLUSTERED ([PostId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JournalPostExt
-- ----------------------------
ALTER TABLE [dbo].[JournalPostExt] ADD CONSTRAINT [PK__JournalP__98904B3848B5E855] PRIMARY KEY CLUSTERED ([PostExtId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlAttribute
-- ----------------------------
ALTER TABLE [dbo].[JrnlAttribute] ADD CONSTRAINT [PK__JrnlAttr__C18929EA33F34635] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlItemDispenser
-- ----------------------------
ALTER TABLE [dbo].[JrnlItemDispenser] ADD CONSTRAINT [PK__JrnlItem__1335D4EE3176C2EB] PRIMARY KEY CLUSTERED ([DispenserId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlItemRestriction
-- ----------------------------
ALTER TABLE [dbo].[JrnlItemRestriction] ADD CONSTRAINT [PK__JrnlItem__1A21279BB75C8C78] PRIMARY KEY CLUSTERED ([JrnlItemRestrictionId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlItemType
-- ----------------------------
ALTER TABLE [dbo].[JrnlItemType] ADD CONSTRAINT [PK__JrnlItem__F51540FB9CAB5875] PRIMARY KEY CLUSTERED ([ItemTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlItemTypeTXT
-- ----------------------------
ALTER TABLE [dbo].[JrnlItemTypeTXT] ADD CONSTRAINT [PK__JrnlItem__6E891F36C5DA19A0] PRIMARY KEY CLUSTERED ([ItemTypeTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlItemVerification
-- ----------------------------
ALTER TABLE [dbo].[JrnlItemVerification] ADD CONSTRAINT [PK__JrnlItem__DCA068B737A598C3] PRIMARY KEY CLUSTERED ([JrnlItemVerificationId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlItemVerifyAttribute
-- ----------------------------
ALTER TABLE [dbo].[JrnlItemVerifyAttribute] ADD CONSTRAINT [PK__JrnlItem__C18929EA89516E67] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlOrder
-- ----------------------------
ALTER TABLE [dbo].[JrnlOrder] ADD CONSTRAINT [PK__JrnlOrde__250103E6AE3F2125] PRIMARY KEY CLUSTERED ([JournalId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlRestriction
-- ----------------------------
ALTER TABLE [dbo].[JrnlRestriction] ADD CONSTRAINT [PK__JrnlRest__01A3115B341D95E2] PRIMARY KEY CLUSTERED ([JrnlRestrictionId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table JrnlTotals
-- ----------------------------
ALTER TABLE [dbo].[JrnlTotals] ADD CONSTRAINT [PK__JrnlTota__A6B2C8BBA9AEE0AD] PRIMARY KEY CLUSTERED ([JrnlTtlId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table KeywordMap
-- ----------------------------
ALTER TABLE [dbo].[KeywordMap] ADD CONSTRAINT [PK__KeywordM__72E02C2036E61EE2] PRIMARY KEY CLUSTERED ([KeywordMapId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table LcrParty
-- ----------------------------
ALTER TABLE [dbo].[LcrParty] ADD CONSTRAINT [PK__LcrParty__9B425019E89F77D9] PRIMARY KEY CLUSTERED ([LcrPartyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table LcrPartyTran
-- ----------------------------
ALTER TABLE [dbo].[LcrPartyTran] ADD CONSTRAINT [PK__LcrParty__09B4716B20F354F8] PRIMARY KEY CLUSTERED ([LcrPartyTranId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table LoanEntry
-- ----------------------------
ALTER TABLE [dbo].[LoanEntry] ADD CONSTRAINT [PK__LoanEntr__885AA7E382EB6F8B] PRIMARY KEY CLUSTERED ([LoanEntryId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table LoanEntryHistory
-- ----------------------------
ALTER TABLE [dbo].[LoanEntryHistory] ADD CONSTRAINT [PK__LoanEntr__957B16715DF6D265] PRIMARY KEY CLUSTERED ([LoanEntryHistoryId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table LocalContent
-- ----------------------------
ALTER TABLE [dbo].[LocalContent] ADD CONSTRAINT [PK__LocalCon__00533330771A9560] PRIMARY KEY CLUSTERED ([LocalContentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ManualTaskDef
-- ----------------------------
ALTER TABLE [dbo].[ManualTaskDef] ADD CONSTRAINT [PK__ManualTa__67F45C7A5D8B81A6] PRIMARY KEY CLUSTERED ([ManualTaskDefId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ManualTaskDefTXT
-- ----------------------------
ALTER TABLE [dbo].[ManualTaskDefTXT] ADD CONSTRAINT [PK__ManualTa__5938BB3C7C9B42AD] PRIMARY KEY CLUSTERED ([ManualTaskDefTxtId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MarketGroup
-- ----------------------------
ALTER TABLE [dbo].[MarketGroup] ADD CONSTRAINT [PK__MarketGr__ED20B4F01F8D22CD] PRIMARY KEY CLUSTERED ([MarketGroupId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MarketGroupTXT
-- ----------------------------
ALTER TABLE [dbo].[MarketGroupTXT] ADD CONSTRAINT [PK__MarketGr__68944A09017B296F] PRIMARY KEY CLUSTERED ([MarketTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MarketingSource
-- ----------------------------
ALTER TABLE [dbo].[MarketingSource] ADD CONSTRAINT [PK__Marketin__ED954EDA161E711D] PRIMARY KEY CLUSTERED ([MarketingSourceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MediaType
-- ----------------------------
ALTER TABLE [dbo].[MediaType] ADD CONSTRAINT [PK__MediaTyp__0E6FCB727C328210] PRIMARY KEY CLUSTERED ([MediaTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MerchantCatCode
-- ----------------------------
ALTER TABLE [dbo].[MerchantCatCode] ADD CONSTRAINT [PK__Merchant__5E593E4FF072E0E0] PRIMARY KEY CLUSTERED ([CatCode])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MIRAggregation
-- ----------------------------
ALTER TABLE [dbo].[MIRAggregation] ADD CONSTRAINT [PK__MIRAggre__14597F0EBDF058A5] PRIMARY KEY CLUSTERED ([MirAggregationId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MirParty
-- ----------------------------
ALTER TABLE [dbo].[MirParty] ADD CONSTRAINT [PK__MirParty__057B6809B61E73FA] PRIMARY KEY CLUSTERED ([MirPartyId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MiscValue
-- ----------------------------
ALTER TABLE [dbo].[MiscValue] ADD CONSTRAINT [PK__MiscValu__ED1E8B53A7F1D119] PRIMARY KEY CLUSTERED ([MiscValueId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table MobileTokenAudit
-- ----------------------------
ALTER TABLE [dbo].[MobileTokenAudit] ADD CONSTRAINT [PK__MobileTo__C5B19602EA5FC60E] PRIMARY KEY CLUSTERED ([UID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table ModuleFeature
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_ModuleFeature]
ON [dbo].[ModuleFeature]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ModuleFeature'
		,d.FeatureId
		,'ModuleFeature record deleted'
		,d.FeatureName
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  FeatureId = d.FeatureId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_ModuleFeature]
ON [dbo].[ModuleFeature]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ModuleFeature'
		,d.FeatureId
		,'ModuleFeature record updated'
		,d.FeatureName
		,'default'
		,ISNULL(d.UpdatorDate, d.CreatedDate)
		,ISNULL(d.UpdatorUID, d.CreatorUID)
		,ISNULL(d.UpdatorUSR, d.CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE FeatureId = d.FeatureId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE FeatureId = d.FeatureId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN ModuleFeature lu ON d.FeatureId = lu.FeatureId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE FeatureId = d.FeatureId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE FeatureId = d.FeatureId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table ModuleFeature
-- ----------------------------
ALTER TABLE [dbo].[ModuleFeature] ADD CONSTRAINT [PK__ModuleFe__82230BC9B57B3BE2] PRIMARY KEY CLUSTERED ([FeatureId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table ModuleFeatureTXT
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_ModuleFeatureTXT]
ON [dbo].[ModuleFeatureTXT]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ModuleFeatureTXT'
		,d.ModFeatureTXTId
		,'ModuleFeatureTXT record deleted'
		,d.Description
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE  ModFeatureTXTId = d.ModFeatureTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_ModuleFeatureTXT]
ON [dbo].[ModuleFeatureTXT]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ModuleFeatureTXT'
		,d.ModFeatureTXTId
		,'ModuleFeatureTXT record updated'
		,d.Description
		,'default'
		,NULL
		,NULL
		,NULL
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ModFeatureTXTId = d.ModFeatureTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE ModFeatureTXTId = d.ModFeatureTXTId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN ModuleFeatureTXT lu ON d.ModFeatureTXTId = lu.ModFeatureTXTId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE ModFeatureTXTId = d.ModFeatureTXTId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE ModFeatureTXTId = d.ModFeatureTXTId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table ModuleFeatureTXT
-- ----------------------------
ALTER TABLE [dbo].[ModuleFeatureTXT] ADD CONSTRAINT [PK__ModuleFe__752F1760A0970A8C] PRIMARY KEY CLUSTERED ([ModFeatureTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table NAICSCode
-- ----------------------------
ALTER TABLE [dbo].[NAICSCode] ADD CONSTRAINT [PK__NAICSCod__1F5B06A8F63C617E] PRIMARY KEY CLUSTERED ([NAICSCodeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table NeedsAssessment
-- ----------------------------
ALTER TABLE [dbo].[NeedsAssessment] ADD CONSTRAINT [PK__NeedsAss__3D2BF81E70F7D4CD] PRIMARY KEY CLUSTERED ([AssessmentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table NewAcct
-- ----------------------------
ALTER TABLE [dbo].[NewAcct] ADD CONSTRAINT [PK__NewAcct__402CCBFCC864AB4E] PRIMARY KEY CLUSTERED ([AcctId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Node
-- ----------------------------
ALTER TABLE [dbo].[Node] ADD CONSTRAINT [PK__Node__6BAE2263DC01BCF9] PRIMARY KEY CLUSTERED ([NodeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Note
-- ----------------------------
ALTER TABLE [dbo].[Note] ADD CONSTRAINT [PK__Note__EACE355FB1CB5293] PRIMARY KEY CLUSTERED ([NoteId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table NotificationDevice
-- ----------------------------
ALTER TABLE [dbo].[NotificationDevice] ADD CONSTRAINT [PK__Notifica__8B72B99877B50B6B] PRIMARY KEY CLUSTERED ([NotificationDeviceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table NotificationDeviceRegister
-- ----------------------------
ALTER TABLE [dbo].[NotificationDeviceRegister] ADD CONSTRAINT [PK__Notifica__89A2B42F1537DB67] PRIMARY KEY CLUSTERED ([NotificationRegId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table NotificationLog
-- ----------------------------
ALTER TABLE [dbo].[NotificationLog] ADD CONSTRAINT [PK__Notifica__DD34941274737B91] PRIMARY KEY CLUSTERED ([NotificationLogId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Objection
-- ----------------------------
ALTER TABLE [dbo].[Objection] ADD CONSTRAINT [PK__Objectio__279E062BDCB174B8] PRIMARY KEY CLUSTERED ([ObjectionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ObjectionTXT
-- ----------------------------
ALTER TABLE [dbo].[ObjectionTXT] ADD CONSTRAINT [PK__Objectio__0554190E65A6C088] PRIMARY KEY CLUSTERED ([ObjectionTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferAuditAction
-- ----------------------------
ALTER TABLE [dbo].[OfferAuditAction] ADD CONSTRAINT [PK__OfferAud__DF81CD16BC007B08] PRIMARY KEY CLUSTERED ([OfferAuditActionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferContact
-- ----------------------------
ALTER TABLE [dbo].[OfferContact] ADD CONSTRAINT [PK__OfferCon__0E338971B26FE76D] PRIMARY KEY CLUSTERED ([OfferContactId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferContactAttr
-- ----------------------------
ALTER TABLE [dbo].[OfferContactAttr] ADD CONSTRAINT [PK__OfferCon__5632863174218088] PRIMARY KEY CLUSTERED ([OfferConAttrId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferGoal
-- ----------------------------
ALTER TABLE [dbo].[OfferGoal] ADD CONSTRAINT [PK__OfferGoa__99E26720E84C3E83] PRIMARY KEY CLUSTERED ([OfferGoalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferGoalTXT
-- ----------------------------
ALTER TABLE [dbo].[OfferGoalTXT] ADD CONSTRAINT [PK__OfferGoa__36103655F5308945] PRIMARY KEY CLUSTERED ([OfferGoalTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferMsgBanner
-- ----------------------------
ALTER TABLE [dbo].[OfferMsgBanner] ADD CONSTRAINT [PK__OfferMsg__FD00203E5C56C277] PRIMARY KEY CLUSTERED ([OfferMsgBannerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferMsgBannerTXT
-- ----------------------------
ALTER TABLE [dbo].[OfferMsgBannerTXT] ADD CONSTRAINT [PK__OfferMsg__3DCCCFFD36C62E41] PRIMARY KEY CLUSTERED ([OfferMsgBannerTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferStatus
-- ----------------------------
ALTER TABLE [dbo].[OfferStatus] ADD CONSTRAINT [PK__OfferSta__ECBDB3E893FAA30A] PRIMARY KEY CLUSTERED ([OfferStatusId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfferWatcher
-- ----------------------------
ALTER TABLE [dbo].[OfferWatcher] ADD CONSTRAINT [PK__OfferWat__5681C8C753F74651] PRIMARY KEY CLUSTERED ([OfferWatcherId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OfficerSubject
-- ----------------------------
ALTER TABLE [dbo].[OfficerSubject] ADD CONSTRAINT [PK__OfficerS__1310DE95DEA9F3EA] PRIMARY KEY CLUSTERED ([OfficerSubjectId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OrgGroup
-- ----------------------------
ALTER TABLE [dbo].[OrgGroup] ADD CONSTRAINT [PK__OrgGroup__149AF36A9941004A] PRIMARY KEY CLUSTERED ([GroupId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OrgGroupTXT
-- ----------------------------
ALTER TABLE [dbo].[OrgGroupTXT] ADD CONSTRAINT [PK__OrgGroup__B95A1A274477FF57] PRIMARY KEY CLUSTERED ([GroupTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table OverrideMatrix
-- ----------------------------
ALTER TABLE [dbo].[OverrideMatrix] ADD CONSTRAINT [PK__Override__0A7EADCA6EA0CEFA] PRIMARY KEY CLUSTERED ([OverrideMatrixId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table P2PPayment
-- ----------------------------
ALTER TABLE [dbo].[P2PPayment] ADD CONSTRAINT [PK__P2PPayme__9B556A381B1FC8F2] PRIMARY KEY CLUSTERED ([PaymentId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PageField
-- ----------------------------
ALTER TABLE [dbo].[PageField] ADD CONSTRAINT [PK__PageFiel__C8B6FF07DFC17DFE] PRIMARY KEY CLUSTERED ([FieldId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PageFieldTXT
-- ----------------------------
ALTER TABLE [dbo].[PageFieldTXT] ADD CONSTRAINT [PK__PageFiel__1C18A60492DBBB2E] PRIMARY KEY CLUSTERED ([FieldTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PartyBalance
-- ----------------------------
ALTER TABLE [dbo].[PartyBalance] ADD CONSTRAINT [PK__PartyBal__28C8161FC4D43BA0] PRIMARY KEY CLUSTERED ([PartyBalanceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PartyContact
-- ----------------------------
ALTER TABLE [dbo].[PartyContact] ADD CONSTRAINT [PK__PartyCon__A2B6752F34A2705E] PRIMARY KEY CLUSTERED ([PartyContactId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PartyRelVerify
-- ----------------------------
ALTER TABLE [dbo].[PartyRelVerify] ADD CONSTRAINT [PK__PartyRel__28A6DEF57518C38B] PRIMARY KEY CLUSTERED ([PartyRelVerifyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PartyRelVerifyRes
-- ----------------------------
ALTER TABLE [dbo].[PartyRelVerifyRes] ADD CONSTRAINT [PK__PartyRel__D227D158A968E7FF] PRIMARY KEY CLUSTERED ([PtyRelVerifyResId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PartyRelVerifyRole
-- ----------------------------
ALTER TABLE [dbo].[PartyRelVerifyRole] ADD CONSTRAINT [PK__PartyRel__007117CE7204B3E7] PRIMARY KEY CLUSTERED ([PtyRelVerifyRoleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PaymentTemplate
-- ----------------------------
ALTER TABLE [dbo].[PaymentTemplate] ADD CONSTRAINT [PK__PaymentT__F2EEE91EC429A113] PRIMARY KEY CLUSTERED ([PayTemplateId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Peripheral
-- ----------------------------
ALTER TABLE [dbo].[Peripheral] ADD CONSTRAINT [PK__Peripher__17994575F457BC7B] PRIMARY KEY CLUSTERED ([PeripheralId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PeripheralAttribute
-- ----------------------------
ALTER TABLE [dbo].[PeripheralAttribute] ADD CONSTRAINT [PK__Peripher__C18929EA3187EA2B] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PermitEntry
-- ----------------------------
ALTER TABLE [dbo].[PermitEntry] ADD CONSTRAINT [PK__PermitEn__255990724DED5134] PRIMARY KEY CLUSTERED ([PermitEntryId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PermitEntryTXT
-- ----------------------------
ALTER TABLE [dbo].[PermitEntryTXT] ADD CONSTRAINT [PK__PermitEn__09322231752D24ED] PRIMARY KEY CLUSTERED ([PermitEntryTXTId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PermitModule
-- ----------------------------
ALTER TABLE [dbo].[PermitModule] ADD CONSTRAINT [PK__PermitMo__19E43C4164DCA390] PRIMARY KEY CLUSTERED ([PermitModuleId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PermitModuleTXT
-- ----------------------------
ALTER TABLE [dbo].[PermitModuleTXT] ADD CONSTRAINT [PK__PermitMo__55BE7477A1F00B05] PRIMARY KEY CLUSTERED ([PermitModuleTXTId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PermitSubject
-- ----------------------------
ALTER TABLE [dbo].[PermitSubject] ADD CONSTRAINT [PK__PermitSu__473393B50BE90E19] PRIMARY KEY CLUSTERED ([PermitSubjectId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PermitValue
-- ----------------------------
ALTER TABLE [dbo].[PermitValue] ADD CONSTRAINT [PK__PermitVa__8F6D6BAF581E83A3] PRIMARY KEY CLUSTERED ([PermitValueId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PfmTrnTag
-- ----------------------------
ALTER TABLE [dbo].[PfmTrnTag] ADD CONSTRAINT [PK__PfmTrnTa__3572C732B6F37AFA] PRIMARY KEY CLUSTERED ([PfmTrnTagId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PfmTrnText
-- ----------------------------
ALTER TABLE [dbo].[PfmTrnText] ADD CONSTRAINT [PK__PfmTrnTe__28FA13C93246E76D] PRIMARY KEY CLUSTERED ([PfmTrnTextId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table PhysicalAddress
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_PhysicalAddress]
ON [dbo].[PhysicalAddress]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'PhysicalAddress'
		,@ApplId
		,'PhysicalAddress record deleted'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE PysicalAddrId = d.PysicalAddrId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_PhysicalAddress]
ON [dbo].[PhysicalAddress]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'PhysicalAddress'
		,@ApplId
		,'PhysicalAddress record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE PysicalAddrId = d.PysicalAddrId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE PysicalAddrId = d.PysicalAddrId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN PhysicalAddress lu ON d.PysicalAddrId = lu.PysicalAddrId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE PysicalAddrId = d.PysicalAddrId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE PysicalAddrId = d.PysicalAddrId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table PhysicalAddress
-- ----------------------------
ALTER TABLE [dbo].[PhysicalAddress] ADD CONSTRAINT [PK__Physical__1666A597A7462C1F] PRIMARY KEY CLUSTERED ([PysicalAddrId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PkgProdProduct
-- ----------------------------
ALTER TABLE [dbo].[PkgProdProduct] ADD CONSTRAINT [PK__PkgProdP__F960F9A095A431A7] PRIMARY KEY CLUSTERED ([PackageId], [ProductId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PreferenceValue
-- ----------------------------
ALTER TABLE [dbo].[PreferenceValue] ADD CONSTRAINT [PK__Preferen__E83B5288F4AA985F] PRIMARY KEY CLUSTERED ([PreferenceValueId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PricingModel
-- ----------------------------
ALTER TABLE [dbo].[PricingModel] ADD CONSTRAINT [PK__PricingM__0567295BB68BD969] PRIMARY KEY CLUSTERED ([PricingModelId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PricingModelTXT
-- ----------------------------
ALTER TABLE [dbo].[PricingModelTXT] ADD CONSTRAINT [PK__PricingM__0FAC808D7B8E7A7E] PRIMARY KEY CLUSTERED ([ModelTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Printer
-- ----------------------------
ALTER TABLE [dbo].[Printer] ADD CONSTRAINT [PK__Printer__D452AAC1016C65C1] PRIMARY KEY CLUSTERED ([PrinterId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PrinterTXT
-- ----------------------------
ALTER TABLE [dbo].[PrinterTXT] ADD CONSTRAINT [PK__PrinterT__2DF0DEB38DAE779C] PRIMARY KEY CLUSTERED ([PrinterTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProcessDefinition
-- ----------------------------
ALTER TABLE [dbo].[ProcessDefinition] ADD CONSTRAINT [PK__ProcessD__9B407C7277A92956] PRIMARY KEY CLUSTERED ([CaseProcessId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProcessDefTXT
-- ----------------------------
ALTER TABLE [dbo].[ProcessDefTXT] ADD CONSTRAINT [PK__ProcessD__E4A64F1629657E5F] PRIMARY KEY CLUSTERED ([ProcessTxtId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProcessorRule
-- ----------------------------
ALTER TABLE [dbo].[ProcessorRule] ADD CONSTRAINT [PK__Processo__A0B9538F5A3025AA] PRIMARY KEY CLUSTERED ([ProcessorRuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProcessorRuleSet
-- ----------------------------
ALTER TABLE [dbo].[ProcessorRuleSet] ADD CONSTRAINT [PK__Processo__D046E843ECACEB42] PRIMARY KEY CLUSTERED ([ProcessorRuleSetId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProcessorTXT
-- ----------------------------
ALTER TABLE [dbo].[ProcessorTXT] ADD CONSTRAINT [PK__Processo__E82CEE1F76A01D25] PRIMARY KEY CLUSTERED ([ProcessorTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table ProcessStatus
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_ProcessStatus]
ON [dbo].[ProcessStatus]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ProcessStatus'
		,@ApplId
		,'ProcessStatus record deleted'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ProcessStatusId = d.ProcessStatusId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_ProcessStatus]
ON [dbo].[ProcessStatus]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM DELETED
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'ProcessStatus'
		,@ApplId
		,'ProcessStatus record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE ProcessStatusId = d.ProcessStatusId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE ProcessStatusId = d.ProcessStatusId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN ProcessStatus lu ON d.ProcessStatusId = lu.ProcessStatusId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE ProcessStatusId = d.ProcessStatusId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE ProcessStatusId = d.ProcessStatusId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table ProcessStatus
-- ----------------------------
ALTER TABLE [dbo].[ProcessStatus] ADD CONSTRAINT [PK__ProcessS__5F8648C54663557B] PRIMARY KEY CLUSTERED ([ProcessStatusId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdAttribute
-- ----------------------------
ALTER TABLE [dbo].[ProdAttribute] ADD CONSTRAINT [PK__ProdAttr__C18929EA347323CC] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdFeature
-- ----------------------------
ALTER TABLE [dbo].[ProdFeature] ADD CONSTRAINT [PK__ProdFeat__EA2C0E50CD6319C6] PRIMARY KEY CLUSTERED ([ProdFeatureId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdFeatureTXT
-- ----------------------------
ALTER TABLE [dbo].[ProdFeatureTXT] ADD CONSTRAINT [PK__ProdFeat__48B2A2D55B169D3F] PRIMARY KEY CLUSTERED ([ProdFeatureTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdOfferAttr
-- ----------------------------
ALTER TABLE [dbo].[ProdOfferAttr] ADD CONSTRAINT [PK__ProdOffe__BA53A5F53459D239] PRIMARY KEY CLUSTERED ([ProdOfferAttrId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdOfferContact
-- ----------------------------
ALTER TABLE [dbo].[ProdOfferContact] ADD CONSTRAINT [PK__ProdOffe__5CD24CED39829CC4] PRIMARY KEY CLUSTERED ([ProdOfferContactId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdService
-- ----------------------------
ALTER TABLE [dbo].[ProdService] ADD CONSTRAINT [PK__ProdServ__702B4CC41AC7D50D] PRIMARY KEY CLUSTERED ([ProdServiceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdServiceTXT
-- ----------------------------
ALTER TABLE [dbo].[ProdServiceTXT] ADD CONSTRAINT [PK__ProdServ__CEB7CE4FADD76154] PRIMARY KEY CLUSTERED ([ProdServiceTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProdTranDef
-- ----------------------------
ALTER TABLE [dbo].[ProdTranDef] ADD CONSTRAINT [PK__ProdTran__991D372D078D13E4] PRIMARY KEY CLUSTERED ([ProdTranDefId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Product
-- ----------------------------
ALTER TABLE [dbo].[Product] ADD CONSTRAINT [PK__Product__B40CC6CD0BF5B204] PRIMARY KEY CLUSTERED ([ProductId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductForm
-- ----------------------------
ALTER TABLE [dbo].[ProductForm] ADD CONSTRAINT [PK__ProductF__52675AB7FDBA07C0] PRIMARY KEY CLUSTERED ([ProductFormId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductOffer
-- ----------------------------
ALTER TABLE [dbo].[ProductOffer] ADD CONSTRAINT [PK__ProductO__F3A60B72C729748A] PRIMARY KEY CLUSTERED ([ProductOfferId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductOfferAudit
-- ----------------------------
ALTER TABLE [dbo].[ProductOfferAudit] ADD CONSTRAINT [PK__ProductO__AF3E30BB53621F1E] PRIMARY KEY CLUSTERED ([OfferAuditId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductOfferTXT
-- ----------------------------
ALTER TABLE [dbo].[ProductOfferTXT] ADD CONSTRAINT [PK__ProductO__163E54618994EAD2] PRIMARY KEY CLUSTERED ([ProductOfferTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductRole
-- ----------------------------
ALTER TABLE [dbo].[ProductRole] ADD CONSTRAINT [PK__ProductR__E5D91894C9753E0A] PRIMARY KEY CLUSTERED ([ProductRoleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductTier
-- ----------------------------
ALTER TABLE [dbo].[ProductTier] ADD CONSTRAINT [PK__ProductT__D335986EF938E908] PRIMARY KEY CLUSTERED ([ProductTierId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductTree
-- ----------------------------
ALTER TABLE [dbo].[ProductTree] ADD CONSTRAINT [PK__ProductT__7F80B366871C1537] PRIMARY KEY CLUSTERED ([ProdTreeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProductTXT
-- ----------------------------
ALTER TABLE [dbo].[ProductTXT] ADD CONSTRAINT [PK__ProductT__E575D45BE50C1B48] PRIMARY KEY CLUSTERED ([ProdTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfileFeature
-- ----------------------------
ALTER TABLE [dbo].[ProfileFeature] ADD CONSTRAINT [PK__ProfileF__BEB27208968AB784] PRIMARY KEY CLUSTERED ([FeatureKeyId], [ModuleKeyId], [ProfileKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfileFeatureGroup
-- ----------------------------
ALTER TABLE [dbo].[ProfileFeatureGroup] ADD CONSTRAINT [PK__ProfileF__D66242B1F8CC6838] PRIMARY KEY CLUSTERED ([GroupKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfileFeatureProperty
-- ----------------------------
ALTER TABLE [dbo].[ProfileFeatureProperty] ADD CONSTRAINT [PK__ProfileF__7C03C567F274A61F] PRIMARY KEY CLUSTERED ([FeatureKeyId], [ModuleKeyId], [ProfileKeyId], [PropertyKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfileFieldProperty
-- ----------------------------
ALTER TABLE [dbo].[ProfileFieldProperty] ADD CONSTRAINT [PK__ProfileF__EEDA9850C3237719] PRIMARY KEY CLUSTERED ([FieldPropKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfileGroupPage
-- ----------------------------
ALTER TABLE [dbo].[ProfileGroupPage] ADD CONSTRAINT [PK__ProfileG__0838BAE864492EC2] PRIMARY KEY CLUSTERED ([PageKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfileModule
-- ----------------------------
ALTER TABLE [dbo].[ProfileModule] ADD CONSTRAINT [PK__ProfileM__9CC56213A8B63E6C] PRIMARY KEY CLUSTERED ([ModuleKeyId], [ProfileKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfilePageField
-- ----------------------------
ALTER TABLE [dbo].[ProfilePageField] ADD CONSTRAINT [PK__ProfileP__0D529C5B77FE5467] PRIMARY KEY CLUSTERED ([FieldKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ProfilePropValue
-- ----------------------------
ALTER TABLE [dbo].[ProfilePropValue] ADD CONSTRAINT [PK__ProfileP__CD2D6096D9390086] PRIMARY KEY CLUSTERED ([ProfileKeyId], [PropertyKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PromoChannel
-- ----------------------------
ALTER TABLE [dbo].[PromoChannel] ADD CONSTRAINT [PK__PromoCha__A1AD2AFCDE3B41EF] PRIMARY KEY CLUSTERED ([PromoChannelId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PromoChannelLink
-- ----------------------------
ALTER TABLE [dbo].[PromoChannelLink] ADD CONSTRAINT [PK__PromoCha__88DEFD605680B14E] PRIMARY KEY CLUSTERED ([PromotionId], [PromoChannelId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PromoCode
-- ----------------------------
ALTER TABLE [dbo].[PromoCode] ADD CONSTRAINT [PK__PromoCod__867BC5864B71F854] PRIMARY KEY CLUSTERED ([PromoCodeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PromoCodeTXT
-- ----------------------------
ALTER TABLE [dbo].[PromoCodeTXT] ADD CONSTRAINT [PK__PromoCod__2DFC38D7C01292AC] PRIMARY KEY CLUSTERED ([PromoCodeTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Promotion
-- ----------------------------
ALTER TABLE [dbo].[Promotion] ADD CONSTRAINT [PK__Promotio__52C42FCFA7D9A932] PRIMARY KEY CLUSTERED ([PromotionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PromotionTXT
-- ----------------------------
ALTER TABLE [dbo].[PromotionTXT] ADD CONSTRAINT [PK__Promotio__D5A10EAA07AEEC39] PRIMARY KEY CLUSTERED ([PromotionTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Proposition
-- ----------------------------
ALTER TABLE [dbo].[Proposition] ADD CONSTRAINT [PK__Proposit__9805AE89AB222224] PRIMARY KEY CLUSTERED ([PropositionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table PropositionTXT
-- ----------------------------
ALTER TABLE [dbo].[PropositionTXT] ADD CONSTRAINT [PK__Proposit__B250B9B5BD57261D] PRIMARY KEY CLUSTERED ([PropositionTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QuestionAnswer
-- ----------------------------
ALTER TABLE [dbo].[QuestionAnswer] ADD CONSTRAINT [PK__Question__86BEDFCF824F7FD3] PRIMARY KEY CLUSTERED ([QuestionAnswerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedCustomer
-- ----------------------------
ALTER TABLE [dbo].[QueuedCustomer] ADD CONSTRAINT [PK__QueuedCu__824F748AF0B6F28C] PRIMARY KEY CLUSTERED ([QueuedCustomerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedCustProp
-- ----------------------------
ALTER TABLE [dbo].[QueuedCustProp] ADD CONSTRAINT [PK__QueuedCu__07872C1FA95CF0F5] PRIMARY KEY CLUSTERED ([QuedCustPropId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedIdentifier
-- ----------------------------
ALTER TABLE [dbo].[QueuedIdentifier] ADD CONSTRAINT [PK__QueuedId__5EB5E676162DA0A4] PRIMARY KEY CLUSTERED ([IdentifierId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedIdentifierTXT
-- ----------------------------
ALTER TABLE [dbo].[QueuedIdentifierTXT] ADD CONSTRAINT [PK__QueuedId__1A7B525175A1DBE2] PRIMARY KEY CLUSTERED ([IdentifierTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedReason
-- ----------------------------
ALTER TABLE [dbo].[QueuedReason] ADD CONSTRAINT [PK__QueuedRe__A4F8C0E7DA3C919C] PRIMARY KEY CLUSTERED ([ReasonId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedReasonTXT
-- ----------------------------
ALTER TABLE [dbo].[QueuedReasonTXT] ADD CONSTRAINT [PK__QueuedRe__505FBFDC87547D5A] PRIMARY KEY CLUSTERED ([ReasonTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedStatus
-- ----------------------------
ALTER TABLE [dbo].[QueuedStatus] ADD CONSTRAINT [PK__QueuedSt__C8EE2063D7828B18] PRIMARY KEY CLUSTERED ([StatusId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedStatusHist
-- ----------------------------
ALTER TABLE [dbo].[QueuedStatusHist] ADD CONSTRAINT [PK__QueuedSt__4C6A788E529531A0] PRIMARY KEY CLUSTERED ([QueStatusHistId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedStatusTXT
-- ----------------------------
ALTER TABLE [dbo].[QueuedStatusTXT] ADD CONSTRAINT [PK__QueuedSt__23FEBF03C20B922D] PRIMARY KEY CLUSTERED ([StatusTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueuedTask
-- ----------------------------
ALTER TABLE [dbo].[QueuedTask] ADD CONSTRAINT [PK__QueuedTa__8E03157D85A6B403] PRIMARY KEY CLUSTERED ([QueuedTaskId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueueZone
-- ----------------------------
ALTER TABLE [dbo].[QueueZone] ADD CONSTRAINT [PK__QueueZon__601667B5DFF7BE21] PRIMARY KEY CLUSTERED ([ZoneId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table QueueZoneTXT
-- ----------------------------
ALTER TABLE [dbo].[QueueZoneTXT] ADD CONSTRAINT [PK__QueueZon__E27B76917F4486CA] PRIMARY KEY CLUSTERED ([ZoneTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RareErrorMap
-- ----------------------------
ALTER TABLE [dbo].[RareErrorMap] ADD CONSTRAINT [PK__RareErro__5A86018EE7D2BDAC] PRIMARY KEY CLUSTERED ([RareErrMapId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Recipient
-- ----------------------------
ALTER TABLE [dbo].[Recipient] ADD CONSTRAINT [PK__Recipien__F0A6024D5E37F85B] PRIMARY KEY CLUSTERED ([RecipientId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RecipientParty
-- ----------------------------
ALTER TABLE [dbo].[RecipientParty] ADD CONSTRAINT [PK__Recipien__90D4DD8CBE21877B] PRIMARY KEY CLUSTERED ([RecPartyId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ReconciliationEvent
-- ----------------------------
ALTER TABLE [dbo].[ReconciliationEvent] ADD CONSTRAINT [PK__Reconcil__7944C8102D22BA8E] PRIMARY KEY CLUSTERED ([EventId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegCC
-- ----------------------------
ALTER TABLE [dbo].[RegCC] ADD CONSTRAINT [PK__RegCC__F58D0C3AEDAE3EDA] PRIMARY KEY CLUSTERED ([RegCCId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegData
-- ----------------------------
ALTER TABLE [dbo].[RegData] ADD CONSTRAINT [PK__RegData__EB8AE60617E94ABF] PRIMARY KEY CLUSTERED ([RegDataId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegDataField
-- ----------------------------
ALTER TABLE [dbo].[RegDataField] ADD CONSTRAINT [PK__RegDataF__76F1352998694BB8] PRIMARY KEY CLUSTERED ([RegDataFieldId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegDFile
-- ----------------------------
ALTER TABLE [dbo].[RegDFile] ADD CONSTRAINT [PK__RegDFile__23DC25E2FC1BE0E4] PRIMARY KEY CLUSTERED ([RegDFileId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegEMatrix
-- ----------------------------
ALTER TABLE [dbo].[RegEMatrix] ADD CONSTRAINT [PK__RegEMatr__A6718871CE08C596] PRIMARY KEY CLUSTERED ([RegEMatrixId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegFormField
-- ----------------------------
ALTER TABLE [dbo].[RegFormField] ADD CONSTRAINT [PK__RegFormF__39B4D7D23E82AAE7] PRIMARY KEY CLUSTERED ([RegFormFieldId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegFormFieldTXT
-- ----------------------------
ALTER TABLE [dbo].[RegFormFieldTXT] ADD CONSTRAINT [PK__RegFormF__67A3672180403CF2] PRIMARY KEY CLUSTERED ([DisclosureTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Region
-- ----------------------------
ALTER TABLE [dbo].[Region] ADD CONSTRAINT [PK__Region__ACD844A3315742DB] PRIMARY KEY CLUSTERED ([RegionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegionAttribute
-- ----------------------------
ALTER TABLE [dbo].[RegionAttribute] ADD CONSTRAINT [PK__RegionAt__C18929EACF49FE3F] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegionTXT
-- ----------------------------
ALTER TABLE [dbo].[RegionTXT] ADD CONSTRAINT [PK__RegionTX__DC45BF8250D51E3D] PRIMARY KEY CLUSTERED ([RegionTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Registration
-- ----------------------------
ALTER TABLE [dbo].[Registration] ADD CONSTRAINT [PK__Registra__6EF58810878FF4B3] PRIMARY KEY CLUSTERED ([RegistrationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RegulatoryForm
-- ----------------------------
ALTER TABLE [dbo].[RegulatoryForm] ADD CONSTRAINT [PK__Regulato__94095D7B7C22ACD8] PRIMARY KEY CLUSTERED ([RegulatoryFormId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Reminder
-- ----------------------------
ALTER TABLE [dbo].[Reminder] ADD CONSTRAINT [PK__Reminder__01A83087DA37A668] PRIMARY KEY CLUSTERED ([ReminderId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ResourceDirectory
-- ----------------------------
ALTER TABLE [dbo].[ResourceDirectory] ADD CONSTRAINT [PK__Resource__4ED1816FC995184E] PRIMARY KEY CLUSTERED ([ResourceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RoleAttribute
-- ----------------------------
ALTER TABLE [dbo].[RoleAttribute] ADD CONSTRAINT [PK__RoleAttr__C18929EAAEF06783] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RoleProfile
-- ----------------------------
ALTER TABLE [dbo].[RoleProfile] ADD CONSTRAINT [PK__RoleProf__743A9E1ED9DA1FC7] PRIMARY KEY CLUSTERED ([RoleProfileId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RoutingNbr
-- ----------------------------
ALTER TABLE [dbo].[RoutingNbr] ADD CONSTRAINT [PK__RoutingN__1BA7DE888C4FBE65] PRIMARY KEY CLUSTERED ([RoutingNbrId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SalesScript
-- ----------------------------
ALTER TABLE [dbo].[SalesScript] ADD CONSTRAINT [PK__SalesScr__4948A86703928FB7] PRIMARY KEY CLUSTERED ([SalesScriptId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SalesScriptTXT
-- ----------------------------
ALTER TABLE [dbo].[SalesScriptTXT] ADD CONSTRAINT [PK__SalesScr__260B477F0B012DA8] PRIMARY KEY CLUSTERED ([SalesScriptTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Sanction
-- ----------------------------
ALTER TABLE [dbo].[Sanction] ADD CONSTRAINT [PK__Sanction__D2B11D1D514A8DA1] PRIMARY KEY CLUSTERED ([EntityId], [ListId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SanctionAddr
-- ----------------------------
ALTER TABLE [dbo].[SanctionAddr] ADD CONSTRAINT [PK__Sanction__D174AC8B035AD83C] PRIMARY KEY CLUSTERED ([EntityId], [ListId], [UID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SanctionIdNumber
-- ----------------------------
ALTER TABLE [dbo].[SanctionIdNumber] ADD CONSTRAINT [PK__Sanction__D174AC8B2C1ABD91] PRIMARY KEY CLUSTERED ([EntityId], [ListId], [UID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SanctionName
-- ----------------------------
ALTER TABLE [dbo].[SanctionName] ADD CONSTRAINT [PK__Sanction__D174AC8BDB7ECD8C] PRIMARY KEY CLUSTERED ([EntityId], [ListId], [UID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ScheduledTran
-- ----------------------------
ALTER TABLE [dbo].[ScheduledTran] ADD CONSTRAINT [PK__Schedule__D1610E089A34A47E] PRIMARY KEY CLUSTERED ([SchedTranId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ScriptDefTXT
-- ----------------------------
ALTER TABLE [dbo].[ScriptDefTXT] ADD CONSTRAINT [PK__ScriptDe__72F6CF1D74728E64] PRIMARY KEY CLUSTERED ([ScriptDefTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ScriptDefType
-- ----------------------------
ALTER TABLE [dbo].[ScriptDefType] ADD CONSTRAINT [PK__ScriptDe__968126D3E16542C8] PRIMARY KEY CLUSTERED ([ScriptDefId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ScriptFlow
-- ----------------------------
ALTER TABLE [dbo].[ScriptFlow] ADD CONSTRAINT [PK__ScriptFl__BCA8E5DC596A2846] PRIMARY KEY CLUSTERED ([ScriptFlowId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ScriptHistory
-- ----------------------------
ALTER TABLE [dbo].[ScriptHistory] ADD CONSTRAINT [PK__ScriptHi__F5C2042C833A7DF1] PRIMARY KEY CLUSTERED ([ScriptHistoryId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SearchKey
-- ----------------------------
ALTER TABLE [dbo].[SearchKey] ADD CONSTRAINT [PK__SearchKe__76B06A7289668A11] PRIMARY KEY CLUSTERED ([SearchKeyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SecMsgAttach
-- ----------------------------
ALTER TABLE [dbo].[SecMsgAttach] ADD CONSTRAINT [PK__SecMsgAt__49EB090C43484AFC] PRIMARY KEY CLUSTERED ([SecMsgAttachId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SecureMessage
-- ----------------------------
ALTER TABLE [dbo].[SecureMessage] ADD CONSTRAINT [PK__SecureMe__7C15A83D9DBFA678] PRIMARY KEY CLUSTERED ([SecMsgId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Service
-- ----------------------------
ALTER TABLE [dbo].[Service] ADD CONSTRAINT [PK__Service__C51BB00ABCBD396C] PRIMARY KEY CLUSTERED ([ServiceId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ServiceTran
-- ----------------------------
ALTER TABLE [dbo].[ServiceTran] ADD CONSTRAINT [PK__ServiceT__BD76DF0E646FC25C] PRIMARY KEY CLUSTERED ([ServiceTranId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ServiceTranTXT
-- ----------------------------
ALTER TABLE [dbo].[ServiceTranTXT] ADD CONSTRAINT [PK__ServiceT__43E94647BE8E65BA] PRIMARY KEY CLUSTERED ([ServiceTranTXTId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SessionPartyImage
-- ----------------------------
ALTER TABLE [dbo].[SessionPartyImage] ADD CONSTRAINT [PK__SessionP__05A0237AC7F27395] PRIMARY KEY CLUSTERED ([SessionPartyImageId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SettingCategory
-- ----------------------------
ALTER TABLE [dbo].[SettingCategory] ADD CONSTRAINT [PK__SettingC__E16C482A3650416A] PRIMARY KEY CLUSTERED ([SettingCatId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SettingCatTXT
-- ----------------------------
ALTER TABLE [dbo].[SettingCatTXT] ADD CONSTRAINT [PK__SettingC__5E89811E82DD05B9] PRIMARY KEY CLUSTERED ([SettingCatTXTId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SettingEntry
-- ----------------------------
ALTER TABLE [dbo].[SettingEntry] ADD CONSTRAINT [PK__SettingE__2EE5D74E36AA35C5] PRIMARY KEY CLUSTERED ([SettingEntryId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SettingEntryTXT
-- ----------------------------
ALTER TABLE [dbo].[SettingEntryTXT] ADD CONSTRAINT [PK__SettingE__504891A573E61E69] PRIMARY KEY CLUSTERED ([SettingEntryTXTId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SettingSubject
-- ----------------------------
ALTER TABLE [dbo].[SettingSubject] ADD CONSTRAINT [PK__SettingS__766603E9B79C52BB] PRIMARY KEY CLUSTERED ([SettingSubjectId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SettingValue
-- ----------------------------
ALTER TABLE [dbo].[SettingValue] ADD CONSTRAINT [PK__SettingV__B64B56A5B091FDC3] PRIMARY KEY CLUSTERED ([SettingValueId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowAcctDtls
-- ----------------------------
ALTER TABLE [dbo].[ShadowAcctDtls] ADD CONSTRAINT [PK__ShadowAc__33D29EBEBE73367E] PRIMARY KEY CLUSTERED ([AcctDtlsId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowAcctFees
-- ----------------------------
ALTER TABLE [dbo].[ShadowAcctFees] ADD CONSTRAINT [PK__ShadowAc__1E0FF20CD828EB29] PRIMARY KEY CLUSTERED ([AcctFeesId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowAcctHold
-- ----------------------------
ALTER TABLE [dbo].[ShadowAcctHold] ADD CONSTRAINT [PK__ShadowAc__B31582C7EB03BEB3] PRIMARY KEY CLUSTERED ([AcctHoldId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowAcctNotes
-- ----------------------------
ALTER TABLE [dbo].[ShadowAcctNotes] ADD CONSTRAINT [PK__ShadowAc__DED1FFA881D30AEA] PRIMARY KEY CLUSTERED ([AcctNotesId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowAddr
-- ----------------------------
ALTER TABLE [dbo].[ShadowAddr] ADD CONSTRAINT [PK__ShadowAd__BCDB8FC3F5FEE076] PRIMARY KEY CLUSTERED ([AddrId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowCDMatRenewals
-- ----------------------------
ALTER TABLE [dbo].[ShadowCDMatRenewals] ADD CONSTRAINT [PK__ShadowCD__FAC3025EE74B0A5C] PRIMARY KEY CLUSTERED ([CDMatRenewalsId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowExtAcctDtls
-- ----------------------------
ALTER TABLE [dbo].[ShadowExtAcctDtls] ADD CONSTRAINT [PK__ShadowEx__10C7736428AC368F] PRIMARY KEY CLUSTERED ([ExtAcctDtlsId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowFieldValue
-- ----------------------------
ALTER TABLE [dbo].[ShadowFieldValue] ADD CONSTRAINT [PK__ShadowFi__EF72F1F5602AA666] PRIMARY KEY CLUSTERED ([ShadowFieldId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowFunding
-- ----------------------------
ALTER TABLE [dbo].[ShadowFunding] ADD CONSTRAINT [PK__ShadowFu__6CCF25E9A3FCE13E] PRIMARY KEY CLUSTERED ([FundingId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowParty
-- ----------------------------
ALTER TABLE [dbo].[ShadowParty] ADD CONSTRAINT [PK__ShadowPa__1640CD33F0859CDB] PRIMARY KEY CLUSTERED ([PartyId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowProductDtls
-- ----------------------------
ALTER TABLE [dbo].[ShadowProductDtls] ADD CONSTRAINT [PK__ShadowPr__B05B30B58C9F3638] PRIMARY KEY CLUSTERED ([ProductDtlsId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShadowStmtDtls
-- ----------------------------
ALTER TABLE [dbo].[ShadowStmtDtls] ADD CONSTRAINT [PK__ShadowSt__E79DE3CF540EF029] PRIMARY KEY CLUSTERED ([StmtDtlsId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ShoppingCart
-- ----------------------------
ALTER TABLE [dbo].[ShoppingCart] ADD CONSTRAINT [PK__Shopping__7A789AE44444BC6B] PRIMARY KEY CLUSTERED ([ShoppingCartId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SrchKeyValue
-- ----------------------------
ALTER TABLE [dbo].[SrchKeyValue] ADD CONSTRAINT [PK__SrchKeyV__B0E69F31E41C90E3] PRIMARY KEY CLUSTERED ([SrchKeyValueId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SubUserProfile
-- ----------------------------
ALTER TABLE [dbo].[SubUserProfile] ADD CONSTRAINT [PK__SubUserP__50DB9E007A7AF2BE] PRIMARY KEY CLUSTERED ([SubUserId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SvcTranFee
-- ----------------------------
ALTER TABLE [dbo].[SvcTranFee] ADD CONSTRAINT [PK__SvcTranF__DC835B1EC8ED412F] PRIMARY KEY CLUSTERED ([SvcTranFeeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table sysssislog
-- ----------------------------
ALTER TABLE [dbo].[sysssislog] ADD CONSTRAINT [PK__sysssisl__3213E83F21CDD769] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SystemEnv
-- ----------------------------
ALTER TABLE [dbo].[SystemEnv] ADD CONSTRAINT [PK__SystemEn__C7B68F5082960774] PRIMARY KEY CLUSTERED ([SystemEnvId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SystemVersion
-- ----------------------------
ALTER TABLE [dbo].[SystemVersion] ADD CONSTRAINT [PK__SystemVe__665443CBCCB0A5A5] PRIMARY KEY CLUSTERED ([SystemVersionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TargetCriteria
-- ----------------------------
ALTER TABLE [dbo].[TargetCriteria] ADD CONSTRAINT [PK__TargetCr__C9A19BF4189E41A7] PRIMARY KEY CLUSTERED ([TargetCriteriaId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TargetGroup
-- ----------------------------
ALTER TABLE [dbo].[TargetGroup] ADD CONSTRAINT [PK__TargetGr__73CAF86DCBD00958] PRIMARY KEY CLUSTERED ([TargetGroupId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TaxPlan
-- ----------------------------
ALTER TABLE [dbo].[TaxPlan] ADD CONSTRAINT [PK__TaxPlan__29B0D6E839E9CDEF] PRIMARY KEY CLUSTERED ([TaxPlanId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TaxPlanBen
-- ----------------------------
ALTER TABLE [dbo].[TaxPlanBen] ADD CONSTRAINT [PK__TaxPlanB__CA0CB545CC7C5A9F] PRIMARY KEY CLUSTERED ([TaxPlanBenId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TaxPlanContingenBen
-- ----------------------------
ALTER TABLE [dbo].[TaxPlanContingenBen] ADD CONSTRAINT [PK__TaxPlanC__347AB2BD64FD4AD0] PRIMARY KEY CLUSTERED ([TaxPlanContingenBenId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TaxPlanEmployer
-- ----------------------------
ALTER TABLE [dbo].[TaxPlanEmployer] ADD CONSTRAINT [PK__TaxPlanE__74BDB1DA5460E68D] PRIMARY KEY CLUSTERED ([TaxPlanEmployerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TerminalNotificationActivity
-- ----------------------------
ALTER TABLE [dbo].[TerminalNotificationActivity] ADD CONSTRAINT [PK__Terminal__F1FAD525AC603542] PRIMARY KEY CLUSTERED ([TerminalNotificationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Timezone
-- ----------------------------
ALTER TABLE [dbo].[Timezone] ADD CONSTRAINT [PK__Timezone__58BB386FAF0D6713] PRIMARY KEY CLUSTERED ([TimezoneId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TimezoneTXT
-- ----------------------------
ALTER TABLE [dbo].[TimezoneTXT] ADD CONSTRAINT [PK__Timezone__F7188C0A9D6439F1] PRIMARY KEY CLUSTERED ([TimezoneTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TitleGeneration
-- ----------------------------
ALTER TABLE [dbo].[TitleGeneration] ADD CONSTRAINT [PK__TitleGen__4DE83E95B1B6D8F4] PRIMARY KEY CLUSTERED ([TitleGenerationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TitleOption
-- ----------------------------
ALTER TABLE [dbo].[TitleOption] ADD CONSTRAINT [PK__TitleOpt__16E48607B38EA70B] PRIMARY KEY CLUSTERED ([TitleOptionId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TitleOptionTXT
-- ----------------------------
ALTER TABLE [dbo].[TitleOptionTXT] ADD CONSTRAINT [PK__TitleOpt__BC305A12408724C3] PRIMARY KEY CLUSTERED ([TitleOptionTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TitleRelationshipMatrix
-- ----------------------------
ALTER TABLE [dbo].[TitleRelationshipMatrix] ADD CONSTRAINT [PK__TitleRel__E374F49BF5E0B86B] PRIMARY KEY CLUSTERED ([TitleRelationshipMatrixId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TitleRelationshipTXT
-- ----------------------------
ALTER TABLE [dbo].[TitleRelationshipTXT] ADD CONSTRAINT [PK__TitleRel__5DE6B06BD4DAC0FA] PRIMARY KEY CLUSTERED ([TitleRelationshipTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TraceAuditSeq
-- ----------------------------
ALTER TABLE [dbo].[TraceAuditSeq] ADD CONSTRAINT [PK__TraceAud__E18D229F5B608EC5] PRIMARY KEY CLUSTERED ([TraceAuditId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranAcctTypeLink
-- ----------------------------
ALTER TABLE [dbo].[TranAcctTypeLink] ADD CONSTRAINT [PK__TranAcct__AC00B61F51A8CCBE] PRIMARY KEY CLUSTERED ([TranAcctTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranApprovalLimits
-- ----------------------------
ALTER TABLE [dbo].[TranApprovalLimits] ADD CONSTRAINT [PK__TranAppr__F64431B705447F8B] PRIMARY KEY CLUSTERED ([TranApprovalLimitId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranAudit
-- ----------------------------
ALTER TABLE [dbo].[TranAudit] ADD CONSTRAINT [PK__TranAudi__250103E67283939D] PRIMARY KEY CLUSTERED ([JournalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranCategory
-- ----------------------------
ALTER TABLE [dbo].[TranCategory] ADD CONSTRAINT [PK__TranCate__862287CFF483A010] PRIMARY KEY CLUSTERED ([TranCatId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranCodeMatrix
-- ----------------------------
ALTER TABLE [dbo].[TranCodeMatrix] ADD CONSTRAINT [PK__TranCode__C748FAB13721A78C] PRIMARY KEY CLUSTERED ([TranCodeMatrixId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranDefAttribute
-- ----------------------------
ALTER TABLE [dbo].[TranDefAttribute] ADD CONSTRAINT [PK__TranDefA__C18929EAD96C9E9E] PRIMARY KEY CLUSTERED ([AttributeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranFeeType
-- ----------------------------
ALTER TABLE [dbo].[TranFeeType] ADD CONSTRAINT [PK__TranFeeT__48597E058F49B495] PRIMARY KEY CLUSTERED ([TranFeeTypeId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranFeeTypeTXT
-- ----------------------------
ALTER TABLE [dbo].[TranFeeTypeTXT] ADD CONSTRAINT [PK__TranFeeT__7E4558A11FCF9A77] PRIMARY KEY CLUSTERED ([TranFeeTypeTXTId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranItemConstraintRule
-- ----------------------------
ALTER TABLE [dbo].[TranItemConstraintRule] ADD CONSTRAINT [PK__TranItem__2090ED65A89678E3] PRIMARY KEY CLUSTERED ([TranItemConstraintRuleId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranJournal
-- ----------------------------
ALTER TABLE [dbo].[TranJournal] ADD CONSTRAINT [PK__TranJour__83C00D9599CCA414] PRIMARY KEY CLUSTERED ([TranJournalId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranParty
-- ----------------------------
ALTER TABLE [dbo].[TranParty] ADD CONSTRAINT [PK__TranPart__95CDBE1B8AEF5EA0] PRIMARY KEY CLUSTERED ([TranPartyId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TransactionDef
-- ----------------------------
ALTER TABLE [dbo].[TransactionDef] ADD CONSTRAINT [PK__Transact__F70897C9F520BDA4] PRIMARY KEY CLUSTERED ([TranId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TransactionDefTXT
-- ----------------------------
ALTER TABLE [dbo].[TransactionDefTXT] ADD CONSTRAINT [PK__Transact__EFAD3E24BF64E8C0] PRIMARY KEY CLUSTERED ([TranTXTId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TransactionLimits
-- ----------------------------
ALTER TABLE [dbo].[TransactionLimits] ADD CONSTRAINT [PK__Transact__00F5D2C0E5B2B80B] PRIMARY KEY CLUSTERED ([TransactionLimitId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranScriptDef
-- ----------------------------
ALTER TABLE [dbo].[TranScriptDef] ADD CONSTRAINT [PK__TranScri__7B62D2557C48C52E] PRIMARY KEY CLUSTERED ([TranScriptId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TranSeqNum
-- ----------------------------
ALTER TABLE [dbo].[TranSeqNum] ADD CONSTRAINT [PK__TranSeqN__291B9B97DC143868] PRIMARY KEY CLUSTERED ([TranSeqId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Tray
-- ----------------------------
ALTER TABLE [dbo].[Tray] ADD CONSTRAINT [PK__Tray__1A6BA1B11CCA64E2] PRIMARY KEY CLUSTERED ([TrayId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table TrayType
-- ----------------------------
ALTER TABLE [dbo].[TrayType] ADD CONSTRAINT [PK__TrayType__107FF80B714F4225] PRIMARY KEY CLUSTERED ([TrayTypeId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table UCFHoldMatrix
-- ----------------------------
ALTER TABLE [dbo].[UCFHoldMatrix] ADD CONSTRAINT [PK__UCFHoldM__5CC6EBA362908FC3] PRIMARY KEY CLUSTERED ([HoldReasonCodeId])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table UserDrawer
-- ----------------------------
ALTER TABLE [dbo].[UserDrawer] ADD CONSTRAINT [PK__UserDraw__0B861C7DEF07E778] PRIMARY KEY CLUSTERED ([UserDrawerId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table UserPreferences
-- ----------------------------
ALTER TABLE [dbo].[UserPreferences] ADD CONSTRAINT [PK__UserPref__4D1A68C59DD0C052] PRIMARY KEY CLUSTERED ([UserPreferencesId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table UserRequestHistory
-- ----------------------------
ALTER TABLE [dbo].[UserRequestHistory] ADD CONSTRAINT [PK__UserRequ__6E86CC3D2F484E6B] PRIMARY KEY CLUSTERED ([Duid], [UserName])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Verification
-- ----------------------------
ALTER TABLE [dbo].[Verification] ADD CONSTRAINT [PK__Verifica__E58853D2731D7525] PRIMARY KEY CLUSTERED ([VerficationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table VerificationRollupMatrix
-- ----------------------------
ALTER TABLE [dbo].[VerificationRollupMatrix] ADD CONSTRAINT [PK__Verifica__7F35B10BCAC6E5B4] PRIMARY KEY CLUSTERED ([VerificationRollupId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table VirtualAddress
-- ----------------------------
CREATE TRIGGER [dbo].[tr_D_AUDIT_VirtualAddress]
ON [dbo].[VirtualAddress]
WITH EXECUTE AS CALLER
FOR DELETE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'VirtualAddress'
		,@ApplId
		,'VirtualAddress record deleted'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE VirtAddrId = d.VirtAddrId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,NULL
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
END
GO

CREATE TRIGGER [dbo].[tr_U_AUDIT_VirtualAddress]
ON [dbo].[VirtualAddress]
WITH EXECUTE AS CALLER
FOR UPDATE
NOT FOR REPLICATION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ApplId VARCHAR(32)
		,@ApplName VARCHAR(64)
		,@FinInstKey VARCHAR(32)
		,@UpdatorDate DATETIME
		,@CreatedDate DATETIME
		,@UpdatorUID VARCHAR(32)
		,@CreatorUID VARCHAR(32)
		,@UpdatorUSR VARCHAR(128)
		,@CreatorUSR VARCHAR(128)

	SELECT @ApplId = ApplicationId
		,@ApplName = ApplName
		,@FinInstKey = FinInstKey
		,@UpdatorDate = UpdatorDate
		,@CreatedDate = CreatedDate
		,@UpdatorUID = UpdatorUID
		,@CreatorUID = CreatorUID
		,@UpdatorUSR = UpdatorUSR
		,@CreatorUSR = CreatorUSR
	FROM BookApplication
	WHERE ApplicationId = (
			SELECT ApplicationId
			FROM BookApplication
			WHERE BookApplicationId = (
					SELECT BookApplicationId
					FROM BookParty
					WHERE BookPartyId = (
							SELECT BookPartyId
							FROM DELETED
							)
					)
			)

	INSERT INTO dbo.[AuditLog] (
		TableName
		,ApplicationId
		,MessageContext
		,ApplicationName
		,FinInstKey
		,ModifierDate
		,ModifierUID
		,ModifierUSR
		,OldContent
		,NewContent
		,AuditDate
		)
	SELECT 'VirtualAddress'
		,@ApplId
		,'VirtualAddress record updated'
		,@ApplName
		,@FinInstKey
		,ISNULL(@UpdatorDate, @CreatedDate)
		,ISNULL(@UpdatorUID, @CreatorUID)
		,ISNULL(@UpdatorUSR, @CreatorUSR)
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM DELETED
							WHERE VirtAddrId = d.VirtAddrId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS OLD_VALUES
		,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
							SELECT *
							FROM INSERTED
							WHERE VirtAddrId = d.VirtAddrId
							FOR XML RAW
							), '<row ', ''), '/>', ''), '="', '='), '" ', '|'), '"','') AS NEW_VALUES
		,CONVERT(DATE, GETDATE())
	FROM DELETED d
	INNER JOIN VirtualAddress lu ON d.VirtAddrId = lu.VirtAddrId
	WHERE (
			SELECT *
			FROM DELETED
			WHERE VirtAddrId = d.VirtAddrId
			FOR XML RAW
			) <> (
			SELECT *
			FROM INSERTED
			WHERE VirtAddrId = d.VirtAddrId
			FOR XML RAW
			)
END
GO


-- ----------------------------
-- Primary Key structure for table VirtualAddress
-- ----------------------------
ALTER TABLE [dbo].[VirtualAddress] ADD CONSTRAINT [PK__VirtualA__2F05BA08109B582C] PRIMARY KEY CLUSTERED ([VirtAddrId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table XchgRate
-- ----------------------------
ALTER TABLE [dbo].[XchgRate] ADD CONSTRAINT [PK__XchgRate__3C846905D0DEDF1A] PRIMARY KEY CLUSTERED ([XchgRateId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ZoneFeed
-- ----------------------------
ALTER TABLE [dbo].[ZoneFeed] ADD CONSTRAINT [PK__ZoneFeed__C58B5DE206EBFC55] PRIMARY KEY CLUSTERED ([ZoneFeedId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table ZoneStat
-- ----------------------------
ALTER TABLE [dbo].[ZoneStat] ADD CONSTRAINT [PK__ZoneStat__A0EC1D8D73E3D310] PRIMARY KEY CLUSTERED ([ZoneStatId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

