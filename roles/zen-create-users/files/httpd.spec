###############################################################################
# Spec file for Utils
################################################################################
# Configured to be built by user student or other non-root user
################################################################################
#
Summary: Zenmonics Apache HTTPD
Name: zen-httpd
Version: 1.0.0
Release: 1
License: GPL
URL: https://www.zenmonics.com
Group: Operations
Packager: Al Wiesner
BuildRoot: ~/rpmbuild/

# Build with the following syntax:
# rpmbuild --target noarch -bb utils.spec

%description
Zenmonics RHEL Apache HTTPD build

%prep
################################################################################
# Create the build tree and copy the files from the development directories    #
# into the build tree.                                                         #
################################################################################
echo "BUILDROOT = $RPM_BUILD_ROOT"
mkdir -p $RPM_BUILD_ROOT/etc/httpd

cp -R /home/rpmbuild/apache/httpd/* $RPM_BUILD_ROOT/etc/httpd

exit

%files
%attr(0744, apache, apache) /etc/httpd/*

%pre
getent group apache >/dev/null || groupadd -r apache
getent passwd apache >/dev/null || useradd -r -g apache -d /etc/httpd -s /sbin/nologin -c "httpd apache user" apache
exit 0

%post


%postun

%clean
rm -rf $RPM_BUILD_ROOT/etc/httpd

%changelog
* Fri Mar 08 2019 Al Wiesner <albert.wiesner@zenmonics.com>
  - Zenmonics RPM build of RHEL Apache HTTPD zip package

