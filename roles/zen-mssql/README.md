Role Name
=========

This role will perform the following:
- Install and uninstall of the Microsoft SQL Server
- Create, delete, or import (from .sql file) a database 
- Create a Login User
- Create a DB User
- Command line tools for RHEL 7

Requirements
------------

Base epel-release RHEL repository



Role Variables
--------------

Variables in the vars/main are static variables that should not be changed or overriden.  These define the repo's necessary for installation of the mssql-server and command line tools.  In addition, they define the necessary packages to allow SQL Server to run on the Linux distribution.  Updates to this file should only be due to repo or package dependency changes.

The default user is 'SA' when logging in via command line tools. The SA user is mandatory for initial creation.

Additionally, there are some predefined default values including witin defaults/main:
```yaml
db_name: testDB
db_host: 127.0.0.1
db_port: 1433
db_user: SA
db_password: P@ssWORD!

install_db: false
create_user: false
```
When defined in a playbook, the base role, without any passed variables, will create a SQL Server installation, configure that installation, and setup the SA user.  No other internal DB tasks will be performed.

To create a database and different DB user, you must pass the variables from the playbook to the role as shown below in the 'Exampe Playbooks' section



Example Playbook
----------------

**To use the default installation tasks:**
```yaml
---
- hosts: db
  roles:
    - mssql
```

**To use the installation and create a new db (without specific Application DB user):**
```yaml
---
- hosts: db
  roles:
    - role: mssql
      app_version: "8_2_0_1"
      db_name: env2000-002-DEV-AOCU
      install_db: true
```
Requirements:
* roles/mssql/files directory MUST contain a DDL and DML script with the following format:
    * {version}\_\_DDL\_Base.sql
    * {version}\_\_DML\_Base.sql
    * {version} must be in format of:  "#\_#\_#\_#"
        * Example:
        * 8\_2\_0\_1\_\_DDL\_Base.sql
        * 8\_2\_0\_1\_\_DML\_Base.sql
* playbook must pass the app\_version as "#\_#\_#\_#"


**To install SQL Server, create a new DB, and create an application login and assign as DB user to DB):**
```yaml
---
- hosts: db
  roles:
    - role: mssql
      app_version: "8_2_0_1"
      db_name: env2000-002-DEV-AOCU
      install_db: true
      create_user: true
      app_db_user: oao
      app_db_pass: P@ssWORD!
```
Requirements:
*  See above for the requirements around DDL/DML scripts and app_versions
*  In order to create the user, the database must exist

TO DO
----------------
*  Utilize Ansible vault for password storage; current storage is plain text
*  Update code to pull DDL/DML sql files from Git rather than being stored in roles/mssql/files directory

