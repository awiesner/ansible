#!/bin/bash

# get full path to where script lives, since it lives next to the ansible.cfg file
vagrant_dir="$(pwd)"
ansible_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [[ ! -f "${vagrant_dir}/ansible.cfg" ]]; then
  #copy ansible.cfg file to the base vagrant dir
  cp "${ansible_dir}/config/ansible.cfg" $vagrant_dir
  sed -i '.original' -e "s~/etc/ansible/ansible.log~${vagrant_dir}/ansible.log~g" ansible.cfg
  sed -i '.original' -e "s~/etc/ansible/~${vagrant_dir}/ansible/~g" ansible.cfg
fi

