###############################################################################
# Spec file for JBOSS
################################################################################

%define version 7.1
%define gitLocation git@zcccv-repos.corp.zenmonics.com:devops/server/jboss-eap-%{version}.git


Summary: Zenmonics JBOSS
Name: zen-jboss
Version: %{version}
Release: 1
License: GPL
URL: https://www.zenmonics.com
Group: Operations
Packager: Al Wiesner
BuildRoot: ~/rpmbuild/
AutoReqProv: 0

# Build with the following syntax:
# rpmbuild --target x86_64 -bb jboss.spec

%description
Zenmonics RHEL JBOSS %{version} build

%prep
################################################################################
# Create the build tree and copy the files from the build directories into     #
# the build tree.                                                              #
################################################################################
echo "BUILDROOT = $RPM_BUILD_ROOT"
mkdir -p $RPM_BUILD_ROOT/usr/share/jboss-%{version}
git clone %{gitLocation}

cp -R %{_builddir}/jboss-eap-%{version}/* $RPM_BUILD_ROOT/usr/share/jboss-%{version}

exit

%files
%attr(0744, jboss, jboss) /usr/share/jboss-%{version}

%pre
getent group jboss >/dev/null || groupadd -r jboss --gid 91
getent passwd jboss >/dev/null || adduser -r -g jboss -u 91 -m -s /sbin/nologin -c "jboss user" jboss
exit 0

%post


%postun

%clean
rm -rf $RPM_BUILD_ROOT/usr/share/jboss-%{version}
rm -rf %{_builddir}/jboss-%{version}

%changelog
* Mon Mar 11 2019 Al Wiesner <albert.wiesner@zenmonics.com>
  - Zenmonics RPM build of RHEL JBOSS %{version} zip package
