###############################################################################
# Spec file for HTTPD
################################################################################

%define version 24_2.4 
%define gitLocation git@zcccv-repos.corp.zenmonics.com:devops/server/patched/httpd.git

Summary: Zenmonics Apache HTTPD
Name: zen-httpd
Version: %{version}
Release: 1
License: GPL
URL: https://www.zenmonics.com
Group: Operations
Packager: Al Wiesner
BuildRoot: ~/rpmbuild/
AutoReqProv: 0

# Build with the following syntax:
# rpmbuild --target x86_64  -bb httpd.spec

%description
Zenmonics RHEL Apache HTTPD build

%prep
################################################################################
# Create the build tree and copy the files from the build directories into     #
# the build tree.                                                              #
################################################################################
echo "BUILDROOT = $RPM_BUILD_ROOT"
mkdir -p $RPM_BUILD_ROOT/etc/httpd
git clone %{gitLocation}

cp -R %{_builddir}/httpd/* $RPM_BUILD_ROOT/etc/httpd

exit

%files
%attr(0744, apache, apache) /etc/httpd/*

%pre
getent group apache >/dev/null || groupadd -r apache
getent passwd apache >/dev/null || useradd -r -g apache -d /etc/httpd -s /sbin/nologin -c "httpd apache user" apache
exit 0

%post


%postun

%clean
rm -rf $RPM_BUILD_ROOT/etc/httpd
rm -rf %{_builddir}/httpd

%changelog
* Mon Mar 11 2019 Al Wiesner <albert.wiesner@zenmonics.com>
  - Zenmonics RPM build of RHEL Apache HTTPD zip package

