###############################################################################
# Spec file for JDK
################################################################################

%define version 1.8.0_192
%define gitLocation git@zcccv-repos.corp.zenmonics.com:devops/jdk/linux/1.8/jdk%{version}.git

Summary: Zenmonics Apache HTTPD
Name: zen-jdk
Version: %{version}
Release: 1
License: GPL
URL: https://www.zenmonics.com
Group: Operations
Packager: Al Wiesner
BuildRoot: ~/rpmbuild/
AutoReqProv: 0

# Build with the following syntax:
# rpmbuild --target x86_64 -bb jdk.spec

%description
Zenmonics RHEL JDK %{version} Build

%prep
################################################################################
# Create the build tree and copy the files from the build directories into     #
# the build tree.                                                              #
################################################################################
echo "BUILDROOT = $RPM_BUILD_ROOT"
mkdir -p $RPM_BUILD_ROOT/usr/java/jdk%{version}
git clone %{gitLocation}

cp -R %{_builddir}/jdk%{version}/* $RPM_BUILD_ROOT/usr/java/jdk%{version}

exit

%files
%attr(0744, root, root) /usr/java/jdk%{version}

%pre

%post

%postun

%clean
rm -rf $RPM_BUILD_ROOT/usr/java/jdk%{version}
rm -rf %{_builddir}/jdk%{version}

%changelog
* Mon Mar 11 2019 Al Wiesner <albert.wiesner@zenmonics.com>
  - Zenmonics RPM build of RHEL JDK %{version} zip package
